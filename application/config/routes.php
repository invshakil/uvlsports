<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';

/*
 * HIDE CONTROLLER NAME BY CUSTOM ROUTING (FRONT END)
 */
$route['league_fixture'] = 'welcome/league_fixture';
$route['league-fixture'] = 'welcome/league_fixture';
$route['point_table'] = 'welcome/point_table';
$route['point-table'] = 'welcome/point_table';

$route['weekend_fixture_on_tv'] = 'welcome/weekend_fixture_on_tv';
$route['tv-schedule'] = 'welcome/weekend_fixture_on_tv';

$route['contact'] = 'welcome/contact';
$route['search_result'] = 'welcome/search_result';
$route['search-result'] = 'welcome/search_result';
$route['privacy-policy'] = 'welcome/privacy_policy';

$route['latest-short-stories'] = 'transfer_live';


$route['news_details/(:num)/:any/:any'] = 'welcome/news_details/$1//';
$route['news-details/(:num)/:any/:any'] = 'welcome/news_details/$1//';

$route['english-premier-league'] = 'english_premier_league';
$route['laliga-santander'] = 'laliga_santander';
$route['serie-a'] = 'serie_a';
$route['other-leagues'] = 'other_leagues';
$route['national-teams'] = 'national_teams';
$route['fantasy-football'] = 'fantasy_football';
$route['users-column'] = 'users_column';
$route['user-profile'] = 'user_profile';

/*
 * ---! END OF HIDE CONTROLLER NAME BY CUSTOM ROUTING (FRONT END)
 */

/*
 * HIDE CONTROLLER NAME BY CUSTOM ROUTING (ADMIN END)
 */

$route['sign-in'] = 'user_authentication';

$route['sign-up'] = 'user_registration';


/*
 * ---! END OF HIDE CONTROLLER NAME BY CUSTOM ROUTING (ADMIN END)
 */

$route['404_override'] = 'welcome/page_404';
$route['translate_uri_dashes'] = TRUE;
<?php //echo '<?xml version="1.0" encoding="' . $encoding . '"?>' . "\n"; ?>
<!--<rss version="2.0"-->
<!--     xmlns:dc="http://purl.org/dc/elements/1.1/"-->
<!--     xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"-->
<!--     xmlns:admin="http://webns.net/mvcb/"-->
<!--     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"-->
<!--     xmlns:content="http://purl.org/rss/1.0/modules/content/">-->
<!---->
<!--    <channel>-->
<!---->
<!--        <title>--><?php //echo $feed_name; ?><!--</title>-->
<!---->
<!--        <link>--><?php //echo $feed_url; ?><!--</link>-->
<!--        <description>--><?php //echo $page_description; ?><!--</description>-->
<!--        <dc:language>--><?php //echo $page_language; ?><!--</dc:language>-->
<!--        <dc:creator>--><?php //echo $creator_email; ?><!--</dc:creator>-->
<!--        <dc:rights>Copyright --><?php //echo gmdate("Y", time()); ?><!--</dc:rights>-->
<!--        <admin:generatorAgent rdf:resource="http://www.uvlsports.com/" />-->
<!---->
<!--        --><?php //foreach ($rss_feed as $post): ?>
<!---->
<!--            <item>-->
<!---->
<!--                <!doctype html>-->
<!--                <html lang="en" prefix="op: http://media.facebook.com/op#">-->
<!--                <head>-->
<!--                    <meta charset="utf-8">-->
<!--                    <!-- URL of the web version of this article -->-->
<!--                    <!-- TODO: Change the domain to match the domain of your website -->-->
<!--                    <link rel="canonical" href="--><?php //echo site_url('news_details/' . $post->post_id . '/' . $post->post_category_id . '/' . $post->en_post_url) ?><!--">-->
<!--                    <meta property="op:markup_version" content="v1.0">-->
<!--                </head>-->
<!--                <body>-->
<!--                <article>-->
<!--                    <header>-->
<!--                        <!-- The title and subtitle shown in your Instant Article -->-->
<!--                        <h1><title>--><?php //echo xml_convert($post->post_title); ?><!--</title></h1>-->
<!--<!--                        <h2>Article Subtitle</h2>-->-->
<!---->
<!--                        <!-- The date and time when your article was originally published -->-->
<!--                        <time class="op-published" datetime="2014-11-11T04:44:16Z">--><?php //$date = $post->post_date;
//                            echo date("F j, Y, g:i a", strtotime($date)); ?><!--</time>-->
<!---->
<!--                        <!-- The date and time when your article was last updated -->-->
<!--<!--                        <time class="op-modified" dateTime="2014-12-11T04:44:16Z">December 11th, 4:44 PM</time>-->-->
<!---->
<!--                        <!-- The authors of your article -->-->
<!--<!--                        <address>-->-->
<!--<!--                            <a rel="facebook" href="http://facebook.com/brandon.diamond">Brandon Diamond</a>-->-->
<!--<!--                            Brandon is a avid zombie hunter.-->-->
<!--<!--                        </address>-->-->
<!--<!--                        <address>-->-->
<!--<!--                            <a>TR Vishwanath</a>-->-->
<!--<!--                            Vish is a scholar and a gentleman.-->-->
<!--<!--                        </address>-->-->
<!---->
<!--                        <!-- The cover image shown inside your article -->-->
<!--                        <!-- TODO: Change the URL to a live image from your website -->-->
<!--                        <figure>-->
<!--                            <img src="--><?php //echo base_url('./image_upload/post_image/' . $post->post_image); ?><!--" />-->
<!--<!--                            <figcaption>This image is amazing</figcaption>-->-->
<!--                        </figure>-->
<!---->
<!--                        <!-- A kicker for your article -->-->
<!--<!--                        <h3 class="op-kicker">-->-->
<!--<!--                            This is a kicker-->-->
<!--<!--                        </h3>-->-->
<!---->
<!--                    </header>-->
<!---->
<!--                    <!-- Article body goes here -->-->
<!---->
<!--                    <!-- Body text for your article -->-->
<!--                    <p>--><?php //echo $post->post_description; ?><!-- </p>-->
<!---->
<!--                    <!-- A video within your article -->-->
<!--                    <!-- TODO: Change the URL to a live video from your website -->-->
<!---->
<!--                    <!-- An ad within your article -->-->
<!--                    <!-- TODO: Change the URL to a live ad from your website -->-->
<!--<!--                    <figure class="op-ad">-->-->
<!--<!--                        <iframe src="https://www.adserver.com/ss;adtype=banner320x50" height="60" width="320"></iframe>-->-->
<!--<!--                    </figure>-->-->
<!---->
<!--                    <!-- Analytics code for your article -->-->
<!--<!--                    <figure class="op-tracker">-->-->
<!--<!--                        <iframe src="" hidden></iframe>-->-->
<!--<!--                    </figure>-->-->
<!---->
<!--                    <footer>-->
<!--                        <!-- Credits for your article -->-->
<!--                        <aside>Acknowledgements</aside>-->
<!---->
<!--                        <!-- Copyright details for your article -->-->
<!--                        <small>Legal notes</small>-->
<!--                    </footer>-->
<!--                </article>-->
<!--                </body>-->
<!--                </html>-->
<!---->
<!--<!--                <title>-->--><?php ////echo xml_convert($post->post_title); ?><!--<!--</title>-->-->
<!--<!--                <link>-->--><?php ////echo site_url('news_details/' . $post->post_id . '/' . $post->post_category_id . '/' . $post->en_post_url) ?><!--<!--</link>-->-->
<!--<!--                <guid>-->--><?php ////echo site_url('news_details/' . $post->post_id . '/' . $post->post_category_id . '/' . $post->en_post_url) ?><!--<!--</guid>-->-->
<!--<!--                <content:encoded><![CDATA[ -->--><?php ////echo character_limiter($post->post_description, 200); ?><!--<!-- ]]></content:encoded>-->-->
<!--<!--                <description><![CDATA[ -->--><?php ////echo character_limiter($post->post_description, 200); ?><!--<!-- ]]></description>-->-->
<!--<!--                <enclosure url="-->--><?php ////echo base_url('./image_upload/post_image/' . $post->post_image); ?><!--<!--" length="10240" type="image/jpg" />-->-->
<!--<!--                <pubDate>-->--><?php ////$date = $post->post_date;
////                    echo date("F j, Y, g:i a", strtotime($date)); ?><!--<!--</pubDate>-->-->
<!--<!--                <author>-->--><?php ////echo $post->user_name?><!--<!-- </author>-->-->
<!--            </item>-->
<!---->
<!---->
<!--        --><?php //endforeach; ?>
<!---->
<!--    </channel>-->
<!--</rss>-->
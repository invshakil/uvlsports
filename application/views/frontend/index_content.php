<div class="content_top">
    <!--SLIDER CONTENT -->

    <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="content_middle_middle">
            <div class="slick_slider2">

                <?php
                if (is_array($published_latest) && count($published_latest) >= 1) {


                    foreach ($published_latest as $featured) {
                        ?>
                        <div class="single_featured_slide"><a
                                    href="<?php echo base_url(); ?>news-details/<?php echo $featured->post_id; ?>/<?php echo $featured->post_category_id; ?>/<?php echo $featured->en_post_url; ?>">
                                <img src="<?php echo base_url() . './image_upload/post_image/' . $featured->post_image; ?>"
                                     alt="<?php echo $featured->post_title; ?>"></a>
                            <h2>
                                <a href="<?php echo base_url(); ?>news-details/<?php echo $featured->post_id; ?>/<?php echo $featured->post_category_id; ?>/<?php echo $featured->en_post_url; ?>"><?php echo $featured->post_title; ?></a>
                            </h2>

                        </div>

                        <?php
                    }
                } else {
                    echo "<h6>&nbsp&nbsp&nbsp&nbspNo Post Available!</h6>";
                }
                ?>

            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-6" id="col-lg-6">
        <div class="single_category">
            <h1><span class="bold_line"><span></span></span> <span class="solid_line"></span> <a
                        class="title_text" href="<?php echo base_url()?>latest-short-stories"><strong> খেলার সর্বশেষ সংবাদ </strong>(সংক্ষিপ্ত)</a></h1>
            <hr/>
            <ul class="small_catg popular_catg wow fadeInDown">
                <?php foreach ($transfer_live_latest as $post) { ?>
                    <li id="sideDivEffect">
                        <div class="media wow fadeInDown">
                            <a style="padding-left: 10px" href="#" class="media-left"><img
                                        alt="<?php echo $post->tweet_title; ?>"
                                        src="<?php echo $post->tweet_image; ?>">
                            </a>
                            <div class="media-body">
                                <ul>
                                    <li><h4 class="media-heading"><a
                                                    href="<?php echo base_url(); ?>latest-short-stories"><?php echo $post->tweet_title; ?></a>
                                        </h4></li>
                                </ul>
                                <p>
                                    <?php

                                    $str = $post->tweet_description;

                                    $length = strlen($str);


                                    if ($length > 100) {

                                        $strCut = substr($str, 0, 500);

                                        $str = substr($strCut, 0, strrpos($strCut, " ")) . '...<a href="latest-short-stories">Read More</a>';
                                    }

                                    echo $str;
                                    ?>
                                </p>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>

        </div>
    </div>

</div>
<div class="content_bottom">
    <div class="col-lg-8 col-md-8">
        <div class="content_bottom_left">

            <!-- CRICKET AND OTHERS CONTENT -->

            <div class="technology_catrarea">


                <!--Latest from Football -->

                <div class="content_bottom_left">
                    <div class="single_category wow fadeInDown">
                        <div class="archive_style_1">
                            <h1><span class="bold_line"><span></span></span> <span class="solid_line"></span>
                                <span class="title_text"><strong>ফুটবলের সর্বশেষ আর্টিক্যাল</strong></span></h1>

                            <!--AJAX REQUEST WILL LOAD THE CONTENT -->

                            <div id="results"></div>
                            <div class="text-center">
                                <p class="label label-danger">Scroll to bottom of page to get more post!</p>
                            </div>
                            <div class="loader_image text-center" style="display:none">
                                <b><img src="https://media.giphy.com/media/HVUmX2TtQSO6k/200.gif" width="80"></b>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4" >
        <div class="content_bottom_right" id="sidebar">
            <div class="single_bottom_rightbar">
                <a href="<?php echo base_url()?>cricket"></a>
                <h1><strong> ক্রিকেট ও অন্যান্য সংবাদ</strong></h1>

                </a>
                <ul class="small_catg popular_catg wow fadeInDown">
                    <?php if (is_array($cricket) && count($cricket) >= 1) { ?>

                        <?php foreach ($cricket as $post) { ?>

                            <li>
                                <div class="media wow fadeInDown"><a href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>" class="media-left">
                                        <img alt="<?php $date = $post->post_title; ?>"
                                             src="<?php echo base_url() . './image_upload/post_image/thumbs/' . $post->post_image; ?>">
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading"><a
                                                    href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>"><?php echo $post->post_title; ?></a>
                                        </h4>
                                        <div class="comments_box">
                                    <span class="fa fa-calendar-o">
                                        <?php $date = $post->post_date;
                                        echo date("M d", strtotime($date)); ?>
                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <?php
                        }
                    } else {
                        echo "<h4>&nbsp&nbsp&nbsp&nbspNo Post Available!</h4>";
                    }
                    ?>
                </ul>

            </div>

<!--            <div class="single_bottom_rightbar" id="fixture" style="display: none;">-->
<!--                <h2>FIXTURE & RESULTS</h2>-->
<!--                <div class="pull-right">-->
<!--                    <iframe height="350" frameborder="5" style="vertical-align: bottom;"-->
<!--                            src="http://tools.whatsthescore.com/tools.php?id=37615&iframe=tap-sport-tools-37615&tz-js=Asia/Dhaka"></iframe>-->
<!--                </div-->
<!--            </div>-->
<!--            <div class="single_bottom_rightbar">-->
<!--                <h1>জনপ্রিয় লেখক</h1>-->
<!--                <div class="blog_archive wow fadeInDown">-->
<!---->
<!--                    --><?php //if (is_array($top_author) && count($top_author) >= 1) { ?>
<!--                        --><?php //foreach ($top_author as $recent) { ?>
<!---->
<!--                            <div class="media wow fadeInDown">-->
<!--                                <div class="media-body">-->
<!--                                    <a class="media-heading">-->
<!--                                        <h5><i class="fa fa-user" aria-hidden="true"></i> <a href="--><?php //echo base_url()?><!--user-profile/index/--><?php //echo $recent->user_id?><!--"> Author Name: --><?php //echo $recent->user_name; ?><!--</a></h5>-->
<!--                                        <h5><i class="fa fa-bar-chart" aria-hidden="true"></i> Total Post: --><?php //echo $recent->most; ?><!--</a></h5>-->
<!--                                    <h5><i class="fa fa-heart" aria-hidden="true"></i> Total Hits: --><?php //echo $recent->top; ?><!--</a></h5>-->
<!--                                    <h5><i class="fa fa-calendar" aria-hidden="true"></i> Joined: --><?php //$date = $recent->reg_time; echo date("F d, Y", strtotime($date)); ?><!--</a></h5>-->
<!---->
<!--                                </div>-->
<!--                            </div>-->
<!--                            --><?php
//                        }
//                    } else
//                        echo "<h4>&nbsp&nbsp&nbsp&nbspNo data Available!</h4>";
//                    ?>
<!--                </div>-->
<!--            </div>-->
            <div class="single_bottom_rightbar" id="stickySidebar">

                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Side Bar -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-9787339225677626"
                     data-ad-slot="9494900273"
                     data-ad-format="auto"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>

                <br/>
                <h1><strong>জনপ্রিয় খেলার সংবাদ</strong></h1>
                <ul class="small_catg popular_catg wow fadeInDown">
                    <?php if (is_array($popular_post) && count($popular_post) >= 1) { ?>

                        <?php foreach ($popular_post as $post) { ?>

                            <li>
                                <div class="media wow fadeInDown"><a href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>" class="media-left">
                                        <img alt="<?php $date = $post->post_title; ?>"
                                             src="<?php echo base_url() . './image_upload/post_image/thumbs/' . $post->post_image; ?>">
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading"><a
                                                    href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>"><?php echo $post->post_title; ?></a>
                                        </h4>
                                        <div class="comments_box">
                                    <span class="meta_date">
                                        <?php $date = $post->post_date;
                                        echo date("M d", strtotime($date)); ?>
                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <?php
                        }
                    } else {
                        echo "<h4>&nbsp&nbsp&nbsp&nbspNo Post Available!</h4>";
                    }
                    ?>
                </ul>

            </div>

        </div>
    </div>
</div>

<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>


<script type="text/javascript">
    $(document).ready(function () {
        var total_record = 0;
        var total_groups = <?php echo $total_data; ?>;
        $('#results').load("<?php echo base_url() ?>welcome/load_more_post",
            {'group_no': total_record}, function () {
                total_record++;
            });
        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                if (total_record <= total_groups) {
                    loading = true;
                    $(document).ready(function () {
                        setTimeout(function () {
                            $('.loader_image').show();
                        }, 1200);
                    });

                    //$('.loader_image').show();
                    $.post('<?php echo site_url() ?>welcome/load_more_post', {'group_no': total_record},
                        function (data) {
                            if (data != "") {
                                $("#results").append(data);
                                $('.loader_image').hide();
                                total_record++;
                            }
                        });
                    
                    
                }
            }
        });
        
    });
</script>

<style>
    #sidebar {
        position: -webkit-sticky!important;
        position: sticky!important;
        top: 0!important;
    }
</style>
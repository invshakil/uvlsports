<section id="mainContent">
    <div class="content_bottom">
        <div class="col-lg-8 col-md-8 archive_style_1">

            <h1><span class="bold_line"><span></span></span> <span class="solid_line"></span>
                <span class="title_text"><strong>সর্বশেষ খেলার সংবাদ (সংক্ষিপ্ত)</strong></span></h1>

            <h2>

                <?php
                $msg = $this->session->userdata('message');

                if ($msg) {
                    ?>

                    <span class="label label-success"><?php echo $msg; ?></span>

                    <?php
                    $this->session->unset_userdata('message');
                }
                ?>

            </h2>

            <!--USER_ID = RIZVY-->

            <?php if ($this->session->userdata('access_level') == 1 || $this->session->userdata('access_level') == 2 || $this->session->userdata('user_id') == 10)
            { ?>

            <form id="AddTweet" method="post" name="form" class="contact_form contact_us wow fadeInRightBig">
                <input class="form-control" type="text" id="tweet_title" placeholder="Enter Title" name="tweet_title">
                <input class="form-control" type="text" id="tweet_image" placeholder="Enter Image Link" name="tweet_image">
                <textarea class="form-control" cols="30" rows="10" name="tweet_description" id="tweet_description" data-value="Message"></textarea>
                <input type="submit" class="btn btn-danger" id="submitform" onclick="myFunction()" value="Tweet Update">
            </form>

            <div id="result"></div>

                <!--MODAL -->

                <div id="add_data_Modal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;
                                </button>
                                <h4 class="modal-title">Update Tweet Info</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form" id="insert_form">

                                    <div class="form-group">
                                        <label class="widget-header"><h2><strong>Tweet</strong> Title</h2></label>
                                        <input type="text" class="form-control" name="tweet_title" id="tweet_title1">
                                        <input type="hidden" class="form-control" name="tweet_id" id="tweet_id1">
                                    </div>
                                    <div class="form-group">
                                        <label class="widget-header"><h2><strong>Tweet</strong> Image</h2></label>
                                        <input type="text" class="form-control" name="tweet_image" id="tweet_image1">
                                    </div>
                                    <div class="form-group">
                                        <label class="widget-header"><h2><strong>Tweet</strong> Description</h2></label>
                                        <textarea class="form-control" cols="30" rows="10" name="tweet_description" id="tweet_description1" data-value="Message"></textarea>
                                    </div>

                                    <input type="submit" name="insert" id="insert" value="Insert"
                                           class="btn btn-success"/>
                                    <hr/>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                    Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            <?php } ?>
            <hr/>

            <div id="refresh" class="btn btn-info" style="font-size: 16px;">Refresh &nbsp;&nbsp;<i class="fa fa-refresh fa-spin" aria-hidden="true"></i></div><br>
            <hr/>
            <div id="results" ></div>
            <div class="loader_image text-center" style="display:none">
                <b><img src="https://media.giphy.com/media/HVUmX2TtQSO6k/200.gif" width="80"></b>
            </div>
            <div class="label label-danger">
                <span>Scroll to bottom of page, to get more news!</span>
            </div>

        </div>
        <div class="col-lg-4 col-md-4">
            <div class="content_bottom_right">
                <div class="single_bottom_rightbar">
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Side Bar -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-9787339225677626"
                         data-ad-slot="9494900273"
                         data-ad-format="auto"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>

                    <!--<iframe height="350" frameborder="5" style="vertical-align: bottom;" src="//tools.whatsthescore.com/tools.php?id=37615&iframe=tap-sport-tools-37615&tz-js=Asia/Dhaka"></iframe>
                  -->
                </div>

                <div class="single_bottom_rightbar">
                    <h2>Popular Post</h2>
                    <ul class="small_catg popular_catg wow fadeInDown">
                        <?php if (is_array($popular_post) && count($popular_post) >= 1) { ?>

                            <?php foreach ($popular_post as $post) { ?>

                                <li>
                                    <div class="media wow fadeInDown"><a href="#" class="media-left">
                                            <img alt="<?php $date = $post->post_title; ?>"
                                                 src="<?php echo base_url() . './image_upload/post_image/thumbs/' . $post->post_image; ?>">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a
                                                    href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>"><?php echo $post->post_title; ?></a>
                                            </h4>
                                            <div class="comments_box">
                                    <span class="fa fa-calendar-o">
                                        <?php $date = $post->post_date;
                                        echo date("M d", strtotime($date)); ?>
                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        } else {
                            echo "<h4>&nbsp&nbsp&nbsp&nbspNo Post Available!</h4>";
                        }
                        ?>
                    </ul>

                </div>

                <div class="single_bottom_rightbar">
                    <h1><strong>সর্বশেষ খেলার সংবাদগুলো</strong></h1>
                    <ul class="small_catg popular_catg wow fadeInDown">
                        <?php if (is_array($recent_post) && count($recent_post) >= 1) { ?>

                            <?php foreach ($recent_post as $post) { ?>

                                <li>
                                    <div class="media wow fadeInDown"><a href="#" class="media-left">
                                            <img alt="<?php $date = $post->post_title; ?>"
                                                 src="<?php echo base_url() . './image_upload/post_image/thumbs/' . $post->post_image; ?>">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a
                                                        href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>"><?php echo $post->post_title; ?></a>
                                            </h4>
                                            <div class="comments_box">
                                    <span class="fa fa-calendar-o">
                                        <?php $date = $post->post_date;
                                        echo date("M d", strtotime($date)); ?>
                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        } else {
                            echo "<h4>&nbsp&nbsp&nbsp&nbspNo Post Available!</h4>";
                        }
                        ?>
                    </ul>

                </div>

            </div>
        </div>
    </div>
</section>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>

    /*
        UPDATING TWEET UPDATE
     */

    $(document).ready(function () {
        $('#add').click(function () {
            $('#insert').val("Insert");
            $('#insert_form')[0].reset();
        });

        $(document).on('click', '.edit_data', function () {
            var tweet_id = $(this).attr("id");
            console.log(tweet_id);
            $.ajax({
                url: "<?php echo base_url()?>transfer_live/fetch_tweet_by_id",
                method: "POST",
                data: {tweet_id: tweet_id},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('#tweet_title1').val(data.tweet_title);
                    $('#tweet_description1').val(data.tweet_description);
                    $('#tweet_image1').val(data.tweet_image);
                    $('#tweet_id1').val(data.tweet_id);
                    $('#insert').val("Update");
                    $('#add_data_Modal').modal('show');
                }
            });
        });

        $('#insert_form').on("submit", function (event) {
            event.preventDefault();
            if ($('#tweet_title1').val() == "") {
                alert("tag name is required");
            }
            else if ($('#tweet_description1').val() == '') {
                alert("publication status is required");
            }
            else if ($('#tweet_image1').val() == '') {
                alert("publication status is required");
            }
            else {
                $.ajax({
                    url: "<?php echo base_url()?>transfer_live/update_tweet_by_id",
                    method: "POST",
                    data: $('#insert_form').serialize(),
                    beforeSend: function () {
                        $('#insert_form').val("Inserting");
                    },
                    success: function (data) {
                        $('#insert_form')[0].reset();
                        $('#add_data_Modal').modal('hide');
//                        $('#tag_table').html(data);
                        alert('Tweet Info Updated Successfully!');

                        setTimeout("location.href = '<?php echo base_url()?>latest-short-stories'", 10);
                    }
                });
            }
        });

    });
</script>


<script type="application/javascript">
    function myFunction() {
        var title = $('#tweet_title').val();
        var image = $('#tweet_image').val();
        var desc = $('#tweet_description').val();
        var dataString = 'tweet_title=' + title + '&tweet_image=' + image + '&tweet_description=' + desc;
        if (title === '' || image === '' || desc === '') {
            alert('Please fill up All the fields!');
        } else {
            $.ajax({
                type: 'POST',
                url: "<?php echo base_url();?>transfer_live/save_tweet",
                data: dataString,
                async: false,
                success: function (data) {
                    $("#result").append("<div class='alert-box success'>Successfully tweeted!</div>");

                    $('#tweet_title').val("");
                    $('#tweet_image').val("");
                    $('#tweet_description').val("");
                }
            });
        }
        return false;


    }

</script>


<script type="text/javascript">

      $(document).ready(function () {
        var total_record = 0;
        var total_groups = <?php echo $total_data; ?>;



        $('#results').load("<?php echo base_url() ?>transfer_live/fetch_tweets",
            {'group_no': total_record}, function () {
                total_record++;
            });

        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                if (total_record <= total_groups) {
                    loading = true;
                    $(document).ready(function () {
                        setTimeout(function () {
                            $('.loader_image').show();
                        }, 1200);
                    });

                    //$('.loader_image').show();
                    $.post('<?php echo base_url() ?>transfer_live/fetch_tweets', {'group_no': total_record},
                        function (data) {
                            if (data != "") {
                                $("#results").append(data);
                                $('.loader_image').hide();
                                total_record++;
                            }
                        });
                }
            }
        });
    });
    $('#refresh').click(function () {
        setTimeout("location.href = '<?php echo base_url()?>latest-short-stories'", 100);
    })

</script>

<script type="text/javascript">

    function checkDelete() {



        var chk = confirm('Are you sure about to delete this post?');

        if (chk)

        {

            return true;

        } else {

            return false;

        }

    }



</script>
<?php echo '<?xml version="1.0" encoding="' . $encoding . '"?>' . "\n"; ?>
<rss version="2.0"
     xmlns:dc="http://purl.org/dc/elements/1.1/"
     xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
     xmlns:admin="http://webns.net/mvcb/"
     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
     xmlns:content="http://purl.org/rss/1.0/modules/content/">

    <channel>

        <title><?php echo $feed_name; ?></title>

        <link><?php echo $feed_url; ?></link>
        <description><?php echo $page_description; ?></description>
        <dc:language><?php echo $page_language; ?></dc:language>
        <dc:creator><?php echo $creator_email; ?></dc:creator>
        <dc:rights>Copyright <?php echo gmdate("Y", time()); ?></dc:rights>
        <admin:generatorAgent rdf:resource="http://www.uvlsports.com/" />

        <?php foreach ($rss_feed as $post): ?>

            <item>
                <title><?php echo xml_convert($post->post_title); ?></title>

                <link><?php echo site_url('news_details/' . $post->post_id . '/' . $post->post_category_id . '/' . $post->en_post_url) ?></link>

                <guid><?php echo site_url('news_details/' . $post->post_id . '/' . $post->post_category_id . '/' . $post->en_post_url) ?></guid>

                <pubDate><?php $date = $post->post_date;
                    echo date("F j, Y, g:i a", strtotime($date)); ?>
                </pubDate>

                <author><?php echo $post->user_name;?></author>

<!--                <description><![CDATA[ --><?php //echo $post->post_description, 200; ?><!-- ]]></description>-->
                <description><?php echo $post->post_description; ?></description>


                <content:encoded>
                    <![CDATA[

                    <!doctype html>
                    <html lang="en" prefix="op: http://media.facebook.com/op#">
                    <head>
                        <meta charset="utf-8">
                        <link rel="canonical" href="<?php echo site_url('news_details/' . $post->post_id . '/' . $post->post_category_id . '/' . $post->en_post_url) ?>">
                        <meta property="op:markup_version" content="v1.0">
                    </head>
                    <body>
                    <article>
                        <header>
                            <img src="<?php echo base_url('./image_upload/post_image/' . $post->post_image); ?>" height="348" width="620"/>
                        </header>

                        <?php echo $post->post_description; ?>

                        <footer>
                            © Copyright 2017 Developed by Shakil
                        </footer>
                    </article>
                    </body>
                    </html>



                    ]]>
                </content:encoded>

                <enclosure url="<?php echo base_url('./image_upload/post_image/' . $post->post_image); ?>" length="10240" type="image/jpg" />

            </item>


        <?php endforeach; ?>


    </channel>
</rss>
<style>

    .col-lg-6{
        height: 350px!important;
    }
</style>
<section id="mainContent">
    <div class="content_bottom">
        <?php if ($user_info != '') { ?>
        <div class="col-lg-8 col-md-8">



            <div class="business_category_left wow fadeInDown">
                <ul class="fashion_catgnav">
                    <li>
                        <div class="img-center img-thumbnail img-responsive">
                            <a href="<?php echo base_url(); ?>user_profile/index/<?php echo $user_info->user_id; ?>">
                                <?php if($user_info->oauth_provider != NULL) {?>
                                <img alt="<?php echo $user_info->user_name?>" src="<?php echo $user_info->user_avatar; ?>" class="img-responsive img-thumbnail"  width="100%">
                                <?php }else {?>
                                <img alt="<?php echo $user_info->user_name?>" src="<?php echo base_url() . $user_info->user_avatar; ?>"  width="100%">
                                <?php }?>
                                </a>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="business_category_right">
                <div class="media-body">
                    <a class="media-heading">
                        <h5><i class="fa fa-user" aria-hidden="true"></i> &nbsp; Author Name: <?php echo $user_info->user_name ?></h5>
                        <h5><i class="fa fa-envelope-square" aria-hidden="true"></i> &nbsp; Contact: <a href="mailto:<?php echo $user_info->email; ?>" target="_top"><?php echo $user_info->email; ?></a></h5>
                        <h5><i class="fa fa-bar-chart" aria-hidden="true"></i> Total Post: <?php
                        if ($user_total_post != '') {
                            echo $user_total_post;
                        } else {
                            echo 0;
                        }
                        ?></a></h5>
                    <h5><i class="fa fa-heart" aria-hidden="true"></i> Total Hits: <?php echo $total_hits->hit_count;?></a></h5>
                    <h5><i class="fa fa-calendar" aria-hidden="true"></i> User Since: <?php
                        $date = $user_info->reg_time;
                        echo date("F d, Y", strtotime($date));
                        ?></a></h5>
                    <div class="share_post">
                        <a class="facebook" target="_blank" href="<?php echo $user_info->user_fb; ?>"><i class="fa fa-facebook"></i>Facebook</a>
                        <a class="twitter" target="_blank" href="<?php echo $user_info->user_tw; ?>"><i class="fa fa-twitter"></i>Twitter</a>
                    </div>
                    <h5>
                        <b style="color: red;">USER BIO:</b><br/><br/>
                        <p><?php echo $user_info->user_bio ?></p>
                    </h5>
                    </a>
                </div>
            </div>

            <div class="content_bottom_left">
                <div class="single_category wow fadeInDown">
                    <div class="archive_style_1">

                        <h2><span class="bold_line"><span></span></span> <span class="solid_line"></span> <span
                                class="title_text">Posts of <?php echo $user_info->user_name ?></span></h2>
                        <!-- Category posts -->
                        <?php
                        if (is_array($user_post_info) && count($user_post_info) >= 1) {

                            foreach ($user_post_info as $post) {
                                ?>


                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 wow fadeInDown">
                                    <ul class="fashion_catgnav">
                                        <li>
                                            <div class="catgimg2_container">
                                                <a href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>">
                                                    <img alt="<?php echo $post->post_title; ?>" src="<?php echo base_url() . './image_upload/post_image/' . $post->post_image; ?>">
                                                </a>
                                            </div><hr/>
                                            <h2 class="catg_titile">
                                                <a href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>">
                                                    <?php
                                                    $str = $post->post_title;
                                                    $length = strlen($str);
                                                    if ($length > 100) {
                                                        $strCut = substr($str, 0, 100);
                                                        $str = substr($strCut, 0, strrpos($strCut, " ")) . "...";
                                                    }
                                                    echo $str;
                                                    ?>
                                                </a>
                                            </h2>
                                            <div class="comments_box"><span class="meta_date"><?php $date = $post->post_date;
                                                    echo date("F j, Y, g:i a", strtotime($date)); ?></span> <span
                                                    class="fa fa-user"><a href="<?php echo base_url(); ?>user_profile/index/<?php echo $post->user_id; ?>/<?php
                                                    $name = url_title($post->user_name);
                                                    echo $name;
                                                    ?>"> <?php echo $post->user_name; ?></a></span> </div>
                                            <div class="comments_box"><?php
                                                $str = $post->post_description;
                                                $length = strlen($str);
                                                if ($length > 60) {
                                                    $strCut = substr($str, 0, 60);
                                                    $str = substr($strCut, 0, strrpos($strCut, ' ')) ;
                                                }
                                                echo $str;
                                                ?>
                                                <span
                                                    class="meta_more"><a href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>">Read More...</a></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <?php
                            }
                        } else {
                            echo "<h1>&nbsp&nbsp&nbsp&nbspNo Post Available!</h1>";
                        }
                        ?>
                        <!-- End Category posts -->

                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Side Bar -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-9787339225677626"
                             data-ad-slot="9494900273"
                             data-ad-format="auto"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>

                    </div>
                    <div class="pagination_area">
                        <?php
                        foreach ($links as $link)
                            echo $link;
                        ?>
                    </div>
                </div>
            </div>


        </div>

            <?php
        } else {
            echo 'Post something to view your profile!';
        }
        ?>
        <div class="col-lg-4 col-md-4">
            <div class="content_bottom_right">
                <div class="single_bottom_rightbar">
                    <script type="text/javascript">
                        google_ad_client = "ca-pub-9787339225677626";
                        google_ad_slot = "5650740259";
                        google_ad_width = 336;
                        google_ad_height = 280;
                    </script>
                    <!-- Ad 1 -->
                    <script type="text/javascript"
                            src="//pagead2.googlesyndication.com/pagead/show_ads.js">
                    </script>
                    <br/>
                    <!--<iframe height="350" frameborder="5" style="vertical-align: bottom;" src="//tools.whatsthescore.com/tools.php?id=37615&iframe=tap-sport-tools-37615&tz-js=Asia/Dhaka"></iframe>
                  -->
                </div>

                <div class="single_bottom_rightbar">
                    <h1><strong>জনপ্রিয় খেলার সংবাদ</strong></h1>
                    <ul class="small_catg popular_catg wow fadeInDown">
                        <?php if (is_array($popular_post) && count($popular_post) >= 1) { ?>

                            <?php foreach ($popular_post as $post) { ?>

                                <li>
                                    <div class="media wow fadeInDown"><a href="#" class="media-left">
                                            <img alt="<?php $date = $post->post_title; ?>"
                                                 src="<?php echo base_url() . './image_upload/post_image/thumbs/' . $post->post_image; ?>">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a
                                                    href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>"><?php echo $post->post_title; ?></a>
                                            </h4>
                                            <div class="comments_box">
                                    <span class="meta_date">
                                        <?php $date = $post->post_date;
                                        echo date("M d", strtotime($date)); ?>
                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        } else {
                            echo "<h4>&nbsp&nbsp&nbsp&nbspNo Post Available!</h4>";
                        }
                        ?>
                    </ul>

                </div>
                <div class="single_bottom_rightbar">
                    <h1><strong>সর্বশেষ খেলার সংবাদগুলো</strong></h1>
                    <ul class="small_catg popular_catg wow fadeInDown">
                        <?php if (is_array($recent_post) && count($recent_post) >= 1) { ?>

                            <?php foreach ($recent_post as $post) { ?>

                                <li>
                                    <div class="media wow fadeInDown"><a href="#" class="media-left">
                                            <img alt="<?php $date = $post->post_title; ?>"
                                                 src="<?php echo base_url() . './image_upload/post_image/thumbs/' . $post->post_image; ?>">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a
                                                    href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>"><?php echo $post->post_title; ?></a>
                                            </h4>
                                            <div class="comments_box">
                                    <span class="meta_date">
                                        <?php $date = $post->post_date;
                                        echo date("M d", strtotime($date)); ?>
                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        } else {
                            echo "<h4>&nbsp&nbsp&nbsp&nbspNo Post Available!</h4>";
                        }
                        ?>
                    </ul>

                </div>
                <div class="single_bottom_rightbar wow fadeInDown">
                    <h2>Popular Tags</h2>
                    <ul>
                        <li><a href="#">Cristiano Ronaldo</a></li>
                        <li><a href="#">Lionel Messi</a></li>
                        <li><a href="#">Neymar</a></li>
                        <li><a href="#">Suarez</a></li>
                        <li><a href="#">Modric</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
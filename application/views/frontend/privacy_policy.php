<section id="mainContent">
    <div class="content_bottom">
        <div class="col-lg-8 col-md-8">
            <div class="content_bottom_left">
                <div class="single_category wow fadeInDown">
                    <div class="archive_style_1">

                        <h2><span class="bold_line"><span></span></span> <span class="solid_line"></span> <span
                                class="title_text">Privacy Policy</span></h2>
                        <p>1. Our Articles written by freelancer writer. these content can not be use anywhere other than our website.</p>
                        <p>2. If there is any copyright claim, let us know, we will remove as soon as possible.</p>

                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-4 col-md-4">
            <div class="content_bottom_right">
                <div class="single_bottom_rightbar">

                    <!--<iframe height="350" frameborder="5" style="vertical-align: bottom;" src="//tools.whatsthescore.com/tools.php?id=37615&iframe=tap-sport-tools-37615&tz-js=Asia/Dhaka"></iframe>
                  -->
                </div>
                <div class="single_bottom_rightbar">
                    <h2>Popular Post</h2>
                    <ul class="small_catg popular_catg wow fadeInDown">
                        <?php if (is_array($popular_post) && count($popular_post) >= 1) { ?>

                            <?php foreach ($popular_post as $post) { ?>

                                <li>
                                    <div class="media wow fadeInDown"><a href="#" class="media-left">
                                            <img alt="<?php $date = $post->post_title; ?>"
                                                 src="<?php echo base_url() . './image_upload/post_image/thumbs/' . $post->post_image; ?>">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a
                                                    href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>"><?php echo $post->post_title; ?></a>
                                            </h4>
                                            <div class="comments_box">
                                    <span class="fa fa-calendar-o">
                                        <?php $date = $post->post_date;
                                        echo date("M d", strtotime($date)); ?>
                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        } else {
                            echo "<h4>&nbsp&nbsp&nbsp&nbspNo Post Available!</h4>";
                        }
                        ?>
                    </ul>

                </div>
                <div class="single_bottom_rightbar">
                    <h2>Latest Post</h2>
                    <ul class="small_catg popular_catg wow fadeInDown">
                        <?php if (is_array($recent_post) && count($recent_post) >= 1) { ?>

                            <?php foreach ($recent_post as $post) { ?>

                                <li>
                                    <div class="media wow fadeInDown"><a href="#" class="media-left">
                                            <img alt="<?php $date = $post->post_title; ?>"
                                                 src="<?php echo base_url() . './image_upload/post_image/thumbs/' . $post->post_image; ?>">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a
                                                    href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>"><?php echo $post->post_title; ?></a>
                                            </h4>
                                            <div class="comments_box">
                                    <span class="fa fa-calendar-o">
                                        <?php $date = $post->post_date;
                                        echo date("M d", strtotime($date)); ?>
                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        } else {
                            echo "<h4>&nbsp&nbsp&nbsp&nbspNo Post Available!</h4>";
                        }
                        ?>
                    </ul>

                </div>
                <div class="single_bottom_rightbar wow fadeInDown">
                    <h2>Popular Tags</h2>
                    <ul>
                        <li><a href="#">Cristiano Ronaldo</a></li>
                        <li><a href="#">Lionel Messi</a></li>
                        <li><a href="#">Neymar</a></li>
                        <li><a href="#">Suarez</a></li>
                        <li><a href="#">Modric</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
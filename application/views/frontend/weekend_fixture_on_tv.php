
<section id="ContactContent">
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="contact_area single_page_area">
                <h1><strong>খেলার সময়সূচী</strong></h1>
                <br/><br/>
                <?php $gw_info = $this->db->order_by('gw_id','desc')->limit(1)->get('tbl_gwtitle')->result(); foreach ($gw_info as $row1) {?>
                <h2 class="post-titile"><?php echo $row1->game_week?></h2>
                <table class="table table-responsive table-striped">
                    <?php
                    foreach ($match_fixture as $row) {?>
                    <thead>
                    <tr style="font-weight: bolder; color: #5a0099;" class="success">
                        <th>League</th>
                        <th>Match</th>
                        <th>Time</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr class="warning">
                        <th scope="row"><?php echo $row->match_league;?></th>
                        <td><?php echo $row->match_title;?></td>
                        <td><?php $date = date('M j, Y - g:i a', $row->match_time); echo $date;?> (BD Time)</td>
                    </tr>
                    <tr>
                        <th class="danger" colspan="3"><b style="color: red">#Telecast on:</b> <?php echo $row->match_channel;?></th>
                    </tr>

                    </tbody>
                    <?php } ?>
                </table>
                <?php } ?>

                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Banner Ad -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-9787339225677626"
                     data-ad-slot="3823771198"
                     data-ad-format="link"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>

            </div>

        </div>
        <div class="col-lg-4 col-md-4">
            <div class="content_bottom_right">
                <div class="single_bottom_rightbar">

                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Side Bar -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-9787339225677626"
                         data-ad-slot="9494900273"
                         data-ad-format="auto"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>


                    <br/>
                    <h2>Popular Post</h2>
                    <ul class="small_catg popular_catg wow fadeInDown">
                        <?php if (is_array($popular_post) && count($popular_post) >= 1) { ?>

                            <?php foreach ($popular_post as $post) { ?>

                                <li>
                                    <div class="media wow fadeInDown"><a href="#" class="media-left">
                                            <img alt="<?php $date = $post->post_title; ?>"
                                                 src="<?php echo base_url() . './image_upload/post_image/thumbs/' . $post->post_image; ?>">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a
                                                        href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>"><?php echo $post->post_title; ?></a>
                                            </h4>
                                            <div class="comments_box">
                                    <span class="fa fa-calendar-o">
                                        <?php $date = $post->post_date;
                                        echo date("M d", strtotime($date)); ?>
                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        } else {
                            echo "<h4>&nbsp&nbsp&nbsp&nbspNo Post Available!</h4>";
                        }
                        ?>
                    </ul>

                </div>

            </div>
        </div>
    </div>
</section>


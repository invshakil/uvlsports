<section id="mainContent">
    <div class="content_bottom">
        <div class="col-lg-8 col-md-8">
            <div class="content_bottom_left">
                <div class="single_page_area">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li><a href="#"><?php echo $post_info->category_name; ?></a></li>
                        <li class="active"><?php echo $post_info->post_title; ?></li>
                    </ol>

                    <h1 class="post_titile"><strong><?php echo $post_info->post_title; ?></strong></h1>
                    <img class="img-center img-thumbnail img-responsive"
                         src="<?php echo base_url() . './image_upload/post_image/' . $post_info->post_image; ?>"
                         alt="<?php echo $post_info->post_title; ?>">
                    <hr/>

                    <div class="single_page_content" align="justify">
                        <div class="post_commentbox">
                            <a href="<?php echo base_url(); ?>user-profile/index/<?php echo $post_info->user_id; ?>">
                                <i class="fa fa-user"></i><?php echo $post_info->user_name; ?>
                            </a>
                            <span><i class="fa fa-calendar"></i>
                                <?php $date = $post_info->post_date;
                                echo date("F j, Y, g:i a", strtotime($date));
                                ?>
                            </span>
                            <a href="#"><i class="fa fa-tags"></i><?php echo $post_info->category_name; ?></a>
                        </div>

                        <div id="content">
                            <?php echo $post_info->post_description; ?>
                        </div>

                        <br/>


                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Banner Ad -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-9787339225677626"
                             data-ad-slot="3823771198"
                             data-ad-format="link"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>


                        <br/>

                        <div class="single_page_content">
                            <a href="#"><i class="fa fa-tags"></i> Tags: <?php if ($post_info->post_tag_id == '') {
									foreach ($tag_info as $tags) {
										echo '<button class="btn btn-success">' . $tags->tag_name . " </button> ";
									}
		
								} else {
                                    $tags = explode(',',$post_info->post_tag_id);
                                    
                                    foreach ($tags as $tag){
										echo '<button class="btn btn-success">' . $tag . " </button> ";
                                    }
                                }

                                ?></a> <br/>
                            <hr/>
                            <span class="fa fa-eye"><a
                                        href="#">&nbsp;&nbsp;Viewed: <?php echo $post_info->hit_count; ?>
                                    Times</a></span>&nbsp;&nbsp;
                            <div class="fb-like"
                                 data-href="<?php echo base_url(); ?>news-details/<?php echo $post_info->post_id; ?>/<?php echo $post_info->post_category_id; ?>/<?php echo $post_info->en_post_url; ?>"
                                 data-width="320" data-layout="standard" data-action="like" data-size="small"
                                 data-show-faces="false">
                            </div>
                            <hr/>

                        </div>


                    </div>

                </div>

                <style>
                    .alert-danger{
                        background-color: red;
                        color: white;
                    }
                </style>

                <br/><br/>
                <div class="single_page_content">
                    <p class="alert alert-danger"><b>You may want to read this:</b></p>
                    <ul>
                        <?php foreach ($read_more_articles as $post) { ?>
                            <li style="font-size: 15px;">
                                <a href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>"
                                   target="_blank"><i class="fa fa-window-maximize" aria-hidden="true"></i>
                                    &nbsp; <?php echo $post->post_title; ?></a><br/>

                            </li>
                        <?php } ?>
                    </ul>

                </div>


            </div>

            <div class="share_post">
                <a class="facebook" target="_blank"
                   href="https://www.facebook.com/sharer.php?u=<?php echo base_url(); ?>news-details/<?php echo $post_info->post_id; ?>/<?php echo $post_info->post_category_id; ?>/<?php echo $post_info->en_post_url; ?>"><i
                            class="fa fa-facebook"></i>Facebook</a>
                <a class="twitter" target="_blank"
                   href="https://twitter.com/home?status=<?php echo $post_info->post_title ?>&<?php echo base_url(); ?>news-details/<?php echo $post_info->post_id; ?>/<?php echo $post_info->post_category_id; ?>/<?php echo $post_info->en_post_url; ?>"><i
                            class="fa fa-twitter"></i>Twitter</a>
                <a class="googleplus" target="_blank"
                   href="https://plus.google.com/share?url=<?php echo base_url(); ?>news-details/<?php echo $post_info->post_id; ?>/<?php echo $post_info->post_category_id; ?>/<?php echo $post_info->en_post_url; ?>"><i
                            class="fa fa-google-plus"></i>Google+</a>
                <a class="linkedin" target="_blank"
                   href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url(); ?>news-details/<?php echo $post_info->post_id; ?>/<?php echo $post_info->post_category_id; ?>/<?php echo $post_info->en_post_url; ?>"><i
                            class="fa fa-linkedin"></i>LinkedIn</a>
                <a class="pinterest" target="_blank"
                   href="https://pinterest.com/pin/create/button/?url=<?php echo base_url(); ?>news-details/<?php echo $post_info->post_id; ?>/<?php echo $post_info->post_category_id; ?>/<?php echo $post_info->en_post_url; ?>"><i
                            class="fa fa-pinterest"></i>Pinterest</a>
            </div>
            <div class="fb-comments"
                 data-href="<?php echo base_url(); ?>news-details/<?php echo $post_info->post_id; ?>/<?php echo $post_info->post_category_id; ?>/<?php echo $post_info->en_post_url; ?>"
                 data-width="600" data-numposts="5"></div>

            <div class="similar_post">
                <h2>Similar Post You May Like <i class="fa fa-thumbs-o-up"></i></h2>
                <ul class="small_catg similar_nav wow fadeInDown animated">
                    <?php if (is_array($similar_post) && count($similar_post) >= 1) { ?>

                        <?php foreach ($similar_post as $post) { ?>

                            <li>
                                <div class="media wow fadeInDown animated"
                                     style="visibility: visible; animation-name: fadeInDown;">
                                    <a class="media-left related-img"
                                       href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>">
                                        <img class="img-responsive img-thumbnail"
                                             src="<?php echo base_url() . './image_upload/post_image/resized/' . $post->post_image; ?>"
                                             alt="<?php echo $post->post_title ?>"></a>
                                    <div class="media-body">
                                        <h4 class="media-heading"><a
                                                    href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>">
                                                <?php
                                                $str = $post->post_title;
                                                $length = strlen($str);
                                                if ($length > 150) {
                                                    $strCut = substr($str, 0, 150);
                                                    $str = substr($strCut, 0, strrpos($strCut, " ")) . "...";
                                                }
                                                echo $str;
                                                ?>
                                            </a></h4>
                                    </div>
                                </div>
                            </li>
                        <?php }
                    } ?>
                </ul>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="content_bottom_right">
                <div class="single_bottom_rightbar">


                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Side Bar -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-9787339225677626"
                         data-ad-slot="9494900273"
                         data-ad-format="auto"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>


                    <br/>
                    <h1><strong>সর্বশেষ খেলার সংবাদগুলো</strong></h1>
                    <ul class="small_catg popular_catg wow fadeInDown">
                        <?php if (is_array($recent_post) && count($recent_post) >= 1) { ?>

                            <?php foreach ($recent_post as $post) { ?>

                                <li>
                                    <div class="media wow fadeInDown"><a href="#" class="media-left">
                                            <img alt="<?php $date = $post->post_title; ?>"
                                                 class="img-responsive img-thumbnail"
                                                 src="<?php echo base_url() . './image_upload/post_image/thumbs/' . $post->post_image; ?>">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a
                                                        href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>"><?php echo $post->post_title; ?></a>
                                            </h4>
                                            <div class="comments_box">
                                    <span class="fa fa-calendar-o">
                                        <?php $date = $post->post_date;
                                        echo date("M d", strtotime($date)); ?>
                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        } else {
                            echo "<h4>&nbsp&nbsp&nbsp&nbspNo Post Available!</h4>";
                        }
                        ?>
                    </ul>
                </div>
                <div class="single_bottom_rightbar">
                    <h1><strong>জনপ্রিয় খেলার সংবাদ</strong></h1>
                    <ul class="small_catg popular_catg wow fadeInDown">
                        <?php if (is_array($popular_post) && count($popular_post) >= 1) { ?>

                            <?php foreach ($popular_post as $post) { ?>

                                <li>
                                    <div class="media wow fadeInDown"><a href="#" class="media-left">
                                            <img alt="<?php $date = $post->post_title; ?>"
                                                 class="img-responsive img-thumbnail"
                                                 src="<?php echo base_url() . './image_upload/post_image/thumbs/' . $post->post_image; ?>">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a
                                                        href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>"><?php echo $post->post_title; ?></a>
                                            </h4>
                                            <div class="comments_box">
                                    <span class="fa fa-calendar-o">
                                        <?php $date = $post->post_date;
                                        echo date("M d", strtotime($date)); ?>
                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        } else {
                            echo "<h4>&nbsp&nbsp&nbsp&nbspNo Post Available!</h4>";
                        }
                        ?>
                    </ul>

                </div>
                <div class="single_bottom_rightbar wow fadeInDown">
                    <h2>Find us on Facebook</h2>
                    <iframe src="https://www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/uvlsportsofficial/&amp;width=285&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=false&amp;height=400"
                            scrolling="yes" frameborder="0" allowtransparency="true"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
    .single_page_content img{
        margin-right: 20px;
    }
</style>

<div id="fb-root"></div>

<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script>
    $(document).ready(function () {
        $('img').addClass('img-responsive img-thumbnail').css({
            width: 'auto',
            height: 'auto'
        });

    });

    $(document).ready(function () {
//        if (typeof YOUTUBE_VIDEO_MARGIN == 'undefined') {
//            YOUTUBE_VIDEO_MARGIN = 5;
//        }
//        $('iframe').each(function (index, item) {
//            if ($(item).attr('src').match(/(https?:)?\/\/www\.youtube\.com/)) {
//                var w = $(item).attr('width');
//                var h = $(item).attr('height');
//                var ar = h / w * 100;
//                ar = ar.toFixed(2);
//                //Style iframe
//                $(item).css('position', 'absolute');
//                $(item).css('top', '0');
//                $(item).css('left', '0');
//                $(item).css('width', '100%');
//                $(item).css('height', '100%');
//                $(item).css('max-width', w + 'px');
//                $(item).css('max-height', h + 'px');
//                $(item).wrap('<div style="max-width:' + w + 'px;margin:0 auto; padding:' + YOUTUBE_VIDEO_MARGIN + 'px;" />');
//                $(item).wrap('<div style="position: relative;padding-bottom: ' + ar + '%; height: 0; overflow: hidden;" />');
//            }
//        });
    });
</script>

<script>(function (d, s, id) {

        var js, fjs = d.getElementsByTagName(s)[0];

        if (d.getElementById(id))
            return;

        js = d.createElement(s);

        js.id = id;

        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";

        fjs.parentNode.insertBefore(js, fjs);

    }(document, 'script', 'facebook-jssdk'));
</script>
<!--<script src="https://cdn.rawgit.com/skipser/youtube-autoresize/master/youtube-autoresizer.js"></script>-->

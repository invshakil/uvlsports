<?php
$latest_headline = $this -> welcome_model -> select_all_titles();

$user_id = $this -> session -> userdata('user_id');
$user_name = $this -> session -> userdata('user_name');

$total_published_post_by_user = $this -> super_admin_extra -> select_all_published_post_by_user($user_id);

$acess_level = $this -> session -> userdata('access_level');
?>

<!DOCTYPE html>
<html>
<head xmlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#">
    <title><?php echo $title; ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta property="fb:app_id" content="287256215072572"/>
    <meta property="og:site_name" content="Universal Sports"/>
    <meta property="og:url" content="<?php echo $url ?>"/>

    <meta property="og:title" content="<?php echo $title ?>"/>
	<?php if ($image != FALSE) { ?>
        <meta property="og:image" content="<?php echo $image; ?>"/>
	<?php } ?>
	<?php if ($description != FALSE) { ?>

        <meta property="og:description" content="<?php echo $description ?>"/>
        <meta name="description" content="<?php echo $description ?>">
	<?php } ?>
    
    <?php if (isset($tags_info)) {?>
        <meta name="keyword" content="<?php echo $tags_info?>">

	<?php } elseif (isset($tag_info)) { ?>

        <meta name="keyword" content="<?php foreach ($tag_info as $tag) {
			echo $tag -> tag_name . ', ';
		} ?><?php echo $keyword; ?>">

	<?php } else {
		echo '<meta name="keyword" content="' . $keyword . '">';
	} ?>
    <meta property="og:locale" content="en_US"/>
	<?php if (isset($author_info)) { ?>

        <meta property="og:type" content="article"/>
        <meta name="author" content="<?php echo $author_info ?>"/>
        <meta property="article:author" content="<?php echo $author_info ?>"/>

	<?php } else {
		echo '<meta property="og:type" content="website" />';
		echo '<meta name="author" content="https://www.facebook.com/1360157000709953">';
//        echo '<meta property="article:author" content="https://www.facebook.com/1360157000709953">';
	} ?>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>webAssets/css/bootstrap-v1.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>webAssets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>webAssets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>webAssets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>webAssets/css/theme.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>webAssets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>webAssets/css/css2.css" type="text/css" media="screen">
    <link href="https://fonts.maateen.me/solaiman-lipi/font.css" rel="stylesheet">

    <style type="text/css">
        .loader_image {
            padding: 10px 0px;
            width: 100%;
        }

        .sticky {
            position: fixed;
            width: 100%;
            left: 0;
            top: 0;
            background-color: #4169E1;
            z-index: 100;
            border-top: 0;
        }

        #sideDivEffect{
            border-radius: 5px!important;
            /*background-color: #ffffff!important;*/
            padding-top: 10px;
            padding-right: 5px;
            /*box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 20px rgba(0, 0, 0, 0.2) inset;*/
            margin-bottom: 10px;
            -moz-box-shadow: 1px 1px 3px 2px #DCDCDC;
            -webkit-box-shadow: 1px 1px 3px 2px #DCDCDC;
            box-shadow: 1px 1px 3px 2px #DCDCDC;
            margin-left: 5px;
        }
        
        .catgimg2_container > a > img
        {
            padding-top: 10px!important;
        }

        .small_catg > li, popular_catg > li {
            border-radius: 5px!important;
            /*background-color: #ffffff!important;*/
            padding-top: 10px;
            padding-right: 5px;
            /*box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 20px rgba(0, 0, 0, 0.2) inset;*/
            margin-bottom: 10px;
            -moz-box-shadow: 1px 1px 3px 2px #DCDCDC;
            -webkit-box-shadow: 1px 1px 3px 2px #DCDCDC;
            box-shadow: 1px 1px 3px 2px #DCDCDC;
            margin-left: 5px;
        }
        
        .media a {
            padding-left: 5px!important;
        }

    </style>

    <meta property="fb:pages" content="1360157000709953"/>

    <!--[if lt IE 9]>
    <script src="<?php echo base_url(); ?>webAssets/js/html5shiv.min.js"></script>
    <script src="<?php echo base_url(); ?>webAssets/js/respond.min.js"></script>
    <![endif]-->

    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-9787339225677626",
            enable_page_level_ads: true
        });
    </script>

</head>
<body>
<!--<div id="preloader">-->
<!--    <div id="status">&nbsp;</div>-->
<!--</div>-->
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
<div class="container">
    <header id="header">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="header_top">
                    <div class="header_top_left">
						<?php if ($this -> session -> userdata('user_id') == NULL) { ?>

                            <ul class="top_nav">
                                <li><a href="<?php echo base_url(); ?>sign-up">Sign
                                        up</a></li>
                                <li><a href="<?php echo base_url(); ?>sign-in">Sign
                                        in</a></li>
                            </ul>
						<?php } else { ?>

                        <ul class="top_nav">
                            <!--Checking if admin or user to go add post url -->

							<?php if ($acess_level == 3) { ?>
                                <li><a
                                            href="<?php echo base_url(); ?>user_dashboard/add_post" target="blank">Add
                                        Post</a></li>
							<?php } else { ?>
                                <li><a
                                            href="<?php echo base_url(); ?>post_controller/add_post"
                                            target="blank">Add
                                        Post</a></li>
							<?php } ?>

							<?php if ($total_published_post_by_user != 0) { ?>
                                <li><a
                                            href="<?php echo base_url(); ?>user_profile/index/<?php echo $this -> session -> userdata('user_id'); ?>"
                                            target="blank">View Profile</a>
                                </li>
							<?php } ?>

                            <!--Checking if admin or user to go add post url -->
							<?php if ($acess_level == 3) { ?>
                                <li><a href="<?php echo base_url(); ?>user_dashboard"
                                       target="blank">Logged in
                                        as <?php echo $this -> session -> userdata('user_name') ?>
                                        || DASHBOARD</a></li>
							<?php } else { ?>
                                <li><a href="<?php echo base_url(); ?>super_admin_panel"
                                       target="blank">Logged in
                                        as <?php echo $this -> session -> userdata('user_name') ?>
                                        || DASHBOARD</a></li>
							<?php } ?>

							<?php if ($this -> session -> userdata('login_type') == 1) { ?>
                                <li><a href="<?php echo base_url(); ?>social_verification/logout"
                                       target="_blank">Logout</a></li>
							<?php } else { ?>
								<?php if ($this -> session -> userdata('access_level') != 3) { ?>
                                    <li><a href="<?php echo base_url(); ?>super_admin_panel/logout" target="_blank">Logout</a>
                                    </li>
								<?php } else { ?>
                                    <li><a href="<?php echo base_url(); ?>user_dashboard/logout"
                                           target="_blank">Logout</a></li>
								<?php }
							} ?>


							<?php } ?>
                        </ul>
                    </div>
                    <div class="header_top_right">
                        <form class="search_form" action="<?php echo base_url(); ?>search-result" method="get">

                            <input name="search_data" id="search_data" type="text" placeholder="Search Article"
                                   onkeyup="liveSearch()" autocomplete="off">
                            <input name="submit" type="submit" value="">
                        </form>
                    </div>
                </div>

                <div class="header_bottom">
                    <div class="header_bottom_left" style="padding-top: 20px;"><a class="logo"
                                                                                  href="<?php echo base_url(); ?>">UNIVERSAL<strong>SPORTS</strong>
                            <span>AN FFBD EFFORT</span></a></div>
                    <div class="header_bottom_right">
                        <div class="pull-right">

                            <script type="text/javascript">
                                google_ad_client = "ca-pub-9787339225677626";
                                google_ad_slot = "9862689649";
                                google_ad_width = 728;
                                google_ad_height = 90;
                            </script>
                            <!-- top banner -->
                            <script type="text/javascript"
                                    src="//pagead2.googlesyndication.com/pagead/show_ads.js">
                            </script>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </header>
    <header class="header-top" id="header">
        <header class="right clearfix" id="suggestions">
            <!--<div id="suggestions">-->
            <ul class="list-group" id="autoSuggestionsList">
            </ul>
            <!--</div>-->
        </header>
    </header>
    <div id="navarea">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar"><span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                    </button>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav custom_nav">
                        <li class="<?php if ($title === 'Home || Universal Sports') echo 'active' ?>"><a
                                    href="<?php echo base_url(); ?>">হোম</a></li>
                        <li class="<?php if ($title === 'Latest Short Stories || Universal Sports') echo 'active' ?>"><a
                                    href="<?php echo base_url(); ?>latest-short-stories">খেলার সর্বশেষ সংবাদ</a></li>
                        <li class="<?php if ($title === 'Weekend Fixture on TV || Universal Sports') echo 'active' ?>">
                            <a href="<?php echo base_url(); ?>tv-schedule">খেলার সময়সূচী</a></li>
                        <li><a href="#" class="" data-toggle="dropdown" role="button"
                               aria-expanded="false">ইউরোপিয়ান লিগ</a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo base_url(); ?>english-premier-league">ইংলিশ প্রিমিয়ার লিগ</a>
                                </li>
                                <li><a href="<?php echo base_url(); ?>laliga-santander">লা লিগা</a></li>
                                <li><a href="<?php echo base_url(); ?>serie-a">সেরি-আ</a></li>
                                <li><a href="<?php echo base_url(); ?>other-leagues">চ্যাম্পিয়ন্স লিগ ও অন্যান্য</a>
                                </li>
                                <li><a href="<?php echo base_url(); ?>national-teams">জাতীয় দলের সংবাদ</a></li>
                            </ul>
                        </li>
                        <li><a href="#" class="" data-toggle="dropdown" role="button"
                               aria-expanded="false">ফিচারড আর্টিক্যাল</a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo base_url(); ?>fantasy-football">ফুটবল ফ্যান্টাসি</a></li>
                                <li><a href="<?php echo base_url(); ?>articles">আর্টিক্যাল</a></li>
                                <li><a href="<?php echo base_url(); ?>users-column">পাঠকের মতামত</a></li>
                                <li><a href="<?php echo base_url(); ?>bangladesh">বাংলাদেশ</a></li>
                            </ul>
                        </li>
                        <li><a href="#" class="" data-toggle="dropdown" role="button"
                               aria-expanded="false">খেলার সময়সূচী ও পয়েন্ট তালিকা</a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo base_url(); ?>league-fixture">খেলার সময়সূচী</a></li>
                                <li><a href="<?php echo base_url(); ?>point-table">পয়েন্ট তালিকা</a></li>
                            </ul>
                        </li>
                        <li <?php if ($title === 'Cricket || Universal Sports') echo 'active' ?>><a
                                    href="<?php echo base_url(); ?>cricket">ক্রিকেট</a></li>
                        <!--                            <li><a href="-->
						<?php //echo base_url(); ?><!--welcome/twitter">Twitter</a></li>-->
                        <li <?php if ($title === 'Contact || Universal Sports') echo 'active' ?>><a
                                    href="<?php echo base_url(); ?>contact">মতামত</a></li>

                    </ul>
                </div>
            </div>


        </nav>

    </div>
    <section id="mainContent">
        <!--INNER CONTENT -->
		<?php echo $maincontent; ?>
        <!--INNER CONTENT -->
    </section>
</div>
<footer id="footer">
    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="single_footer_top wow fadeInLeft">

                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Side Bar -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-9787339225677626"
                             data-ad-slot="9494900273"
                             data-ad-format="auto"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>

                        <br/>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="single_footer_top wow fadeInDown">
                        <h2>ফেসবুক ফ্যান পেইজ</h2>
                        <iframe src="https://www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/uvlsportsofficial/&amp;width=285&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=false&amp;height=400"
                                scrolling="yes" frameborder="0" allowtransparency="true"></iframe>

                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="single_footer_top wow fadeInRight">
                        <h2>About Us</h2>
                        <p align="justify">We are a small portion of sports enthusiast from a small yet beautiful
                            country called Bangladesh. People are very passionate and enthusiastic about sports in our
                            country, particularly for Cricket and Football.
                            We intend to deliver all the latest updates and analysis about both the sport and create a
                            strong platform for sports lovers to write their heart out about their favorite one. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="footer_bottom_left">
                        <p>Copyright &copy; 2017 <a href="https://www.sshakil.com/" target="_blank">FFBD</a></p>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="footer_bottom_right" style="color: white!important;
                        font-size: 14px;
                        font-weight: bold;">
                        <b><a href="https://www.uvlsports.com/robots.txt">Robot</a></b>&nbsp;|&nbsp;
                        <b><a href="https://www.uvlsports.com/sitemap.xml">Sitemap</a></b>&nbsp;|&nbsp;
                        <b> <a href="https://www.sshakil.com/" target="_blank">Developed By Saif Shakil</a> </b>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="<?php echo base_url(); ?>webAssets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>webAssets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>webAssets/js/wow.min.js"></script>
<script src="<?php echo base_url(); ?>webAssets/js/slick.min.js"></script>
<script src="<?php echo base_url(); ?>webAssets/js/custom.js"></script>

<div id="fb-root"></div>

<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<script>window.twttr = (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
            t = window.twttr || {};
        if (d.getElementById(id))
            return t;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);

        t._e = [];
        t.ready = function (f) {
            t._e.push(f);
        };

        return t;
    }(document, "script", "twitter-wjs"));</script>

<script>
    function liveSearch() {
        var input_data = $('#search_data').val();
        if (input_data.length === 0) {
            $('#suggestions').hide();
        } else {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>welcome/search",
                data: {search_data: input_data},
                success: function (data) {
                    // return success
                    if (data.length > 0) {
                        $('#suggestions').show();
                        $('#autoSuggestionsList').addClass('auto_list');
                        $('#autoSuggestionsList')(data);
                    }
                }
            });
        }
    }
</script>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-44033160-2', 'auto');
    ga('send', 'pageview');

</script>

<script>
    $(document).ready(function () {
        // grab the initial top offset of the navigation
        var stickyNavTop = $('#navarea').offset().top;

        // our function that decides weather the navigation bar should have "fixed" css position or not.
        var stickyNav = function () {
            var scrollTop = $(window).scrollTop(); // our current vertical position from the top

            // if we've scrolled more than the navigation, change its position to fixed to stick to top,
            // otherwise change it back to relative
            if (scrollTop > stickyNavTop) {
                $('.nav').addClass('sticky').css({
                    opacity: .7
                });
            } else {
                $('.nav').removeClass('sticky').css({
                    opacity: 1
                });
            }
        };

        stickyNav();
        // and run it again every time you scroll
        $(window).scroll(function () {
            stickyNav();
        });
    });
</script>

</body>
</html>
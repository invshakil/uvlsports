<section id="ContactContent">
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="contact_area single_page_area">
                <h1 class="post_titile"><strong>আমাদের সম্পর্কে</strong></h1>
                <div class="single_post_content">
                    <p>We are a small portion of sports enthusiast from a small yet beautiful country called Bangladesh.
                        People are very passionate and enthusiastic about sports in our country, particularly for
                        Cricket and Football. We intend to deliver all the latest updates and analysis about both the
                        sport and create a strong platform for sports lovers to write their heart out about their
                        favorite one.</p>

                    <p>Our mission is to provide our audience with all the latest news and updates of Football and
                        Cricket all around the world.</p>

                    <p>Our vision is to become one of the most reliable and resourceful websites for sports in the whole
                        world.</p>
                </div>

                <div class="contact_bottom">
                    <div class="contact_us wow fadeInRightBig">
                        <h2>Contact Us</h2>
                        <?php $msg = $this->session->userdata('message');
                        if (isset($msg)) {
                        ?>
                        <h3><?php echo $msg;
                            $this->session->unset_userdata('message');
                            } ?></h3>
                        <form action="<?php echo base_url() ?>welcome/save_contact_sms" class="contact_form"
                              method="post">
                            <input class="form-control" type="text" name="sender_name" placeholder="Name(required)"
                                   required>
                            <input class="form-control" type="email" name="sender_email" placeholder="E-mail(required)"
                                   required>
                            <input class="form-control" type="text" name="sender_subject"
                                   placeholder="Subject(required)" required>
                            <textarea class="form-control" cols="30" name="sender_message" rows="10"
                                      placeholder="Message(required)" required></textarea>
                            <input type="submit" class="btn btn-danger" value="SEND MESSAGE">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="content_bottom_right">
                <div class="single_bottom_rightbar">
                    <h1><strong>জনপ্রিয় খেলার সংবাদ</strong></h1>
                    <ul class="small_catg popular_catg wow fadeInDown">
                        <?php if (is_array($popular_post) && count($popular_post) >= 1) { ?>

                            <?php foreach ($popular_post as $post) { ?>

                                <li>
                                    <div class="media wow fadeInDown"><a href="#" class="media-left">
                                            <img alt="<?php $date = $post->post_title; ?>"
                                                 src="<?php echo base_url() . './image_upload/post_image/thumbs/' . $post->post_image; ?>">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a
                                                        href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>"><?php echo $post->post_title; ?></a>
                                            </h4>
                                            <div class="comments_box">
                                    <span class="fa fa-calendar-o">
                                        <?php $date = $post->post_date;
                                        echo date("M d", strtotime($date)); ?>
                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        } else {
                            echo "<h4>&nbsp&nbsp&nbsp&nbspNo Post Available!</h4>";
                        }
                        ?>
                    </ul>

                </div>

            </div>
        </div>
    </div>
</section>
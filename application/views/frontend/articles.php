<section id="mainContent">
    <div class="content_bottom">
        <div class="col-lg-8 col-md-8">
            <div class="content_bottom_left">
                <div class="single_category wow fadeInDown">
                    <div class="archive_style_1">

                        <h1><span class="bold_line"><span></span></span> <span class="solid_line"></span> <span
                                class="title_text"><strong><?php echo $headline ?></strong></span></h1>
                        <!-- Category posts -->
                        <?php
                        if (is_array($all_post_from_league) && count($all_post_from_league) >= 1) {

                        foreach ($all_post_from_league as $post) {
                        ?>

                            <style>

                                .col-lg-6{
                                    height: 350px!important;
                                }
                            </style>

                        <div class="col-lg-6 col-md-6 col-sm-6 wow fadeInDown " id="sideDivEffect" style="width: 48%;height: 320px!important;">
                            <ul class="fashion_catgnav">
                                <li>
                                    <div class="catgimg2_container">
                                        <a href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>">
                                            <img alt="<?php echo $post->post_title; ?>" class="img-responsive img-thumbnail" src="<?php echo base_url() . './image_upload/post_image/' . $post->post_image; ?>">
                                        </a>
                                    </div><hr/>
                                    <h2 class="catg_titile">
                                        <a href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>">
                                            <?php
                                            $str = $post->post_title;
                                            $length = strlen($str);
                                            if ($length > 100) {
                                                $strCut = substr($str, 0, 100);
                                                $str = substr($strCut, 0, strrpos($strCut, " ")) . "...";
                                            }
                                            echo $str;
                                            ?>
                                        </a>
                                    </h2>
                                    <div class="comments_box"><span class="meta_date"><?php $date = $post->post_date;
                                            echo date("F j, Y, g:i a", strtotime($date)); ?></span> <span
                                            class="fa fa-user"><a href="<?php echo base_url(); ?>user_profile/index/<?php echo $post->user_id; ?>/<?php
                                            $name = url_title($post->user_name);
                                            echo $name;
                                            ?>"> <?php echo $post->user_name; ?></a></span> </div>
                                </li>
                            </ul>
                        </div>

                            <?php
                        }
                        } else {
                            echo "<h1>&nbsp&nbsp&nbsp&nbspNo Post Available!</h1>";
                        }
                        ?>
                        <!-- End Category posts -->

                    </div>
                </div>
            </div>
            <div class="pagination_area">
<!--                <nav>-->
<!--                    <ul class="pagination">-->

<!--                        <ul>-->
                            <?php
                            foreach ($links as $link)
                                echo $link;
                            ?>
<!--                        </ul>-->

<!--                    </ul>-->
<!--                </nav>-->
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="content_bottom_right">
                <div class="single_bottom_rightbar">

                    <!--<iframe height="350" frameborder="5" style="vertical-align: bottom;" src="//tools.whatsthescore.com/tools.php?id=37615&iframe=tap-sport-tools-37615&tz-js=Asia/Dhaka"></iframe>
                  -->
                </div>
                <div class="single_bottom_rightbar">

                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Side Bar -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-9787339225677626"
                         data-ad-slot="9494900273"
                         data-ad-format="auto"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>

                    <br/>
                    <h1><strong>জনপ্রিয় খেলার সংবাদ</strong></h1>
                    <ul class="small_catg popular_catg wow fadeInDown">
                        <?php if (is_array($popular_post) && count($popular_post) >= 1) { ?>

                            <?php foreach ($popular_post as $post) { ?>

                                <li>
                                    <div class="media wow fadeInDown"><a href="#" class="media-left">
                                            <img alt="<?php $date = $post->post_title; ?>"
                                                 src="<?php echo base_url() . './image_upload/post_image/thumbs/' . $post->post_image; ?>">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a
                                                        href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>"><?php echo $post->post_title; ?></a>
                                            </h4>
                                            <div class="comments_box">
                                    <span class="meta_date">
                                        <?php $date = $post->post_date;
                                        echo date("M d", strtotime($date)); ?>
                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        } else {
                            echo "<h4>&nbsp&nbsp&nbsp&nbspNo Post Available!</h4>";
                        }
                        ?>
                    </ul>

                </div>
                <div class="single_bottom_rightbar">
                    <h1><strong>সর্বশেষ খেলার সংবাদগুলো</strong></h1>
                    <ul class="small_catg popular_catg wow fadeInDown">
                        <?php if (is_array($recent_post) && count($recent_post) >= 1) { ?>

                            <?php foreach ($recent_post as $post) { ?>

                                <li>
                                    <div class="media wow fadeInDown"><a href="#" class="media-left">
                                            <img alt="<?php $date = $post->post_title; ?>"
                                                 src="<?php echo base_url() . './image_upload/post_image/thumbs/' . $post->post_image; ?>">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a
                                                        href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>"><?php echo $post->post_title; ?></a>
                                            </h4>
                                            <div class="comments_box">
                                    <span class="meta_date">
                                        <?php $date = $post->post_date;
                                        echo date("M d", strtotime($date)); ?>
                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        } else {
                            echo "<h4>&nbsp&nbsp&nbsp&nbspNo Post Available!</h4>";
                        }
                        ?>
                    </ul>

                </div>
                <div class="single_bottom_rightbar wow fadeInDown">
                    <h2>Popular Tags</h2>
                    <ul>
                        <li><a href="#">Cristiano Ronaldo</a></li>
                        <li><a href="#">Lionel Messi</a></li>
                        <li><a href="#">Neymar</a></li>
                        <li><a href="#">Suarez</a></li>
                        <li><a href="#">Modric</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
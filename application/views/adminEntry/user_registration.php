<?php
//if (isset($_POST['submit'])) {
//    echo '<pre>';
//    print_r($_POST);
//    print_r($_FILES);
//    exit();
//}
?>


<!DOCTYPE html>

<html>

    <head>

        <meta charset="UTF-8">

        <title><?php echo $title; ?></title>   

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <meta name="apple-mobile-web-app-capable" content="yes" />

        <meta name="description" content="">

        <meta name="keywords" content="coco bootstrap template, coco admin, bootstrap,admin template, bootstrap admin,">

        <meta name="author" content="Huban Creative">



        <?php echo $header_content; ?>

        <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>

        <script>



            function checkPasswordMatch() {

                var password = $("#password").val();

                var confirmPassword = $("#txtConfirmPassword").val();



                if (password != confirmPassword)
                    $("#divCheckPasswordMatch").html("Passwords do not match!").addClass('yellow');

                else
                    $("#divCheckPasswordMatch").html("Password matched.").addClass('yellow');

            }



            $(document).ready(function () {

                $("#txtConfirmPassword").keyup(checkPasswordMatch);

            });





            $(document).ready(function () {



                $('#password').keyup(function () {

                    $('#result').html(checkStrength($('#password').val()))

                })



                function checkStrength(password) {



                    //initial strength

                    var strength = 0



                    //if the password length is less than 6, return message.

                    if (password.length < 6) {

                        $('#result').removeClass()

                        $('#result').addClass('short')

                        return 'Password Length: Too short'

                    }



                    //length is ok, lets continue.



                    //if length is 8 characters or more, increase strength value

                    if (password.length > 7)
                        strength += 1



                    //if password contains both lower and uppercase characters, increase strength value

                    if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
                        strength += 1



                    //if it has numbers and characters, increase strength value

                    if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
                        strength += 1



                    //if it has one special character, increase strength value

                    if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
                        strength += 1



                    //if it has two special characters, increase strength value

                    if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,",%,&,@,#,$,^,*,?,_,~])/))
                        strength += 1



                    //now we have calculated strength value, we can return messages



                    //if value is less than 2

                    if (strength < 2) {

                        $('#result').removeClass()

                        $('#result').addClass('weak')

                        return 'Password Strength: Weak'

                    } else if (strength == 2) {

                        $('#result').removeClass()

                        $('#result').addClass('good')

                        return 'Password Strength: Good'

                    } else {

                        $('#result').removeClass()

                        $('#result').addClass('strong')

                        return 'Password Strength: Strong'

                    }

                }

            });



        </script>

        <style>

            .yellow {

                font-weight: bold;

                color: yellow;

            }



            #register {



            }

            #result{

                text-align: center;

                font-size: 20px;

                font-weight: bold;

                background-color: #999999;

                margin: auto;

            }

            #register .short{

                color: #ccffcc;

            }

            #register .weak{

                color: turquoise;

            }

            #register .good{

                color: #00e158;

            }

            #register .strong{

                color: #0000ff;

            }







        </style>



    </head>

    <body class="fixed-left signup-page">



        <!-- Begin page -->

        <div class="container">

            <div class="full-content-center animated fadeInDownBig">

                <div class="login-wrap">

                    <div class="login-block">

                        <p class="text-center"><a href="<?php echo base_url();?>"><img src="<?php echo base_url(); ?>adminAssets/img/login-logo.png" alt="Logo" height="80" width="240"></a></p>

                        <div class="form-group">

                            <?php echo $this->session->flashdata('verify_msg'); ?>

                            <h3>

                                <?php
                                $msg = $this->session->userdata('message');

                                if ($msg) {
                                    ?>

                                    <span class="label label-warning"><?php echo $msg; ?></span>   

                                    <?php
                                    $this->session->unset_userdata('message');
                                }
                                ?>

                            </h3>

                        </div>

                        <div class="form-group">

                            <div class="form-group">

                                <a href="<?php echo $this->facebook->login_url(); ?>"><img src="<?php echo base_url()?>signup_facebook.png" width="100%" class="img-responsive"></a>

                            </div>

                        </div>

                        <hr/>
                        <hr/>
                        <h2 align="center" style="font-size: 40px; font-weight: bold;">OR</h2>
                        <hr/>
                        <hr/>

                        <form role="form" action="<?php echo base_url(); ?>User_registration" enctype="multipart/form-data" method="post">
                        
                            <div class="form-group login-input">
                                
                                <i class="fa fa-user overlay"></i>

                                <input type="text" name="user_name" value="<?php echo set_value('user_name'); ?>" class="form-control text-input" placeholder="Enter Full Name" required="">

                                <div class="form-group login-input"><?php echo form_error('user_name'); ?></div>

                            </div>

                            <div class="form-group login-input">

                                <i class="fa fa-envelope overlay"></i>

                                <input type="email" name="email" value="<?php echo set_value('email'); ?>" class="form-control text-input" placeholder="Enter Email ID" required>

                                <div class="form-group login-input"><?php echo form_error('email'); ?></div>

                            </div>

                            <div class="form-group login-input" id="register">

                                <i class="fa fa-key overlay"></i>

                                <input type="password" name="user_password" value="<?php echo set_value('user_password'); ?>" class="form-control text-input" placeholder="Enter Your Password" id="password" required>

                                <div class="form-group login-input" id="result"></div>

                                <div class="form-group login-input"><?php echo form_error('user_password'); ?></div>

                            </div>

                            <div class="form-group login-input">

                                <i class="fa fa-key overlay"></i>

                                <input type="password" name="confirm_password" value="<?php echo set_value('confirm_password'); ?>" class="form-control text-input" placeholder="Confirm Password" id="txtConfirmPassword" onChange="checkPasswordMatch();" required>

                            </div>

                            <div class="form-group login-input" id="divCheckPasswordMatch"></div>

<!--                            <div class="row">-->
<!---->
<!--                                <div class="col-sm-12">-->
<!---->
<!--                                    <input type="file" name="user_avatar" class="btn btn-default btn-block" title="Upload your image [max size: 200KB]">-->
<!---->
<!--                                    <div class="form-group login-input">--><?php //echo form_error('message'); ?><!--</div>-->
<!---->
<!--                                </div>-->
<!---->
<!--                            </div>-->
<!--                            <p style="color: #30ffec; padding-top: 5px; font-weight: bold; font-size: 14pxl">Please upload image within 200kb size!</p>-->

                            <div class="row">

                                <div class="col-sm-12">

                                    <button type="submit" name="submit" class="btn btn-success btn-block">Sign Up</button>

                                </div>

                            </div>

                            <div class="row" style="padding-top: 10px">
                                <div class="col-sm-12" align="center">
                                    <a href="<?php echo base_url(); ?>sign-in" class="btn btn-danger">
                                        <i class="fa fa-lock" aria-hidden="true"></i>
                                        Already User? please sign in!
                                    </a>

                                </div>
                            </div>

                            <?php //echo form_close();  ?>

                            <?php echo $this->session->flashdata('msg'); ?>

                        </form>

                    </div>

                </div>



            </div>

        </div>

    </div>

    <!-- the overlay modal element -->

    <div class="md-overlay"></div>

    <!-- End of eoverlay modal -->

    <script>

        var resizefunc = [];

    </script>



    <?php echo $footer_content; ?>

</body>

</html>
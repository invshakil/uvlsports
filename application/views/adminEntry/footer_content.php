         <!-- Specific JS Libraries-->

         <script src="<?php echo base_url(); ?>adminAssets/libs/bootstrap/js/bootstrap.min.js"></script>
         <script src="<?php echo base_url(); ?>adminAssets/libs/jqueryui/jquery-ui-1.10.4.custom.min.js"></script>
         <script src="<?php echo base_url(); ?>adminAssets/libs/jquery-ui-touch/jquery.ui.touch-punch.min.js"></script>
         <script src="<?php echo base_url(); ?>adminAssets/libs/jquery-detectmobile/detect.js"></script>
         <script src="<?php echo base_url(); ?>adminAssets/libs/jquery-animate-numbers/jquery.animateNumbers.js"></script>

         <script src="<?php echo base_url(); ?>adminAssets/libs/fastclick/fastclick.js"></script>
         <script src="<?php echo base_url(); ?>adminAssets/libs/jquery-blockui/jquery.blockUI.js"></script>
         <script src="<?php echo base_url(); ?>adminAssets/libs/bootstrap-bootbox/bootbox.min.js"></script>
         <script src="<?php echo base_url(); ?>adminAssets/libs/jquery-slimscroll/jquery.slimscroll.js"></script>
         <script src="<?php echo base_url(); ?>adminAssets/libs/jquery-sparkline/jquery-sparkline.js"></script>
         <script src="<?php echo base_url(); ?>adminAssets/libs/nifty-modal/js/classie.js"></script>
         <script src="<?php echo base_url(); ?>adminAssets/libs/nifty-modal/js/modalEffects.js"></script>
         <script src="<?php echo base_url(); ?>adminAssets/libs/sortable/sortable.min.js"></script>
         <script src="<?php echo base_url(); ?>adminAssets/libs/bootstrap-fileinput/bootstrap.file-input.js"></script>
         <script src="<?php echo base_url(); ?>adminAssets/libs/bootstrap-select/bootstrap-select.min.js"></script>
         <script src="<?php echo base_url(); ?>adminAssets/libs/bootstrap-select2/select2.min.js"></script>


         <script src="<?php echo base_url(); ?>adminAssets/libs/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
         <script src="<?php echo base_url(); ?>adminAssets/libs/jquery-icheck/icheck.min.js"></script>


         <!-- Demo Specific JS Libraries -->
         <script src="<?php echo base_url(); ?>adminAssets/libs/prettify/prettify.js"></script>

         <script src="<?php echo base_url(); ?>adminAssets/js/init.js"></script>
         <!-- Page Specific JS Libraries -->
         <script src="<?php echo base_url(); ?>adminAssets/libs/bootstrap-select/bootstrap-select.min.js"></script>

         <script src="<?php echo base_url(); ?>adminAssets/libs/jquery-clndr/moment-2.5.1.js"></script>


         <script src="<?php echo base_url(); ?>adminAssets/libs/pace/pace.min.js"></script>
         <script src="<?php echo base_url(); ?>adminAssets/js/pages/forms.js"></script>

         <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

        <?php if ($this->session->userdata('message') != ""){ ?>
            <script>
                toastr.success('<?php echo $this->session->userdata('message');?>');
            </script>
        <?php } $this->session->unset_userdata('message');?>

         <?php if ($this->session->userdata('error') != ""){ ?>
             <script>
                 toastr.error('<?php echo $this->session->userdata('error');?>');
             </script>
         <?php } $this->session->unset_userdata('error');?>

         <?php if ($this->session->flashdata('verify_msg') != ""){ ?>
             <script>
                 toastr.success('<?php echo $this->session->flashdata('verify_msg');?>');
             </script>
         <?php } $this->session->unset_userdata('verify_msg'); ?>

<!--         <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->

         <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>
         <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
         <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>

         <script>
             $(document).ready(function() {
                 $('#summernote').summernote({
                     height: 460,   //set editable area's height
                     codemirror: { // codemirror options
                         theme: 'monokai'
                     }
                 });
             });
         </script>

         <script src="<?php echo base_url(); ?>adminAssets/libs/jquery-datatables/js/jquery.dataTables.min.js"></script>
         <script src="<?php echo base_url(); ?>adminAssets/libs/jquery-datatables/js/dataTables.bootstrap.js"></script>
         <script src="<?php echo base_url(); ?>adminAssets/libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
         <script src="<?php echo base_url(); ?>adminAssets/js/pages/datatables.js"></script>
<!-- Top Bar Start -->

<div class="topbar">

    <div class="topbar-left">

        <div class="logo">

            <style>
                .logo h1 img {
                    height: 90%;
                    background-color: #720e9e;
                }
            </style>
            <h3 style="background: #68C39F;color: white; padding-left: 10px; font-weight: bold;font-family: 'Bookman Old Style'; font-size: 17px">
                <a href="<?php echo base_url()?>" target="_blank">UNIVERSAL SPORTS</a>
            </h3>

        </div>

        <button class="button-menu-mobile open-left">

            <i class="fa fa-bars"></i>

        </button>

    </div>

    <!-- Button mobile view to collapse sidebar menu -->

    <div class="navbar navbar-default" role="navigation">

        <div class="container">

            <div class="navbar-collapse2">

                <ul class="nav navbar-nav navbar-right top-navbar">

                    <li class="dropdown iconify hide-phone">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-globe"></i><span
                                    class="label label-danger absolute"></span></a>

                        <ul class="dropdown-menu dropdown-message">

                            <li class="dropdown-header notif-header"><i class="icon-bell-2"></i> New Notifications<a
                                        class="pull-right" href="#"><i class="fa fa-cog"></i></a></li>


                            <li class="dropdown-footer">

                                <div class="btn-group btn-group-justified">

                                    <div class="btn-group">

                                        <a href="#" class="btn btn-sm btn-primary"><i class="icon-ccw-1"></i>
                                            Refresh</a>

                                    </div>

                                    <div class="btn-group">

                                        <a href="#" class="btn btn-sm btn-danger"><i class="icon-trash-3"></i> Clear
                                            All</a>

                                    </div>

                                    <div class="btn-group">

                                        <a href="#" class="btn btn-sm btn-success">See All <i
                                                    class="icon-right-open-2"></i></a>

                                    </div>

                                </div>

                            </li>

                        </ul>

                    </li>


                    <li class="dropdown iconify hide-phone"><a href="#" onclick="javascript:toggle_fullscreen()"><i
                                    class="icon-resize-full-2"></i></a></li>

                    <li class="dropdown topbar-profile">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span
                                    class="rounded-image topbar-profile-image">
                                           <?php
                                           if ($this->session->userdata('login_type') == 1) {
                                               ?>
                                               <img src="<?php echo $this->session->userdata('user_avatar'); ?>">
                                           <?php } else { ?>
                                               <img src="<?php echo base_url() . $this->session->userdata('user_avatar'); ?>">
                                           <?php } ?>
                                        </span><?php echo $this->session->userdata('user_name'); ?> <i
                                    class="fa fa-caret-down"></i>
                        </a>

                        <ul class="dropdown-menu">


                            <?php if ($total_published_post_by_user != 0) { ?>

                                <li>
                                    <a href="<?php echo base_url(); ?>user_profile/index/<?php echo $this->session->userdata('user_id'); ?>/<?php
                                    $name = url_title($this->session->userdata('user_name'));
                                    echo $name;
                                    ?>" target="blank">My Profile</a></li>

                            <?php } ?>

                            <li><a href="<?php echo base_url(); ?>account_settings">Account Setting</a></li>

                            <li><a class="md-trigger" data-modal="md-fall"><i class="icon-logout-1"></i>
                                    Logout</a></li>

                        </ul>

                    </li>

                </ul>

            </div>

            <!--/.nav-collapse -->

        </div>

    </div>

</div>

<!-- Top Bar End -->

<!-- Left Sidebar Start -->

<div class="left side-menu">

    <div class="sidebar-inner slimscrollleft">

        <!-- Search form -->

        <form role="search" class="navbar-form">

            <div class="form-group">

                <input type="text" placeholder="Search" class="form-control">

                <button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>

            </div>

        </form>

        <div class="clearfix"></div>

        <!--- Profile -->

        <div class="profile-info">

            <div class="col-xs-4">

                <a href="#" class="rounded-image profile-image">
                    <?php
                    if ($this->session->userdata('login_type') == 1) {
                        ?>
                        <img src="<?php echo $this->session->userdata('user_avatar'); ?>">
                    <?php } else { ?>
                        <img src="<?php echo base_url() . $this->session->userdata('user_avatar'); ?>">
                    <?php } ?>
                </a>

            </div>

            <div class="col-xs-8">

                <div class="profile-text">Welcome <b><?php echo $this->session->userdata('user_name'); ?></b></div>

                <div class="profile-buttons">

                    <a href="javascript:;"><i class="fa fa-envelope-o pulse"></i></a>

                    <a href="#connect" class="open-right"><i class="fa fa-comments"></i></a>

                    <a class="md-trigger" data-modal="md-fall" title="Sign Out"><i
                                class="fa fa-power-off text-red-1"></i></a>

                </div>

            </div>

        </div>

        <!--- Divider -->

        <div class="clearfix"></div>

        <hr class="divider"/>

        <div class="clearfix"></div>

        <!--- Divider -->

        <div id="sidebar-menu">

            <ul>

                <?php if ($this->session->userdata('access_level') != 3) { ?>
                    <li>
                        <a href='<?php echo base_url(); ?>super_admin_panel'
                           class="<?php if ($title == 'Dashboard | Admin Master') echo 'active'; ?>">
                            <i class='fa fa-home'></i><span>Dashboard</span>
                            <span class="pull-right"></i></span>
                        </a></li>
                <?php } else { ?>
                    <li>
                        <a href='<?php echo base_url(); ?>user_dashboard'
                           class="<?php if ($title == 'Dashboard | User Panel') echo 'active'; ?>">
                            <i class='fa fa-home'></i><span>Dashboard</span>
                            <span class="pull-right"></i></span>
                        </a>
                    </li>
                <?php } ?>


                <!--Only Admin Can see this menu -->
                <?php if ($this->session->userdata('access_level') != 3) { ?>

                    <li class='has_sub'><a href='javascript:void(0);'><i
                                    class='fa fa-users'></i><span>User Panel</span> <span class="pull-right"><i
                                        class="fa fa-angle-down"></i></span></a>

                        <ul>

                            <li>
                                <a href='<?php echo base_url(); ?>super_admin_panel/admin_list'
                                   class="<?php if ($title == 'Admin List | Admin Master') echo 'active'; ?>">
                                    <span>Admin List</span>
                                </a>
                            </li>

                            <li>
                                <a href='<?php echo base_url(); ?>super_admin_panel/manage_user'
                                   class="<?php if ($title == 'Manage User | Admin Master') echo 'active'; ?>">
                                    <span>Manage User</span>
                                </a>
                            </li>

                        </ul>

                    </li>

                <?php } ?>



                <?php if ($this->session->userdata('access_level') != 3) { ?>

                    <li class='has_sub'><a href='javascript:void(0);'><i
                                    class='fa fa-tag'></i><span>Post Element</span> <span class="pull-right"><i
                                        class="fa fa-angle-down"></i></span></a>

                        <ul>


                            <!--Only Admin Can see this menu -->


                            <li><a href='<?php echo base_url(); ?>categories'
                                   class="<?php if ($title == 'Categories | Admin Master') echo 'active'; ?>"><span>Categories</span></a>
                            </li>
                            <li><a href='<?php echo base_url(); ?>tags'
                                   class="<?php if ($title == 'Tags | Admin Master') echo 'active'; ?>"><span>Tags</span></a>
                            </li>

                        </ul>

                    </li>
                <?php } ?>




                <?php if ($this->session->userdata('access_level')) { ?>

                    <li class='has_sub'><a href='javascript:void(0);'><i
                                    class='fa fa-text-width'></i><span>Post</span> <span class="pull-right"><i
                                        class="fa fa-angle-down"></i></span></a>

                        <ul>
                            <!--Only User Can see this menu -->
                            <?php if ($this->session->userdata('access_level') == 3) { ?>
                                <li><a href='<?php echo base_url(); ?>user_dashboard/add_post'
                                       class="<?php if ($title == 'Add Post | User Panel') echo 'active'; ?>"><span>Add Post</span></a>
                                </li>

                                <!--Only Admin Can see this menu -->
                            <?php } else if ($this->session->userdata('access_level') != 3) { ?>
                                <li>
                                    <a href='<?php echo base_url(); ?>post_controller/add_post'
                                       class="<?php if ($title == 'Add Post | Admin Master') echo 'active'; ?>"><span>Add Post</span></a>
                                </li>
                                <li>
                                    <a href='<?php echo base_url(); ?>post_controller/draft_post'
                                       class="<?php if ($title == 'Draft Post | Admin Master') echo 'active'; ?>"><span>Draft Post</span></a>
                                </li>
                                <?php
                            } else {
                                return false;
                            }
                            ?>
                            <!--Only Admin Can see this menu -->
                            <?php if ($this->session->userdata('access_level') != 3) { ?>

                                <li>
                                    <a href='<?php echo base_url(); ?>post_controller/manage_post'
                                       class="<?php if ($title == 'Manage Post | Admin Master') echo 'active'; ?>"><span>Manage Post</span></a>
                                </li>

                            <?php } ?>
                        </ul>

                    </li>

                    <li>
                        <a href='<?php echo base_url(); ?>super_admin_panel/image_gallery'
                           class="<?php if ($title == 'Image Gallery | Admin Master') echo 'active'; ?>"><i
                                    class='fa fa-picture-o'></i>
                            <span>Image Gallery</span>
                            <span class="pull-right"></i></span>
                        </a>
                    </li>

                    <li class='has_sub'><a href='javascript:void(0);'><i class='fa fa-user'></i><span>Profile</span>
                            <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>

                        <ul>
                            <?php if ($total_published_post_by_user != 0) { ?>
                                <li>
                                    <a href="<?php echo base_url(); ?>user_profile/index/<?php echo $this->session->userdata('user_id'); ?>/<?php
                                    $name = url_title($this->session->userdata('user_name'));
                                    echo $name;
                                    ?>" target="blank"><span>Visit Profile</span></a></li>
                            <?php } ?>

                            <?php if ($this->session->userdata('access_level') == 3) { ?>
                                <li>
                                    <a href="<?php echo base_url(); ?>user_dashboard/my_post"
                                       class="<?php if ($title == 'My Post | User Panel') echo 'active'; ?>"><span>My Post</span></a>
                                </li>
                            <?php } else { ?>

                                <li>
                                    <a href="<?php echo base_url(); ?>super_admin_panel/my_post"
                                       class="<?php if ($title == 'My Post | User Panel') echo 'active'; ?>"><span>My Post</span></a>
                                </li>
                            <?php } ?>

                            <li>
                                <a href="<?php echo base_url(); ?>account_settings"
                                   class="<?php if ($title == 'Account Settings | User Panel') echo 'active'; ?>"><span>Account Settings</span></a>
                            </li>
                            <li><a href="<?php echo base_url(); ?>password_change"
                                   class="<?php if ($title == 'Password Change | User Panel') echo 'active'; ?>"><span>Password Change</span></a>
                            </li>

                        </ul>

                    </li>


                <?php } ?>


                <!--Only Admin Can see this menus -->
                <?php if ($this->session->userdata('access_level') != 3) { ?>

                    <li class='has_sub'><a href='javascript:void(0);'><i class='fa fa-pie-chart'></i><span>Match Schedule</span>
                            <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>

                        <ul>

                            <li><a href='<?php echo base_url(); ?>match_schedule'
                                   class="<?php if ($title == 'Game Week Create | Admin Master') echo 'active'; ?>"><span>Add Game Week</span></a>
                            </li>

                            <li><a href='<?php echo base_url(); ?>match_schedule/game_week_match_add'
                                   class="<?php if ($title == 'Match Create | Admin Master') echo 'active'; ?>"><span>Add Match to GW</span></a>
                            </li>

                            <li><a href='<?php echo base_url(); ?>match_schedule/manage_game_week_match'
                                   class="<?php if ($title == 'Manage Matches | Admin Master') echo 'active'; ?>"><span>Manage Matches of GW</span></a>
                            </li>

                        </ul>

                    </li>

                    <li>
                        <a href='<?php echo base_url(); ?>contact_mail'
                           class="<?php if ($title == 'Contact || Admin Panel') echo 'active'; ?>"><i
                                    class='fa fa-envelope'></i>
                            <span>
                                            Contact <?php $r = $this->db->where('read_status', 0)->from('tbl_contact')->count_all_results();
                                if ($r > 0) { ?>
                                    <span class="label label-danger pull-right">UNREAD <?php echo $r; ?></span>
                                <?php } ?>
                                        </span>
                            <span class="pull-right"></i></span>
                        </a>
                    </li>

                    <li class='has_sub'><a href='javascript:void(0);'><i class='fa fa-facebook-square'></i><span>Social Control</span>
                            <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>

                        <ul>

                            <li><a href='<?php echo base_url(); ?>super_admin_panel/twitter_feed'><span>Twitter Feed Link</span></a>
                            </li>

                            <li>
                                <a href='<?php echo base_url(); ?>super_admin_panel/facebook_link'><span>Facebook Link</span></a>
                            </li>

                            <li>
                                <a href='<?php echo base_url(); ?>super_admin_panel/twitter_link'><span>Twitter Link</span></a>
                            </li>

                        </ul>

                    </li>

                    <li class='has_sub'><a href='javascript:void(0);'><i class='fa fa-newspaper-o'></i><span>Sponsor Control</span>
                            <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>

                        <ul>

                            <li><a href='<?php echo base_url(); ?>super_admin_panel/sponsor_category'><span>Sponsor Category</span></a>
                            </li>

                            <li>
                                <a href='<?php echo base_url(); ?>super_admin_panel/post_sponsor'><span>Post Sponsor</span></a>
                            </li>

                        </ul>

                    </li>

                    <li class='has_sub'><a href='javascript:void(0);'><i
                                    class='fa fa-apple'></i><span>API Control</span> <span class="pull-right"><i
                                        class="fa fa-angle-down"></i></span></a>

                        <ul>

                            <li><a href='<?php echo base_url(); ?>super_admin_panel/home_page_fixture_api'><span>Home Page Fixture API</span></a>
                            </li>

                            <li>
                                <a href='<?php echo base_url(); ?>super_admin_panel/live_games_api'><span>Live Games API</span></a>
                            </li>

                            <li><a href='<?php echo base_url(); ?>super_admin_panel/league_fixtures_api'><span>League Fixture API</span></a>
                            </li>

                            <li><a href='<?php echo base_url(); ?>super_admin_panel/league_tables_api'><span>League Table API</span></a>
                            </li>

                        </ul>

                    </li>
                <?php } ?>
            </ul>

            <div class="clearfix"></div>

        </div>

        <div class="clearfix"></div>


        <div class="clearfix"></div>
        <br><br><br>

    </div>


</div>

<!-- Left Sidebar End -->
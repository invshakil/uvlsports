<link href="<?php echo base_url(); ?>adminAssets/libs/jqueryui/ui-lightness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>adminAssets/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>adminAssets/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>adminAssets/libs/fontello/css/fontello.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>adminAssets/libs/animate-css/animate.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>adminAssets/libs/nifty-modal/css/component.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>adminAssets/libs/magnific-popup/magnific-popup.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>adminAssets/libs/pace/pace.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>adminAssets/libs/sortable/sortable-theme-bootstrap.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>adminAssets/libs/jquery-icheck/skins/all.css" rel="stylesheet" />

<!-- Extra CSS Libraries Start -->
<link href="<?php echo base_url(); ?>adminAssets/libs/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>adminAssets/libs/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>adminAssets/libs/bootstrap-xeditable/css/bootstrap-editable.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url(); ?>adminAssets/css/style.css" rel="stylesheet" type="text/css" />
<!--<link href="--><?php //echo base_url(); ?><!--adminAssets/css/AdminLTE.css" rel="stylesheet" type="text/css" />-->

<!-- Extra CSS Libraries End -->
<link href="<?php echo base_url(); ?>adminAssets/css/style-responsive.css" rel="stylesheet" />

<!-- Code Highlighter for Demo -->
<link href="<?php echo base_url(); ?>adminAssets/libs/prettify/github.css" rel="stylesheet" />

<!-- Extra CSS Libraries End -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<link href="<?php echo base_url(); ?>adminAssets/libs/jquery-datatables/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>adminAssets/libs/jquery-datatables/extensions/TableTools/css/dataTables.tableTools.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url(); ?>adminAssets/css/style.css" rel="stylesheet" type="text/css" />
<!-- Extra CSS Libraries End -->

<script>
    var resizefunc = [];
</script>

<script src="https://code.jquery.com/jquery-1.9.1.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
<!--<link href="--><?php //echo base_url(); ?><!--adminAssets/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />-->

<style>
    a{
        color: #f3f3f3;
    }

    .navbar-default .navbar-nav>li>a {
        color: white;
        font-weight: bold;
    }

    .navbar-default .navbar-nav>li>a:hover{
        color: white;
    }
</style>
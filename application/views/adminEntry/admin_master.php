<?php

$author_id = $this->session->userdata('user_id');

$total_published_post_by_user = $this->super_admin_extra->select_all_published_post_by_user($author_id);
?>

<!DOCTYPE html>

<html>

<head>

    <meta charset="UTF-8">

    <title><?php echo $title; ?></title>

    <link rel="shortcut icon" href="<?php echo base_url(); ?>/favicon.ico" type="image/x-icon">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

    <meta name="apple-mobile-web-app-capable" content="yes"/>

    <meta name="description" content="">

    <meta name="keywords" content="coco bootstrap template, coco admin, bootstrap,admin template, bootstrap admin,">

    <meta name="author" content="Huban Creative">

    <?php echo $header_content; ?>

    <style>

        .alert-warning {
            background-color: #EAE696 !important;
            color: #5a0099 !important;
            border-color: #faebcc;
        }
    </style>

</head>

<body class="fixed-left">

<!-- Modal Start -->

<!-- Modal Task Progress -->

<!-- Modal Logout -->

<div class="md-modal md-fall" id="md-fall">

    <div class="md-content">

        <h3><strong>Logout</strong> Confirmation</h3>

        <div>

            <p class="text-center">Are you sure want to logout from admin panel?</p>

            <p class="text-center">

                <button class="btn btn-danger md-close">Nope!</button>
                <?php if ($this->session->userdata('login_type') == 1) { ?>
                    <a href="<?php echo base_url(); ?>social_verification/logout" class="btn btn-success md-close">Yes,
                        Log me out!</a>
                <?php } else {?>
                <?php if ($this->session->userdata('access_level') != 3) { ?>
                    <a href="<?php echo base_url(); ?>super_admin_panel/logout" class="btn btn-success md-close">Yes,
                        Log me out!</a>
                <?php } else { ?>
                    <a href="<?php echo base_url(); ?>user_dashboard/logout" class="btn btn-success md-close">Yes, Log
                        me out!</a>
                <?php } }?>
            </p>

        </div>

    </div>

</div>        <!-- Modal End -->

<!-- Begin page -->

<div id="wrapper">


    <?php include "navigation.php";?>

    <!-- Right Sidebar Start -->


    <!-- Right Sidebar End -->

    <!-- Start right content -->

    <div class="content-page">

        <!-- ============================================================== -->

        <!-- Start Content here -->

        <!-- ============================================================== -->

        <?php echo $admin_maincontent; ?>

        <!-- ============================================================== -->

        <!-- End content here -->

        <!-- ============================================================== -->


    </div>

    <!-- End right content -->


</div>


<div id="contextMenu" class="dropdown clearfix">

    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu"
        style="display:block;position:static;margin-bottom:5px;">

        <li><a tabindex="-1" href="javascript:;" data-priority="high"><i class="fa fa-circle-o text-red-1"></i> High
                Priority</a></li>

        <li><a tabindex="-1" href="javascript:;" data-priority="medium"><i class="fa fa-circle-o text-orange-3"></i>
                Medium Priority</a></li>

        <li><a tabindex="-1" href="javascript:;" data-priority="low"><i class="fa fa-circle-o text-yellow-1"></i> Low
                Priority</a></li>

        <li><a tabindex="-1" href="javascript:;" data-priority="none"><i class="fa fa-circle-o text-lightblue-1"></i>
                None</a></li>

    </ul>

</div>

<!-- End of page -->

<!-- the overlay modal element -->

<div class="md-overlay"></div>

<!-- End of eoverlay modal -->

<script>

    var resizefunc = [];

</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<?php echo $footer_content; ?>


</body>


</html>
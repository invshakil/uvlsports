<div class="content">
    <!-- Start info box -->
    <?php if ($this->session->userdata('access_level') != 3) { ?>
    <div class="row top-summary">
        <div class="col-lg-3 col-md-6">
            <div class="widget green-1 animated fadeInDown">
                <div class="widget-content padding">
                    <div class="widget-icon">
                        <i class="icon-globe-inv"></i>
                    </div>
                    <div class="text-box">
                        <p class="maindata">TOTAL <b>POST PUBLISHED</b></p>
                        <h2><span class="animate-number" data-value="<?php echo $total_published_post; ?>"
                                  data-duration="400">0</span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <!--<i class="fa fa-caret-up rel-change"></i> <b>39%</b> increase in traffic-->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="widget darkblue-2 animated fadeInDown">
                <div class="widget-content padding">
                    <div class="widget-icon">
                        <i class="icon-bag"></i>
                    </div>
                    <div class="text-box">
                        <p class="maindata">TOTAL <b>PENDING POST</b></p>
                        <h2><span class="animate-number" data-value="<?php echo $total_pending_post; ?>"
                                  data-duration="300">0</span></h2>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <!--<i class="fa fa-caret-down rel-change"></i> <b>11%</b> decrease in sales-->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="widget green-1 animated fadeInDown">
                <div class="widget-content padding">
                    <div class="widget-icon">
                        <i class="icon-globe-inv"></i>
                    </div>
                    <div class="text-box">
                        <p class="maindata">OVERALL <b>HITS IN POST</b></p>
                        <h2><span class="animate-number" data-value="<?php echo $total_user_hit->hit_count; ?>"
                                  data-duration="3000">0</span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <!--<i class="fa fa-caret-up rel-change"></i> <b>39%</b> increase in traffic-->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="widget lightblue-1 animated fadeInDown">
                <div class="widget-content padding">
                    <div class="widget-icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <div class="text-box">
                        <p class="maindata">TOTAL <b>VERIFIED USERS</b></p>
                        <h2><span class="animate-number" data-value="<?php echo $total_registered_user ?>"
                                  data-duration="3000">0</span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <!--<i class="fa fa-caret-up rel-change"></i> <b>6%</b> increase in users-->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <?php } ?>

        <div class="col-lg-3 col-md-6">
            <div class="widget lightblue-1 animated fadeInDown">
                <div class="widget-content padding">
                    <div class="widget-icon">
                        <i class="icon-globe-inv"></i>
                    </div>
                    <div class="text-box">
                        <p class="maindata">TOTAL <b>WRITTEN POST BY YOU (PUBLISHED)</b></p>
                        <?php if (isset($total_published_post_by_user)) { ?>

                            <h2><span class="animate-number" data-value="<?php echo $total_published_post_by_user ?>"
                                      data-duration="300">0</span></h2>

                        <?php } else { ?>

                            <h2><span class="animate-number" data-value="0" data-duration="300">0</span></h2>

                        <?php } ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <!--<i class="fa fa-caret-up rel-change"></i> <b>6%</b> increase in users-->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="widget green-1 animated fadeInDown">
                <div class="widget-content padding">
                    <div class="widget-icon">
                        <i class="icon-globe-inv"></i>
                    </div>
                    <div class="text-box">
                        <p class="maindata">TOTAL <b>WRITTEN POST BY YOU (PENDING)</b></p>

                        <?php if (isset($total_pending_post_by_user)) { ?>

                            <h2><span class="animate-number" data-value="<?php echo $total_pending_post_by_user; ?>"
                                      data-duration="300">0</span></h2>

                        <?php } else { ?>

                            <h2><span class="animate-number" data-value="0" data-duration="300">0</span></h2>
                        <?php } ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <!--<i class="fa fa-caret-up rel-change"></i> <b>39%</b> increase in traffic-->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="widget darkblue-2 animated fadeInDown">
                <div class="widget-content padding">
                    <div class="widget-icon">
                        <i class="icon-bag"></i>
                    </div>
                    <div class="text-box">
                        <p class="maindata">TOTAL <b>HITS ON YOUR POST</b></p>
                        <?php if (isset($total_hits)) { ?>
                        <h2><span class="animate-number" data-value="<?php echo $total_hits->hit_count; ?>"
                                  data-duration="300">0</span></h2>
                        <?php } else { ?>

                            <h2><span class="animate-number" data-value="0" data-duration="300">0</span></h2>
                        <?php } ?>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <!--<i class="fa fa-caret-down rel-change"></i> <b>11%</b> decrease in sales-->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="widget green-1 animated fadeInDown">
                <div class="widget-content padding">
                    <div class="widget-icon">
                        <i class="icon-globe-inv"></i>
                    </div>
                    <div class="text-box">
                        <p class="maindata"><b>AVERAGE</b> HITS ON YOUR POST</p>

                        <?php if (isset($total_pending_post_by_user)) { ?>

                            <h2><span class="animate-number" data-value="<?php echo ceil($avg_hits->hit_count); ?>"
                                      data-duration="300">0</span></h2>

                        <?php } else { ?>

                            <h2><span class="animate-number" data-value="0" data-duration="300">0</span></h2>
                        <?php } ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <!--<i class="fa fa-caret-up rel-change"></i> <b>39%</b> increase in traffic-->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>


    <!-- End of info box -->
    <div class="row">
        <div class="col-sm-12">
            <div class="widget">
                <div class="widget-content padding">
                    <div class="col-sm-3">
                        <div class="widget green-1">
                            <div class="widget-header">
                                <h2><strong>SUBMIT</strong> YOUR POST</h2>
                                <div class="additional-btn">
                                    <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                                    <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                                    <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
                                </div>
                            </div>
                            <div class="widget-content padding">
                                <?php if ($this->session->userdata('access_level') != 3) { ?>
                                    <p align="center">
                                        <a class="btn btn-danger" href="<?php echo base_url(); ?>post_controller/add_post.html">
                                            WRITE NEW ARTICLE
                                        </a>
                                    </p>
                                <?php } else { ?>
                                    <p align="center">
                                        <a class="btn btn-danger" href="<?php echo base_url(); ?>user_dashboard/add_post.html">
                                            WRITE NEW ARTICLE
                                        </a>
                                    </p>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="widget darkblue-1">
                            <div class="widget-header">
                                <h2><strong>YOUR ARTICLES</strong> LIST</h2>
                                <div class="additional-btn">
                                    <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                                    <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                                    <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
                                </div>
                            </div>
                            <div class="widget-content padding">
                                <?php if ($this->session->userdata('access_level') != 3) { ?>
                                    <p align="center">
                                        <a class="btn btn-success" href="<?php echo base_url(); ?>super_admin_panel/my_post.html">
                                            ARTICLE LIST
                                        </a>
                                    </p>

                                <?php } else { ?>
                                    <p align="center">
                                        <a class="btn btn-success" href="<?php echo base_url(); ?>user_dashboard/my_post.html">
                                            ARTICLE LIST
                                        </a>
                                    </p>

                                <?php } ?>
                            </div>
                        </div>

                        <div class="widget red-1">
                            <div class="widget-header">
                                <h2><strong>ACCOUNT</strong> SETTINGS</h2>
                                <div class="additional-btn">
                                    <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                                    <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                                    <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
                                </div>
                            </div>
                            <div class="widget-content padding">
                                <p align="center">
                                    <a class="btn btn-info" href="<?php echo base_url(); ?>account_settings.html">
                                        CHANGE SETTINGS
                                    </a>
                                </p>

                            </div>
                        </div>

                        <div class="widget yellow-1">
                            <div class="widget-header">
                                <h2><strong>PASSWORD</strong> CHANGE</h2>
                                <div class="additional-btn">
                                    <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                                    <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                                    <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
                                </div>
                            </div>
                            <div class="widget-content padding">
                                <p align="center">
                                    <a class="btn btn-primary" href="<?php echo base_url(); ?>password_change.html">
                                        CHANGE YOUR PASSWORD
                                    </a>
                                </p>

                            </div>
                        </div>

                    </div>



                    <div class="col-sm-5">
                        <h4 class="alert alert-warning" align="justify"><b>Popular Post by You</b></h4>
                        <?php
                        if (is_array($popular_post_by_user) && count($popular_post_by_user) >= 1) {

                            foreach ($popular_post_by_user as $post) {
                                ?>
                                <ul class="list-group">
                                    <li class="list-group-item"><a style="color: #0A487C" href="<?php echo base_url(); ?>news_details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>" target="_blank"><?php echo $post->post_title; ?></a> <span class="badge"><?php echo $post->hit_count; ?></span></li>
                                </ul>
                            <?php }
                        } else {
                            echo "You have not posted anything";
                        } ?>

                    </div>

                    <div class="col-sm-4">
                        <div class="widget darkblue-2">
                            <div class="widget-header">
                                <h2><strong>PROFILE PICTURE</strong> CHANGE</h2>
                                <div class="additional-btn">
                                    <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                                    <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                                    <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
                                </div>
                            </div>
                            <div class="widget-content padding">
                                <form action="<?= base_url()?>super_admin_panel/change_profile_picture" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <input type="file" name="user_avatar" class="btn btn-info"
                                               onchange="readURL(this);"
                                               title="Select Image">

                                        <br/>
                                        <br/>

                                        <?php if ($this->session->userdata('user_avatar') == '') {?>
                                        <img id="blah" src="http://placehold.it/620x348"
                                             class="img-responsive img-thumbnail" alt="your image"/>
                                        <?php }
                                        else {?>
                                            <?php
                                                if ($this->session->userdata('login_type') == 1) {
                                                    ?>
                                                    <img id="blah" src="<?php echo $this->session->userdata('user_avatar'); ?>" class="img-responsive img-thumbnail">
                                                <?php } else { ?>
                                                    <img id="blah" src="<?php echo base_url() . $this->session->userdata('user_avatar'); ?>" class="img-responsive img-thumbnail">
                                                <?php }

                                        }?>
                                        <input type="submit" class="btn btn-danger form-control" value="Change Profile Picture">


                                    </div>
                                </form>
                            </div>
                        </div>


                        <script>
                            function readURL(input) {
                                if (input.files && input.files[0]) {
                                    var reader = new FileReader();

                                    reader.onload = function (e) {
                                        $('#blah')
                                            .attr('src', e.target.result);
                                    };

                                    reader.readAsDataURL(input.files[0]);
                                }
                            }
                        </script>

                    </div>

                </div>
            </div>
            <?php if ($this->session->userdata('access_level') == 3) { ?>
                <div class="widget">
                    <div class="widget-content padding">
                        <h3 class="alert alert-success" align="justify">
                            Hello <?php echo $this->session->userdata('user_name'); ?>, Welcome to website user
                            panel!</h3>

                        <div class="alert alert-info">
                            <p>
                                First of all thanks for registration! Here you can post about anything about sports.
                                Preferably Football And Cricket right now, as we will expand our coverage on other
                                sports in
                                later versions of website.
                            </p>

                            <p>
                                Here we are giving you instruction about how you will post, which will be published on
                                website! So follow those instructions, so that you will have a published post in
                                website! ;)
                            </p>

                            <p>
                                From left Navigation Menu go to Add Post;

                            </p>

                            <p>
                                1. Give a Title of your post.
                            </p>

                            <p>
                                2. Write the details of your post which will be on post description box. Use the editor
                                as
                                it is almost like microsoft office.
                            </p>

                            <p>
                                3. Select on which category your post belongs.
                            </p>

                            <p>
                                4. Submit your post for Admin Approval. After proof Reading admin will publish your
                                post.
                                You can see whether your post published or on pending from dashboard!
                            </p>

                            <p>
                                Finally thanks for joining with us. Have a nice day!
                            </p>

                        </div>
                        <div>
                            <h3 class="alert alert-success" align="justify">To unlock your profile make sure you have a
                                published post! ;) </h3>
                        </div>
                    </div>

                </div>
            <?php } ?>

        </div>
    </div>

</div>
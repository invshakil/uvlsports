<!DOCTYPE html>

<html>

<head>

    <meta charset="UTF-8">

    <title><?php echo $title; ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

    <meta name="apple-mobile-web-app-capable" content="yes"/>

    <meta name="description" content="">

    <meta name="keywords" content="coco bootstrap template, coco admin, bootstrap,admin template, bootstrap admin,">

    <meta name="author" content="Huban Creative">


    <?php include 'header_content.php'; ?>

</head>

<body class="fixed-left login-page">

<!-- Begin page -->

<div class="container">

    <div class="full-content-center">

        <div class="row">


        </div>

        <div class="login-wrap animated flipInX">

            <div class="login-block">

                <p class="text-center"><a href="<?php echo base_url(); ?>"><img
                                src="<?php echo base_url(); ?>adminAssets/img/login-logo.png" alt="Logo" height="80"
                                width="240"></a></p>


                <div class="form-group">

                    <div class="form-group">

                        <?php echo $this->session->flashdata('verify_msg'); ?>

                    </div>

                </div>

                <div class="form-group">

                    <div class="form-group">

                        <a href="<?php echo $this->facebook->login_url(); ?>"><img
                                    src="<?php echo base_url() ?>login_facebook.png" width="100%"
                                    class="img-responsive"></a>

                    </div>

                </div>


                <form role="form" method="post" action="<?php echo base_url(); ?>user_authentication/user_login_check">

                    <div class="form-group login-input">

                        <i class="fa fa-user overlay"></i>

                        <input type="text" name="email" class="form-control text-input" placeholder="Enter your Email"
                               required="">

                    </div>

                    <div class="form-group login-input">

                        <i class="fa fa-key overlay"></i>

                        <input type="password" name="user_password" class="form-control text-input"
                               placeholder="Enter your password" required="">

                    </div>

                    <div class="form-group">

                        <h3>

                            <?php
                            $msg = $this->session->userdata('message');

                            if ($msg) {
                                ?>

                                <?php echo $msg; ?>

                                <?php
                                $this->session->unset_userdata('message');
                            }
                            ?>

                        </h3>

                    </div>
                    <a href="<?php echo base_url(); ?>reset_password.html" class="forgot-password">
                        <p style="color:#53c1ed"><u>Forgot the password?</u>
                        <p>
                    </a>

                    <div class="row">

                        <div class="col-sm-12">

                            <button type="submit" name="submit" class="btn btn-success btn-block">Sign In</button>

                        </div>

                    </div>

                    <div class="row" style="padding-top: 10px">
                        <div class="col-sm-12" align="center">
                            <a href="<?php echo base_url(); ?>sign-up" class="btn btn-danger">
                                <i class="fa fa-lock" aria-hidden="true"></i>
                                Don't have account? Please sign up!
                            </a>

                        </div>
                    </div>


                </form>

            </div>

        </div>


    </div>

</div>

<!-- the overlay modal element -->

<div class="md-overlay"></div>

<!-- End of eoverlay modal -->

<script>

    var resizefunc = [];

</script>

<?php echo $footer_content; ?>

</body>

</body>

</html>


<!DOCTYPE html>

<html>

    <head>

        <meta charset="UTF-8">

        <title><?php echo $title; ?></title>   

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <meta name="apple-mobile-web-app-capable" content="yes" />

        <meta name="description" content="">

        <meta name="keywords" content="coco bootstrap template, coco admin, bootstrap,admin template, bootstrap admin,">

        <meta name="author" content="Huban Creative">



        <?php echo $header_content; ?>

    </head>

    <body class="fixed-left login-page">

        <!-- Begin page -->

        <div class="container">

            <div class="full-content-center">

                <div class="row">



                </div>

                <div class="login-wrap animated flipInX">

                    <div class="login-block">

                        <p class="text-center"><a href="<?php echo base_url();?>"><img src="<?php echo base_url(); ?>adminAssets/img/login-logo.png" alt="Logo" height="80" width="240"></a></p>

                        <div class="form-group">

                            <div class="form-group">

                                

                            </div>

                        </div>

                        <form role="form" method="post" action="<?php echo base_url(); ?>reset_password/update_password">

                            <div class="form-group login-input">

                                <i class="fa fa-user overlay"></i>
								<?php if(isset($code)) { ?>
									<input type="hidden" name="user_code" class="form-control text-input" value = "<?php echo $code;?>" required="">
								<?php } ?>
                                <input type="email" name="email" class="form-control text-input" value = "<?php echo (isset($email)) ? $email : '';?>" required="">

                            </div>
							<div class="form-group login-input">

                                <i class="fa fa-key overlay"></i>

                                <input type="password" name="user_password" class="form-control text-input" placeholder="Enter your password" required="">

                            </div>
							<div class="form-group login-input">

                                <i class="fa fa-key overlay"></i>

                                <input type="password" name="confirm_password" class="form-control text-input" placeholder="Confirm your password" required="">

                            </div>
							<div class="form-group">
								<h3>

                                    <?php

                                     $this->session->userdata('message');

                                    ?>

                                </h3>
							</div>

                            <div class="row">

                                <div class="col-sm-8">

                                    <button type="submit" name="submit" class="btn btn-success btn-block">Update New Password</button>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>



            </div>

        </div>

        <!-- the overlay modal element -->

        <div class="md-overlay"></div>

        <!-- End of eoverlay modal -->

        <script>

            var resizefunc = [];

        </script>

        <?php echo $footer_content; ?>

    </body>

</body>

</html>
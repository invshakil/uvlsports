<script>
    history.pushState(null, null, document.URL);
    window.addEventListener('popstate', function () {
        history.pushState(null, null, document.URL);
        alert('Please Click on Update Post!');
    });
</script>

<!--<div class="content-page">-->
<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
    <!-- Page Heading Start -->
    <div class="page-heading">
        <h1><i class='fa fa-bars'></i> &nbsp&nbsp&nbsp Edit This Post</h1>
    </div>
    <!-- Page Heading End-->
    <div class="row">

        <div class="col-md-12">
            <div class="widget">
                <div class="widget-content padding">
                    <div class="tab-content tab-boxed">
                        <div class="tab-pane fade active in" id="demo2-home">
                            <!-- Start of Add Category Content -->
							<?php if ($this -> session -> userdata('access_level') == 3) { ?>
                            <form role="form" name="edit_post_info" id="registerForm"
                                  action="<?php echo base_url(); ?>user_dashboard/update_post.html" method="post"
                                  enctype="multipart/form-data">
								
								<?php } else { ?>
                                <form role="form" name="edit_post_info" id="registerForm"
                                      action="<?php echo base_url(); ?>post_controller/update_post.html" method="post"
                                      enctype="multipart/form-data">
									<?php } ?>
                                    <h3>
										<?php
										$msg = $this -> session -> userdata('message');
										if ($msg) {
											?>
                                            <span class="label label-success"><?php echo $msg; ?></span>
											<?php
											$this -> session -> unset_userdata('message');
										}
										?>
                                    </h3>
                                    <div class="form-group">
                                        <label class="widget-header"><h2><strong>Post</strong> Title</h2></label>
                                        <input type="text" class="form-control" name="post_title"
                                               value="<?php echo $post_info -> post_title; ?>">
                                        <input type="hidden" class="form-control" name="post_id"
                                               value="<?php echo $post_info -> post_id; ?>">
                                        <div class="label-warning"><?php echo form_error('post_title'); ?></div>
                                    </div>
                                    <div class="widget">
                                        <div class="widget-header">
                                            <h2><strong>Post</strong> Description</h2>
                                            <div class="additional-btn">
                                                <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                                                <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                                                <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
                                            </div>
                                        </div>
                                        <div class="alert alert-warning">
                                        <span>
                                            <p>Please Avoid these common mistakes:</p><br/>
                                            <p>1. Don't give space before full stop, comma or any expression sign. Use Space after these signs. ( । , ! ? )</p>
                                            <p>2. Please avoid english words in title, use less english letter in description</p>
                                            <p>3. Try to use standard Bangla. e.g. করতেছে -> করছে, খাইতেছে->খাচ্ছে</p>
                                        </span>
                                        </div>
                                        <div class="widget-content">
                                            <textarea id="summernote"
                                                      name="post_description"><?php echo $post_info -> post_description; ?></textarea>
                                            <div class="label-warning"><?php echo form_error('post_description'); ?></div>
                                        </div>
                                    </div>

                                    <!--Only Admin Can see this option -->
									<?php if ($this -> session -> userdata('access_level') != 3) { ?>
                                        <div class="form-group">
                                            <label class="widget-header"><h2><strong>Cover</strong> Image(620x348)</h2>
                                            </label>
                                            <div class="col-sm-10">
                                                <input type="file" name="post_image"
                                                       value="<?php echo base_url() . $post_info -> post_image; ?>"
                                                       class="btn btn-default btn-lg" title="Update Image"><br/>
                                                <img src="<?php echo base_url() . './image_upload/post_image/resized/' . $post_info -> post_image; ?>">
                                                <div class="label-warning"><?php echo form_error('post_image'); ?></div>
                                            </div>
                                        </div>
									<?php } ?>

                                    <div class="form-group">
                                        <label class="widget-header"><h2><strong>Select</strong> Category</h2></label>
                                        <div class="col-sm-4">
                                            <select name="post_category_id" class="form-control selectpicker">
												<?php
												foreach ($all_published_category as $category) {
													?>
                                                    <option value="<?php echo $category -> category_id; ?>"><?php echo $category -> category_name; ?></option>
												<?php } ?>
                                            </select>
                                            <div class="label-warning"><?php echo form_error('post_category_id'); ?></div>
                                        </div>
                                    </div>
                                    <hr/>

                                    <!--Only Admin Can see this option -->
									<?php if ($this -> session -> userdata('access_level') != 3) { ?>
                                        <div class="form-group">

                                            <label class="widget-header"><h2><strong>Enter</strong> Tags</h2></label>
	
											<?php if ($post_info -> post_tag_id == '') { ?>
                                            <div class="row">
                                                <div class="col-sm-12">
													
			
														<?php
														$tag_info = $this -> db -> join('tbl_tags t', 't.tag_id = ta.tag_id') -> get_where('post_multi_tag ta', array ('post_id' => $post_info -> post_id)) -> result_array();
														?>
                                                        <b style="padding-left: 10px;">Selected Tags:</b> <?php
														if ($tag_info != '') {
															foreach ($tag_info as $tags) {
																echo '<button class="btn btn-success">' . $tags['tag_name'] . " </button> ";
															}
														} else {
															echo 'Please Insert Tags for this post!';
														}
			
													 ?>
                                                </div>
                                            </div>
                                                <br>
                                            <?php }?>
                                            

                                            <div class="row">
                                                <div class="col-sm-6" id="output">

                                                    <input type="text" value="<?php echo $post_info->post_tag_id; ?>" class="tags form-control"
                                                           name="post_tag_id"/>
                                                    <br/><br/>

                                                    <div class="items"></div>

                                                </div>
                                                <div class="col-sm-6" style="margin-top: 10px;">
                                                    <span class="alert alert-info">Enter Tags Name and press enter or comma to separate</span>
                                                </div>
                                            </div>
                                            

                                        </div>

                                        <div class="form-group">
                                            <label class="widget-header"><h2><strong>Featured</strong> Status</h2>
                                            </label>
                                            <div class="radio iradio">
                                                <label>
                                                    <input type="radio" name="featured_status" id="optionsRadios1"
                                                           value="1" checked>&nbsp; Yes, on Slider
                                                </label>
                                            </div>
                                            <hr/>
                                            <div class="radio iradio">
                                                <label>
                                                    <input type="radio" name="featured_status" id="optionsRadios2"
                                                           value="0">&nbsp; Nope!
                                                </label>
                                            </div>
                                            <hr/>
                                        </div>
									<?php } ?>

                                    <div class="form-group">
                                        <label class="widget-header"><h2><strong>Publication</strong> Status</h2>
                                        </label>

                                        <!--Only Admin Can see this option -->
										<?php if ($this -> session -> userdata('access_level') != 3) { ?>
                                            <div class="radio iradio">
                                                <label>
                                                    <input type="radio" name="post_publication_status"
                                                           id="optionsRadios1" value="1">&nbsp; Published
                                                </label>
                                            </div>


                                            <hr/>
                                            <div class="radio iradio">
                                                <label>
                                                    <input type="radio" name="post_publication_status"
                                                           id="optionsRadios2" value="0" checked>&nbsp; Pending
                                                </label>
                                            </div>
                                            <div class="label-warning"><?php echo form_error('post_publication_status'); ?></div>
                                            <hr/>
										<?php } ?>
                                    </div>

                                    <!--Only Admin Can see this option -->
									<?php if ($this -> session -> userdata('access_level') != 3) { ?>
                                        <div class="form-group">
                                            <label class="widget-header"><h2><strong>SEO URL (English words separated by
                                                        dash)</strong> [e.g this-is-the-title-link]</h2></label>
                                            <input type="text" class="form-control" name="en_post_url"
                                                   value="<?php echo $post_info -> en_post_url; ?>">
                                            <div class="label-warning"><?php echo form_error('en_post_url'); ?></div>
                                        </div>

                                        <div class="form-group">

                                            <label class="widget-header"><h2><strong>Most Important two lines (160
                                                        character max) about this post which defines the post</strong>
                                                </h2></label>
                                            <div class="alert alert-warning">
                                            <span style="color: red; font-weight: bolder">
                                                <p>[যেমনঃ ইংলিশ প্রিমিয়ার লিগে আজ চেলসির মুখোমুখি আর্সেনাল। বাংলাদেশ সময় রাত ৯টায় এই ম্যাচটি অনুষ্ঠিত হবে।] - এটি দেখে ইনফরমেটিভ মনে হচ্ছে।</p>
                                            </span>
                                            </div>
                                            <input type="text" class="form-control" name="meta_description"
                                                   value="<?php if ($post_info -> meta_description != FALSE) echo $post_info -> meta_description; ?>">

                                            <div class="label-warning"><?php echo form_error('meta_description'); ?></div>

                                        </div>
									<?php } ?>

                                    <hr/>
                                    <br/>

                                    <button type="submit" name="submit" class="btn btn-primary btn-lg btn-block">Update
                                        Post
                                    </button>

                                </form>

                        </div>

                    </div>
                    <hr>

                </div>
            </div>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End content here -->
<!-- ============================================================== -->

<script type="text/javascript">
    document.forms['edit_post_info'].elements['post_category_id'].value = '<?php echo $post_info -> post_category_id ?>';
    document.forms['edit_post_info'].elements['featured_status'].value = '<?php echo $post_info -> featured_status ?>';
    document.forms['edit_post_info'].elements['post_publication_status'].value = '<?php echo $post_info -> post_publication_status ?>';
</script>

<style>
    img {
        max-width: 100%;
    }

    input[type=file] {
        padding: 10px;
        background: #2d2d2d;
    }

    .items span {
        display: inline-block;
        margin: 5px;
        padding: 5px;
        border: 1px solid #CCC;
    }

    .bootstrap-tagsinput {
        width: 100% !important;
    }

</style>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js"></script>
<script>
    $(document).ready(function () {
        $('.tags').tagsinput({
            allowDuplicates: true
        });

        $('.tags').on('itemAdded', function (item, tag) {
            $('.items').html('');
            var tags = $('.tags').tagsinput('items');
            $.each(tags, function (index, tag) {
                $('.items').append('<span>' + tag + '</span>');
            });
        });
    });

    $(document).ready(function () {
        $('.tags').tagsinput({
            allowDuplicates: false
        });

    });
</script>


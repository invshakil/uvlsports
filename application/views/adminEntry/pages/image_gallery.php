<div class="Content">

    <div class="widget transparent animated fadeInDown" style="padding: 15px">
        <div class="box-info animated fadeInDown">
            <h2><strong>Gallery</strong> with caption</h2>

            <div class="gallery-wrap">
                
                <?php foreach ($all_images as $image) {?>
                
                <div class="column col-md-1 col-sm-2 col-xs-6" style="width: auto!important;">
                    <div class="inner">
                        <a onclick="copyToClipboard('#image<?php echo $image->post_id; ?>')" class="img-thumbnail img-responsive img-center" title="<?php echo $image->post_title; ?>">
                            <div class="img-wrap" style="cursor: copy;max-height: 100px">
                                <img src="<?php echo base_url() . './image_upload/post_image/thumbs/' . $image -> post_image; ?>" alt="Image gallery" title="<?php echo $image->post_title; ?>"
                                     class="mfp-fade">
                            </div>
                            <div class="caption-hover" style="position: inherit">
                                <p style="display: none">Image Path: <em id="image<?php echo $image->post_id; ?>"><?php echo $image -> post_image; ?> </em></p>
                                <button onclick="copyToClipboard('#image<?php echo $image->post_id; ?>')" class="btn btn-danger">Copy Image Source</button>
                            </div>
                        </a>
                    </div>
                </div>
                
                <?php } ?>
                
                
            </div>
			<?php
			foreach ($links as $link) {
				echo $link;
			}
			?>

        </div>
    </div>


</div>

<script>
    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        $temp.remove();
        toastr.success('Image Source Copied to clipboard');
//        alert('Image Source Copied to clipboard');
    }

</script>

<link href="<?php echo base_url(); ?>adminAssets/libs/magnific-popup/magnific-popup.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>adminAssets/libs/magnific-popup/jquery.magnific-popup.min.js"></script>
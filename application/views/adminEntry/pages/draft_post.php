<div class="Content">

    <div class="page-heading">

        <h1><i class='fa fa-bars'></i> &nbsp;&nbsp;&nbsp;Draft New Article</h1>

    </div>

    <div class="widget col-md-8 col-md-offset-2">

        <div class="widget-content padding">

            <div class="tab-content tab-boxed">

                <div class="tab-pane fade active in " id="demo2-home">


                    <form role="form" id="registerForm"
                          action="<?php echo base_url(); ?>post_controller/save_draft_post" method="post"
                          enctype="multipart/form-data">
                        <h3>
                            <?php
                                $this->session->userdata('message');
                            ?>

                        </h3>

                        <div class="form-group">

                            <label class="widget-header"><h2><strong>Post</strong> Title</h2></label>

                            <input type="text" class="form-control" name="post_title" id="post_title"
                                   value="<?php echo set_value('post_title'); ?>">

                            <div class="label-warning"><?php echo form_error('post_title'); ?></div>

                        </div>

                        <div class="widget">

                            <div class="widget-header">

                                <h2><strong>Post</strong> Description</h2>

                                <div class="additional-btn">

                                    <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>

                                    <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>

                                    <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>

                                </div>

                            </div>
                            <div class="alert alert-warning">
                                            <span>
                                                <p>Please Avoid these common mistakes:</p><br/>
                                                <p>1. Don't give space before full stop, comma or any expression sign. Use Space after these signs. ( । , ! ? )</p>
                                                <p>2. Please avoid english words in title, use less english letter in description</p>
                                                <p>3. Try to use standard Bangla. e.g. করতেছে -> করছে, খাইতেছে->খাচ্ছে</p>
                                            </span>
                            </div>
                            <div class="widget-content">

                                            <textarea id="summernote" name="post_description" id="post_description"
                                                      value="<?php echo set_value('post_description'); ?>"></textarea>

                                <div class="label-warning"><?php echo form_error('post_description'); ?></div>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="widget-header"><h2><strong>Select</strong> Category</h2></label>

                            <div class="col-sm-6">

                                <select name="post_category_id" class="form-control selectpicker"
                                        data-live-search="true">

                                    <?php
                                    foreach ($all_published_category as $category) {
                                        ?>

                                        <option value="<?php echo $category->category_id; ?>"><?php echo $category->category_name; ?></option>

                                    <?php } ?>

                                </select>

                                <div class="label-warning"><?php echo form_error('post_category_id'); ?></div>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="widget-header"><h2><strong>Author</strong> Name: </h2></label>

                            <span><input type="text" style="font-size: 20px; font-weight: bold;"
                                         class="btn btn-green-3"
                                         value="<?php echo $this->session->userdata('user_name'); ?>"
                                         disabled=""></span>

                        </div>

                        <hr/>

                        <br/>


                        <button type="submit" name="submit" class="btn btn-primary btn-lg btn-block">Draft Post

                        </button>


                    </form>


                    <!-- End of Add Category Content -->

                </div> <!-- / .tab-pane -->


                <!-- / .tab-pane -->

            </div> <!-- / .tab-content -->

            <hr>


        </div>

    </div>


</div>
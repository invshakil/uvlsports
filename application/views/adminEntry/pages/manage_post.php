
<div class="content">

    <!-- Page Heading Start -->

    <div class="page-heading">

        <h1><i class='fa fa-bars'></i> &nbsp&nbsp&nbsp Manage Post</h1>

        <?php if ($this->session->userdata('message')) { ?>
            <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                <?php
                 $this->session->userdata('message');


                ?>

            </h3>
        <?php } ?>
    </div>


    <!-- Page Heading End-->
    <div class="row">


        <div class="col-md-12">

            <div class="widget">

                <div class="pull-left">
                    <a class="btn btn-default" href="<?php echo base_url(); ?>post_controller/browse_pending_post">Browse
                        Pending Post</a>
                </div>

                <div class="row" style="padding-right: 15px;">

                    <div class="input-group col-md-4 col-md-offset-8">
                        <form role="form" method="get">
                            <input type="text" name="name" class="form-control"
                                   placeholder="Search by Post Title or Part of post description...">
                        </form>
                        <span class="input-group-btn">
                            <button class="btn btn-danger" type="button">
                                <span class=" glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                    </div>


                </div>

                <div class="widget-content table-responsive ">
                    <br/>

                        <table class="table table-hover table-bordered display" cellspacing="0" width="100%">

                            <thead align="center">

                            <tr>

<!--                                <th style="text-align: center; width: 2%;">ID</th>-->

                                <th style="text-align:">Post Title</th>

                                <th style="text-align:">Category Name</th>

                                <th style="text-align:">Author Name</th>

                                <th style="text-align:">Post Date</th>

                                <th style="text-align:">Post Image</th>

                                <th style="text-align:">Publication Status</th>

                                <th style="text-align:">Currently Editing By</th>

                                <th style="text-align:">Action</th>

                            </tr>

                            </thead>


                            <tbody>

                            <?php
                            foreach ($all_post as $post) {
                            ?>

                            <tr>

<!--                                <td>--><?php //echo $post->post_id; ?><!--</td>-->

                                <?php if ($post->post_publication_status != 1) { ?>
                                    <td align="center"><?php echo $post->post_title; ?></td>
                                <?php } else { ?>
                                    <td align="center"><strong><a  style="color: #0A487C"
                                                    href="<?php echo base_url(); ?>news-details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>"
                                                    target="_blank"><?php echo $post->post_title; ?></a></strong></td>
                                <?php } ?>
                                <td align="center"><?php echo $post->category_name; ?></td>

                                <td align="center">
                                    <div class="label label-info">
                                        <?php echo $post->user_name; ?>
                                    </div>
                                </td>

                                <td align="center">
                                    <div class="label label-primary">
                                        <?php $date = $post->post_date;
                                        echo date("D, dS F/Y h:i A", strtotime($date));
                                        ?>
                                    </div>

                                </td>

                                <td align="center"><img
                                            src="<?php echo base_url() . './image_upload/post_image/thumbs/' . $post->post_image; ?>" width="80">
                                </td>


                                <td align="center">

                                    <?php if ($post->post_publication_status == 1) { ?>

                                        <label class="label label-success"><a title="change publication status"
                                                                                 href="<?php echo base_url(); ?>post_controller/unpublish_post/<?php echo $post->post_id ?>"><?php echo "Published"; ?></a></label>


                                    <?php } else {
                                        ?>

                                        <label class="label label-danger"><a title="change publication status"
                                                                                  href="<?php echo base_url(); ?>post_controller/publish_post/<?php echo $post->post_id ?>"><?php echo "Pending"; ?></a></label>

                                    <?php } ?>

                                </td>

                                <td align="center" style="width: 200px;">
                                    <?php if ($post->last_modified_by == NULL) {
                                        echo '<label class="alert alert-warning">You Can Edit this file</label>';
                                    } else {
                                        echo '<label class="alert alert-danger">' . $post->last_modified_by . ' is editing this post.</label>';
                                    } ?>
                                </td>

                                <td>

                                    <div class="btn-group btn-group-xs" style="width: 100px;">

                                        <?php if ($post->edit_lock_status != 1) { ?>

                                            <div class="pull-left"><a data-toggle="tooltip" title="Edit"
                                                                      href="<?php echo base_url(); ?>post_controller/edit_post/<?php echo $post->post_id ?>"
                                                                      class="btn btn-info"><i class="fa fa-edit"></i></a>
                                            </div>

                                        <?php } elseif($post->last_modified_by == $this->session->userdata('user_name')) { ?>
                                            <div class="pull-left"><a data-toggle="tooltip" title="Edit"
                                                                      href="<?php echo base_url(); ?>post_controller/edit_post/<?php echo $post->post_id ?>"
                                                                      class="btn btn-info"><i class="fa fa-edit"></i></a>
                                            </div>

                                        <?php }?>

                                        <div class="pull-right"><a data-toggle="tooltip" title="Delete"
                                                                   href="<?php echo base_url(); ?>post_controller/delete_post/<?php echo $post->post_id ?>"
                                                                   onclick="return checkDelete();"
                                                                   class="btn btn-danger"><i
                                                        class="fa fa-trash-o"></i></a></div>

                                    </div>

                                </td>

                                <?php } ?>

                            </tr>

                            </tbody>

                        </table>

<!--                    </form>-->

                </div>

                <div class="data-table-toolbar">
                    <!--<ul class="pagination">-->
                    <?php
                    foreach ($links as $link) {
                        echo $link;
                    }
                    ?>
                    <!--</ul>-->
                </div>

            </div>

        </div>


    </div>

    <hr>


</div>


<!-- ============================================================== -->

<!-- End content here -->

<!-- ============================================================== -->


<!--</div>-->
<!--<script>-->
<!--    $(function () {-->
<!--        $('#myTable').DataTable({-->
<!--            'paging': true,-->
<!--            'lengthChange': true,-->
<!--            'searching': true,-->
<!--            'ordering': true,-->
<!--            'info': true,-->
<!--            'autoWidth': false-->
<!--        })-->
<!--    })-->
<!---->
<!--</script>-->

<script type="text/javascript">

    function checkDelete() {


        var chk = confirm('Are you sure about to delete this post?');

        if (chk) {

            return true;

        } else {

            return false;

        }

    }


</script>

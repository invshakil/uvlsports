

<div class="content">

    <!-- Page Heading Start -->

    <div class="page-heading">

        <h1><i class='fa fa-bars'></i> &nbsp&nbsp&nbsp Manage Post</h1>

        <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

            <?php
                $this->session->userdata('message');

            ?>

        </h3>

    </div>

    <!-- Page Heading End-->				<div class="row">



        <div class="col-md-12">

            <div class="widget">

                <div class="widget-content">

                    <br>					

                    <!--<div class="table-responsive">-->

                    <form class='form-horizontal' role='form'>

                        <table class="table table-striped table-bordered" cellspacing="0" width="100%">

                            <thead align="center" >

                                <tr>

                                    <th style="text-align: center;">Post Title</th>

                                    <th style="text-align: center;">Category Name</th>

                                    <th style="text-align: center;">Post Date</th>

                                    <th style="text-align: center;">Post Description</th>

                                    <th style="text-align: center;">Publication Status</th>

                                    <th style="text-align: center;">Action</th>

                                        <!--<th style="text-align: center;">Tags</th>-->



                                </tr>

                            </thead>



                            <tbody>

                                <?php
                                if (is_array($all_post) && count($all_post) >= 1) {

                                    foreach ($all_post as $post) {
                                        ?>

                                        <tr>


                                            <?php if ($post->post_publication_status != 1) { ?>
                                                <td align="center"><?php echo $post->post_title; ?></td>
                                            <?php } else { ?>
                                                <td align="center"><strong><a style="color: #0A487C"
                                                                href="<?php echo base_url(); ?>news_details/<?php echo $post->post_id; ?>/<?php echo $post->post_category_id; ?>/<?php echo $post->en_post_url; ?>"
                                                                target="_blank"><?php echo $post->post_title; ?></a></strong></td>
                                            <?php } ?>

                                            <td align="center"><?php echo $post->category_name; ?></td>

                                            <td align="center"><?php $date = $post->post_date;
                                                echo date("F j, Y, g:i a", strtotime($date)); ?></td>

                                            <td align="justify"><?php
                                                $str = $post->post_description;

                                                $length = strlen($str);



                                                if ($length > 600) {

                                                    $strCut = substr($str, 0, 600);

                                                    $str = substr($strCut, 0, strrpos($strCut, ' ')) . '... <a href="' . base_url() . 'post_controller/edit_post/' . $post->post_id . '">edit</a>';
                                                }

                                                echo $str;
                                                ?>



                                            </td>


                                            <td align="center">

                                                <?php if ($post->post_publication_status == 1) { ?>

                                                    <label class="btn btn-green-1 btn-lg"><?php echo "Published"; ?></label>



                                                <?php } else {
                                                    ?>

                                                    <label class="btn btn-yellow-1 btn-lg"><?php echo "Pending"; ?></label>   

                                                <?php } ?>

                                            </td>

                                            <td >

                                                <div class="btn-group btn-group-xs" style="width: 50px;">

                                                    <a data-toggle="tooltip" title="Edit" href="<?php echo base_url(); ?>user_dashboard/edit_post/<?php echo $post->post_id ?>" class="btn btn-default"><i class="fa fa-edit"></i></a>

                                                </div>

                                            </td>

                                        </tr>

                                        <?php
                                    }
                                } else {
                                    echo "<div class='alert alert-info'><h3>&nbsp&nbsp&nbsp&nbspYou have not posted anything yet!</h3></div>";
                                }
                                ?>

                            </tbody>

                        </table>

                    </form>

                </div>

            </div>

        </div>



    </div>

    <hr>



</div>



<!-- ============================================================== -->

<!-- End content here -->

<!-- ============================================================== -->



<!--</div>-->

<script type="text/javascript">

    function checkDelete() {



        var chk = confirm('Are You Sure to Delete This ?');

        if (chk)

        {

            return true;

        } else {

            return false;

        }

    }



</script>
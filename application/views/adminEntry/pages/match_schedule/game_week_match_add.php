<!--<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>-->

        <script src="<?php echo base_url(); ?>adminAssets/libs/jquery/jquery-1.11.1.min.js"></script>

<div class="content">

    <!-- Page Heading Start -->

    <div class="page-heading">

        <h1><i class='fa fa-bars'></i> &nbsp&nbsp&nbspAdd Match To Game Week</h1>

    </div>


    <div class="col-md-10 col-md-offset-1">

        <div class="widget">

            <div class="widget-content padding">

                <div class="tab-content tab-boxed">

                    <div class="tab-pane fade active in" id="demo2-home">

                        <!-- Start of Add Category Content -->


                        <form role="form" id="registerForm"
                              action="<?php echo base_url(); ?>match_schedule/save_match_info" method="post"
                              enctype="multipart/form-data">
                            <!--<form role="form" id="registerForm"  method="post"  enctype="multipart/form-data">-->

                            <h3>

                                <?php

                                $this->session->userdata('message');


                                ?>

                            </h3>
                            <div class="form-group">

                                <label class="widget-header"><h2><strong>Select</strong> Game Week</h2></label>

                                <div class="col-sm-6">

                                    <select name="gw_id" class="form-control selectpicker"
                                            data-live-search="true">

                                        <?php $all_gameweek = $this->db->limit(1)->order_by('gw_id','desc')->get('tbl_gwtitle')->result();
                                        foreach ($all_gameweek as $category) {
                                            ?>

                                            <option value="<?php echo $category->gw_id; ?>"><?php echo $category->game_week; ?></option>

                                        <?php } ?>

                                    </select>

                                    <div class="label-warning"><?php echo form_error('gw_id'); ?></div>

                                </div>

                            </div>
                            <div class="form-group">

                                <label class="widget-header"><h2><strong>Select</strong> League</h2></label>

                                <div class="col-sm-6">

                                    <select name="match_league" class="form-control selectpicker">
                                            <option value="English Premier League">English Premier League</option>
                                            <option value="English League Cup">English League Cup</option>
                                            <option value="FA CUP">FA CUP</option>
                                            <option value="La Liga">La Liga</option>
                                            <option value="Copa Del Rey">Copa Del Rey</option>
                                            <option value="Serie A">Serie A</option>
                                            <option value="Coppa Italia">Coppa Italia</option>
                                            <option value="Bundesliga">Bundesliga</option>
                                            <option value="League 1">League 1</option>
                                            <option value="Champions League">Champions League</option>
                                            <option value="Europa League">Europa League</option>
                                            <option value="International Friendly">International Friendly</option>
                                            <option value="World Cup Qualifier">World Cup Qualifier</option>
                                            <option value="World Cup">World Cup</option>

                                    </select>

                                    <div class="label-warning"><?php echo form_error('match_league'); ?></div>

                                </div>

                            </div>

                            <div class="form-group">

                                <label class="widget-header"><h2><strong>Match</strong> Name</h2></label>

                                <input type="text" class="form-control" name="match_title" id="match_title"
                                       value="<?php echo set_value('match_title'); ?>">

                                <div class="label-warning"><?php echo form_error('match_title'); ?></div>

                            </div>

                            <div class="form-group" >

                                <label class="widget-header"><h2><strong>Match</strong> Time</h2></label>

                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' class="form-control" name="match_time"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                <div class="label-warning"><?php echo form_error('match_time'); ?></div>

                            </div>

                            <div class="form-group">

                                <label class="widget-header"><h2><strong>Match</strong> Channel</h2></label>

                                <input type="text" class="form-control" name="match_channel" id="match_channel"
                                       value="<?php echo set_value('match_channel'); ?>">

                                <div class="label-warning"><?php echo form_error('match_channel'); ?></div>

                            </div>


                            <div class="form-group">

                                <label class="widget-header"><h2><strong>Publication</strong> Status</h2>
                                </label>

                                <select name="m_publication_status" class="form-control selectpicker">
                                    <option value="1">Published</option>
                                    <option value="0">Pending</option>

                                </select>

                                <div class="label-warning"><?php echo form_error('m_publication_status'); ?></div>

                                <hr/>

                            </div>

                            <button type="submit" name="submit" class="btn btn-primary btn-lg btn-block">Add
                                Match to Game Week
                            </button>


                        </form>


                        <!-- End of Add Category Content -->

                    </div> <!-- / .tab-pane -->


                    <!-- / .tab-pane -->

                </div> <!-- / .tab-content -->

                <hr>


            </div>

        </div>


    </div>

</div>

</div>

<!-- ============================================================== -->

<!-- End content here -->

<!-- ============================================================== -->
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker();
    });
</script>
<!--<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>-->

<script src="<?php echo base_url(); ?>adminAssets/libs/jquery/jquery-1.11.1.min.js"></script>

<div class="content">

    <!-- Page Heading Start -->

    <div class="page-heading">

        <h1><i class='fa fa-bars'></i> &nbsp;&nbsp; Update Match Information</h1>

    </div>


    <div class="col-md-10 col-md-offset-1">

        <div class="widget">

            <div class="widget-content padding">

                <div class="tab-content tab-boxed">

                    <div class="tab-pane fade active in" id="demo2-home">

                        <!-- Start of Add Category Content -->


                        <form role="form" id="registerForm" action="<?php echo base_url(); ?>match_schedule/update_match_info" method="post">

                            <h3>

                                <?php

                                $this->session->userdata('message');


                                ?>

                            </h3>
                            <div class="form-group">

                                <label class="widget-header"><h2><strong>Select</strong> Game Week</h2></label>

                                <div class="col-sm-6">

                                    <select name="gw_id" class="form-control selectpicker"
                                            data-live-search="true">

                                        <?php $all_gameweek = $this->db->get('tbl_gwtitle')->result();
                                        foreach ($all_gameweek as $category) {
                                            ?>

                                            <option value="<?php echo $category->gw_id; ?>" <?php if($match_info->gw_id == $category->gw_id) echo 'selected';?>
                                            ><?php echo $category->game_week; ?></option>

                                        <?php } ?>

                                    </select>

                                    <div class="label-warning"><?php echo form_error('gw_id'); ?></div>

                                </div>

                            </div>
                            <div class="form-group">

                                <label class="widget-header"><h2><strong>Select</strong> League</h2></label>

                                <div class="col-sm-6">

                                    <select name="match_league" class="form-control selectpicker">
                                        <option value="English Premier League" <?php if($match_info->match_league == 'English Premier League') echo 'selected';?>>English Premier League</option>
                                        <option value="La Liga" <?php if($match_info->match_league == 'La Liga') echo 'selected';?>>La Liga</option>
                                        <option value="Seria A" <?php if($match_info->match_league == 'Seria A') echo 'selected';?>>Seria A</option>
                                        <option value="Bundesliga" <?php if($match_info->match_league == 'Bundesliga') echo 'selected';?>>Bundesliga</option>
                                        <option value="Champions League" <?php if($match_info->match_league == 'Champions League') echo 'selected';?>>Champions League</option>
                                        <option value="Europa League" <?php if($match_info->match_league == 'Europa League') echo 'selected';?>>Europa League</option>
                                        <option value="International Friendly" <?php if($match_info->match_league == 'International Friendly') echo 'selected';?>>International Friendly</option>
                                        <option value="World Cup Qualifier" <?php if($match_info->match_league == 'World Cup Qualifier') echo 'selected';?>>World Cup Qualifier</option>
                                        <option value="World Cup" <?php if($match_info->match_league == 'World Cup') echo 'selected';?>>World Cup</option>

                                    </select>

                                    <div class="label-warning"><?php echo form_error('match_league'); ?></div>

                                </div>

                            </div>

                            <div class="form-group">

                                <label class="widget-header"><h2><strong>Match</strong> Name</h2></label>

                                <input type="text" class="form-control" name="match_title" id="match_title"
                                       value="<?php echo $match_info->match_title; ?>">

                                <div class="label-warning"><?php echo form_error('match_title'); ?></div>

                            </div>

                            <div class="form-group" >

                                <label class="widget-header"><h2><strong>Match</strong> Time</h2></label>

                                <div class='input-group date' id='datetimepicker1'>
                                    <label class="label label-danger">Current Time: <?php echo date('M j, Y - g:i a', $match_info->match_time);?></label>

                                    <input type='text' class="form-control" name="match_time" value="<?php echo date('m/d/Y H:i A', $match_info->match_time);?>"/>

                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                <div class="label-warning"><?php echo form_error('match_time'); ?></div>

                            </div>

                            <div class="form-group">

                                <label class="widget-header"><h2><strong>Match</strong> Channel</h2></label>

                                <input type="text" class="form-control" name="match_channel" id="match_channel"
                                       value="<?php echo $match_info->match_channel; ?>">

                                <div class="label-warning"><?php echo form_error('match_channel'); ?></div>

                            </div>


                            <div class="form-group">

                                <label class="widget-header"><h2><strong>Publication</strong> Status</h2>
                                </label>

                                <div class="radio iradio">

                                    <!--Only Admin Can see this option -->

                                    <label>

                                        <input type="radio" name="m_publication_status"
                                               id="optionsRadios1" value="1" <?php if($match_info->m_publication_status == 1) echo 'checked';?>>&nbsp; Published

                                    </label>
                                </div>

                                <hr/>

                                <div class="radio iradio">

                                    <label>

                                        <input type="radio" name="m_publication_status" id="optionsRadios2"
                                               value="0" <?php if($match_info->m_publication_status == 0) echo 'checked';?>>&nbsp; Pending

                                    </label>

                                </div>

                                <div class="label-warning"><?php echo form_error('m_publication_status'); ?></div>

                                <hr/>

                            </div>

                            <input type='hidden' class="form-control" name="match_id" value="<?php echo $match_info->match_id;?>"/>

                            <button type="submit" name="submit" class="btn btn-primary btn-lg btn-block">Update Match Info
                            </button>


                        </form>


                        <!-- End of Add Category Content -->

                    </div> <!-- / .tab-pane -->


                    <!-- / .tab-pane -->

                </div> <!-- / .tab-content -->

                <hr>


            </div>

        </div>


    </div>

</div>

</div>

<!-- ============================================================== -->

<!-- End content here -->

<!-- ============================================================== -->
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker();
    });
</script>
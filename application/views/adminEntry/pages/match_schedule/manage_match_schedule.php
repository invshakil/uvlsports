
<div class="content">

    <!-- Page Heading Start -->

    <div class="page-heading">

        <h1><i class='fa fa-bars'></i> &nbsp&nbsp&nbsp Manage Match Schedule</h1>

        <?php if ($this->session->userdata('message')) { ?>
            <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                <?php
                $this->session->userdata('message');


                ?>

            </h3>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="widget">
                <div class="widget-content">
                    <br>
                    <div class="table-responsive">
                        <div>
                            <table id="myTable" class="table table-bordered display" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th style="text-align: center; width: 20px">ID</th>
                                    <th style="text-align: center;">Game Week Title</th>
                                    <th style="text-align: center;">League</th>
                                    <th style="text-align: center;">Match Name</th>
                                    <th style="text-align: center;">Match Time</th>
                                    <th style="text-align: center;">Match Channel</th>
                                    <th style="text-align: center;">Publication Status</th>
                                    <th style="text-align: center;">Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php
                                foreach ($game_week_matches as $row) {
                                    ?>
                                    <tr>
                                        <td><?php echo $row->match_id; ?></td>
                                        <td><strong><?php echo $row->game_week; ?></strong></td>
                                        <td><strong><?php echo $row->match_league; ?></strong></td>
                                        <td><strong><?php echo $row->match_title; ?></strong></td>
                                        <td>
                                            <strong><?php echo date('Y-M-d h:i a', $row->match_time); ?></strong>
                                        </td>
                                        <td><strong><?php echo $row->match_channel; ?></strong></td>
                                        <td align="center">
                                            <?php if ($row->m_publication_status == 1) {
                                                ?>
                                                <a href="<?php echo base_url()?>match_schedule/status_to_pending/<?php echo $row->match_id?>" title="Change publication status to pending">
                                                    <span class="label label-success"><?php echo "Published"; ?></span>
                                                </a>
                                                <?php
                                            } else {
                                                ?>
                                                <a href="<?php echo base_url()?>match_schedule/status_to_published/<?php echo $row->match_id?>" title="Change publication status to published">
                                                    <span class="label label-danger"><?php echo "Pending"; ?></span>
                                                </a>
                                            <?php }
                                            ?>
                                        </td>
                                        <td align="center" style="width: 120px;">

                                            <div class="pull-right"><a data-toggle="tooltip" title="Delete"
                                                                       href="<?php echo base_url(); ?>match_schedule/delete_match/<?php echo $row->match_id ?>"
                                                                       onclick="return checkDelete();"
                                                                       class="btn btn-danger"><i
                                                            class="fa fa-trash-o"></i></a></div>

                                            <div class="pull-left">
                                                <div class="pull-left"><a data-toggle="tooltip" title="edit"
                                                                           href="<?php echo base_url(); ?>match_schedule/edit_match_schedule/<?php echo $row->match_id ?>"
                                                                           class="btn btn-info"><i
                                                                class="fa fa-pencil-square-o"></i></a></div>
                                            </div>

                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $('#myTable').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })

</script>

<script type="text/javascript">

    function checkDelete() {


        var chk = confirm('Are you sure about to delete this match fixture?');

        if (chk) {

            return true;

        } else {

            return false;

        }

    }


</script>
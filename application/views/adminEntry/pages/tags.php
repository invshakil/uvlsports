<!--<div class="content-page">-->
<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
    <!-- Page Heading Start -->
    <div class="page-heading">
        <h1><i class='fa fa-bars'></i> Add & Manage Tags</h1>
    </div>
    <!-- Page Heading End-->
    <div class="row">
        <div class="col-sm-12">
            <div class="widget">
                <div class="widget-content padding">
                    <ul id="demo2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#demo2-profile" data-toggle="tab">Manage Tags <span
                                        class="badge badge-primary"></span></a>
                        </li>
                        <li class="">
                            <a href="#demo2-home" data-toggle="tab">Add Tag <span
                                        class="label label-success"></span></a>
                        </li>
                    </ul>

                    <div class="tab-content tab-boxed">
                        <div class="tab-pane fade active in" id="demo2-profile">

                            <!-- Start of Manage Tag Content -->

                            <div class="widget">
                                <div class="widget-content">
                                    <br>
                                    <div class="table-responsive">
                                            <div>
                                                <table id="myTable" class="table table-bordered display" cellspacing="0" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th style="text-align: center; width: 20px">ID</th>
                                                        <th style="text-align: center;">Tag Name</th>
                                                        <th style="text-align: center;">Publication Status</th>
                                                        <th style="text-align: center;">Action</th>
                                                    </tr>
                                                    </thead>

                                                    <tbody>
                                                    <?php
                                                    foreach ($all_tag as $tag) {
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $tag->tag_id; ?></td>
                                                            <td><strong><?php echo $tag->tag_name; ?></strong></td>
                                                            <td align="center">
                                                                <?php if ($tag->publication_status == 1) {
                                                                    ?>
                                                                    <span class="label label-success"><?php echo "Published"; ?></span>

                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <span class="label label-danger"><?php echo "Pending"; ?></span>
                                                                <?php }
                                                                ?>
                                                            </td>
                                                            <td align="center" style="width: 120px;">

                                                                <div class="pull-right"><a data-toggle="tooltip" title="Delete"
                                                                                           href="<?php echo base_url(); ?>tags/delete_tag/<?php echo $tag->tag_id ?>"
                                                                                           onclick="return checkDelete();"
                                                                                           class="btn btn-danger"><i
                                                                                class="fa fa-trash-o"></i></a></div>

                                                                <div class="pull-left">
                                                                    <input type="button" name="edit" value="Edit" id="<?php echo $tag->tag_id; ?>" class="btn btn-info btn-xs edit_data" />
                                                                </div>

                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                    </div>

                                    <!-- MODAL -->

                                    <div id="add_data_Modal" class="modal fade">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;
                                                    </button>
                                                    <h4 class="modal-title">Update Tags Data</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form role="form" id="insert_form">

                                                        <div class="form-group">
                                                            <label class="widget-header"><h2><strong>Tag</strong> Name
                                                                </h2></label>
                                                            <input type="text" class="form-control" name="tag_name" id="tag_name">
                                                            <input type="hidden" class="form-control" name="tag_id" id="tag_id">
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="widget-header"><h2>
                                                                    <strong>Publication</strong> Status</h2></label>
                                                            <div class="radio iradio">
                                                                <select name="publication_status" id="publication_status" class="form-control">
                                                                    <option value="1">Published</option>
                                                                    <option value="0">Pending</option>
                                                                </select>
                                                            </div>

                                                            <hr/>
                                                        </div>

                                                        <input type="submit" name="insert" id="insert" value="Insert"
                                                               class="btn btn-success"/>
                                                        <hr/>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                                        Close
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <!-- End of Manage Tags Content -->
                        </div> <!-- / .tab-pane -->
                        <div class="tab-pane fade " id="demo2-home">
                            <!-- Start of Add Tag Content -->
                            <form role="form" id="registerForm" action="<?php echo base_url(); ?>tags/save_tag.html"
                                  method="post">
                                <h3>
                                    <?php
                                    $this->session->userdata('message');

                                    ?>
                                </h3>
                                <div class="form-group">
                                    <label class="widget-header"><h2><strong>Tag</strong> Name</h2></label>
                                    <input type="text" class="form-control" name="tag_name"
                                           value="<?php echo set_value('tag_name'); ?>">
                                    <div class="label-warning"><?php echo form_error('tag_name'); ?></div>
                                </div>
                                <div class="form-group">
                                    <label class="widget-header"><h2><strong>Publication</strong> Status</h2></label>
                                    <div class="radio iradio">
                                        <label>
                                            <input type="radio" name="publication_status" id="optionsRadios1" value="1"
                                                   checked>&nbsp; Published
                                        </label>
                                    </div>
                                    <hr/>
                                    <div class="radio iradio">
                                        <label>
                                            <input type="radio" name="publication_status" id="optionsRadios2" value="0">&nbsp;
                                            Pending
                                        </label>
                                    </div>
                                    <hr/>
                                </div>
                                <button type="submit" class="btn btn-primary">Add Tag</button>
                                <hr/>
                            </form>

                            <!-- End of Add Tag Content -->
                        </div> <!-- / .tab-pane -->
                        <!-- / .tab-pane -->
                    </div> <!-- / .tab-content -->
                    <hr>

                </div>
            </div>

        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End content here -->
    <!-- ============================================================== -->

    <!--</div>-->
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>-->
<!--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->

    <script>
        $(function () {
            $('#myTable').DataTable({
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })

    </script>

    <script>

        $(document).ready(function () {
            $('#add').click(function () {
                $('#insert').val("Insert");
                $('#insert_form')[0].reset();
            });

            $(document).on('click', '.edit_data', function () {
                var tag_id = $(this).attr("id");
                console.log(tag_id);
                $.ajax({
                    url: "<?php echo base_url()?>tags/fetch_tag",
                    method: "POST",
                    data: {tag_id: tag_id},
                    dataType: "json",
                    success: function (data) {
                        $('#tag_name').val(data.tag_name);
                        $('#publication_status').val(data.publication_status);
                        $('#tag_id').val(data.tag_id);
                        $('#insert').val("Update");
                        $('#add_data_Modal').modal('show');
                    }
                });
            });

            $('#insert_form').on("submit", function (event) {
                event.preventDefault();
                if ($('#tag_name').val() == "") {
                    alert("tag name is required");
                }
                else if ($('#publication_status').val() == '') {
                    alert("publication status is required");
                }
                else {
                    $.ajax({
                        url: "<?php echo base_url()?>tags/update_tag",
                        method: "POST",
                        data: $('#insert_form').serialize(),
                        beforeSend: function () {
                            $('#insert_form').val("Inserting");
                        },
                        success: function (data) {
                            $('#insert_form')[0].reset();
                            $('#add_data_Modal').modal('hide');
                            $('#tag_table').html(data);
                            alert('Tag Name Updated Successfully!');
                            setTimeout("location.href = '<?php echo base_url()?>tags'", 10);
                        }
                    });
                }
            });

        });
    </script>


    <script type="text/javascript">

        function checkDelete() {


            var chk = confirm('Are you sure about to delete this post?');

            if (chk) {

                return true;

            } else {

                return false;

            }

        }


    </script>

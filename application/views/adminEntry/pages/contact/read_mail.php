<section class="content">
    <h3>

        <?php
        $this->session->userdata('message');

        ?>

    </h3>
    <div class="row">
        <div class="col-md-3">
            <a style="color: #0A487C" href="compose.html" class="btn btn-primary btn-block margin-bottom">Compose</a>

            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Folders</h3>

                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active"><a style="color: #0A487C" href="<?php echo base_url()?>contact_mail"><i class="fa fa-inbox"></i> Inbox
                                <span class="label label-danger pull-right"><?php $r = $this->db->where('deletion_status',1)->from('tbl_contact')->count_all_results(); echo $r;?></span></a></li>
                        <li><a style="color: #0A487C" href="<?php echo base_url()?>contact_mail/sent_mail"><i class="fa fa-envelope-o"></i> Sent
                                <span class="label label-primary pull-right"><?php $r = $this->db->from('tbl_contact_reply')->count_all_results(); echo $r;?></span>
                            </a></li>

                        <li><a style="color: #0A487C" href="#"><i class="fa fa-trash-o"></i> Trash</a></li>
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>

            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo $row->sender_name;?></h3>

                    <div class="box-tools pull-right">
                        <a style="color: #0A487C" href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Previous"><i
                                    class="fa fa-chevron-left"></i></a>
                        <a style="color: #0A487C" href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Next"><i
                                    class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="mailbox-read-info">
                        <h3><?php echo $row->sender_subject;?></h3>
                        <h5>From: <?php echo $row->sender_email;?>
                            <span class="mailbox-read-time pull-right"><?php $date = $row->contact_date; echo date("F j, Y, g:i a", strtotime($date)); ?></span></h5>
                    </div>
                    <!-- /.mailbox-read-info -->
                    <div class="mailbox-controls with-border text-center">

                            <a style="color: #0A487C" type="button" class="btn btn-danger btn-sm pull-left" data-toggle="tooltip" href="<?php echo base_url();?>contact_mail/delete_mail/<?php echo $row->contact_id?>" onclick="return checkDelete();"
                                    data-container="body" title="Delete">
                                <i class="fa fa-trash-o"></i></a>
                            <a style="color: #0A487C" type="button" class="btn btn-success btn-sm pull-right" data-toggle="tooltip" href="<?php echo base_url();?>contact_mail/reply_mail/<?php echo $row->contact_id?>"
                                    data-container="body" title="Reply">
                                <i class="fa fa-reply"></i></a>

                    </div>
                    <hr/>
                    <!-- /.mailbox-controls -->
                    <div class="mailbox-read-message" style="text-align: justify">
                        <?php echo nl2br($row->sender_message);?>
                    </div>
                    <!-- /.mailbox-read-message -->
                </div>
                <!-- /.box-body -->
                <!-- /.box-footer -->
                <div class="box-footer">
                    <div class="pull-right">
                        <a style="color: #0A487C" type="button" class="btn btn-success" href="<?php echo base_url();?>contact_mail/reply_mail/<?php echo $row->contact_id?>"><i class="fa fa-reply"></i> Reply</a>
                    </div>
                    <a style="color: #0A487C" type="button" class="btn btn-danger" href="<?php echo base_url();?>contact_mail/delete_mail/<?php echo $row->contact_id?>" onclick="return checkDelete();"><i class="fa fa-trash-o"></i> Delete</a>
                    <a style="color: #0A487C" href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print">
                        Print This Message
                        <i class="entypo-doc-text"></i>
                    </a>
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /. box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

<script type="text/javascript">

    function checkDelete() {


        var chk = confirm('Are you sure about to delete this mail?');

        if (chk) {

            return true;

        } else {

            return false;

        }

    }


</script>
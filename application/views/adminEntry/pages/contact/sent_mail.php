<!--<div class="content-wrapper">-->

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <a style="color: #0A487C" href="compose.html" class="btn btn-primary btn-block margin-bottom">Compose</a>

            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Folders</h3>

                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        <li><a style="color: #0A487C" href="<?php echo base_url()?>contact_mail"><i class="fa fa-inbox"></i> Inbox
                                <span class="label label-primary pull-right"><?php $r = $this->db->where('deletion_status',1)->from('tbl_contact')->count_all_results(); echo $r;?></span></a></li>
                        <li class="active"><a style="color: #0A487C" href="<?php echo base_url()?>contact_mail/sent_mail"><i class="fa fa-envelope-o"></i> Sent
                                <span class="label label-danger pull-right"><?php $r = $this->db->from('tbl_contact_reply')->count_all_results(); echo $r;?></span>
                            </a></li>
                        <li><a style="color: #0A487C" href="#"><i class="fa fa-trash-o"></i> Trash</a></li>
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /. box -->

            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Sent</h3>

                    <div class="box-tools pull-right">
                        <div class="has-feedback">
                            <input type="text" class="form-control input-sm" placeholder="Search Mail">
                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="table-responsive mailbox-messages">
                        <table class="table table-hover table-striped">
                            <tbody>
                            <?php foreach ($sent_mail as $row) { ?>
                                <tr>
                                    <td class="mailbox-star"><a style="color: #0A487C" href="#"><i class="fa fa-star text-yellow"></i></a></td>
                                    <td class="mailbox-name">
                                        <a style="color: #0A487C" href="<?php echo base_url()?>contact_mail/read_sent_mail/<?php echo $row['reply_id'];?>"><b>
                                            <?php $id= $row['admin_id'];
                                            $admin_info = $this->db->get_where('user', array('user_id' => $id))->row();
                                            echo $admin_info->user_name;
                                            ?>

                                        </b></a>
                                    </td>
                                    <td><a style="color: #0A487C" href="<?php echo base_url()?>contact_mail/read_mail/<?php echo $row['contact_id'];?>" target="_blank">To: <?php $sender_id = $row['contact_id']; $info = $this->db->get_where('tbl_contact', array('contact_id'=>$sender_id))->row(); echo $info->sender_email;?></a></td>
                                    <td class="mailbox-subject">
                                        <a style="color: #0A487C" href="<?php echo base_url()?>contact_mail/read_sent_mail/<?php echo $row['reply_id'];?>">

                                            <b><?php echo $row['receiver_subject'];?></b> -
                                            <?php
                                            $str = $row['receiver_reply'];
                                            $length = strlen($str);
                                            if ($length > 50) {
                                                $strCut = substr($str, 0, 50);
                                                $str = substr($strCut, 0, strrpos($strCut, " ")) . "...";
                                            }
                                            echo strip_tags($str);
                                            ?>
                                        </a>
                                    </td>
                                    <td><a style="color: #0A487C" href="<?php echo base_url();?>contact_mail/delete_sent_mail/<?php echo $row['reply_id']?>" onclick="return checkDelete();"><i title="delete selected" class="glyphicon glyphicon-trash pull-right"></i></a></td>
                                    <td><span class="badge"><?php $date = $row['reply_date']; echo date("F j, Y, g:i a", strtotime($date));
                                            ?></span><span>&nbsp;&nbsp;</span></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <!-- /.table -->
                    </div>
                    <!-- /.mail-box-messages -->
                </div>
                <!-- /.box-body -->

            </div>
            <!-- /. box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<!--</div>-->
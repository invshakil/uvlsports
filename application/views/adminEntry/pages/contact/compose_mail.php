<section class="content">
    <div class="row">
        <div class="col-md-3">
            <a style="color: #0A487C" href="compose.html" class="btn btn-primary btn-block margin-bottom">Compose</a>

            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Folders</h3>

                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        <li><a style="color: #0A487C" href="<?php echo base_url() ?>contact_mail"><i class="fa fa-inbox"></i> Inbox
                                <span class="label label-primary pull-right"><?php $r = $this->db->where('deletion_status', 1)->from('tbl_contact')->count_all_results();
                                    echo $r; ?></span></a></li>
                        <li class="active"><a style="color: #0A487C" href="<?php echo base_url()?>contact_mail/sent_mail"><i class="fa fa-envelope-o"></i> Sent
                                <span class="label label-danger pull-right"><?php $r = $this->db->from('tbl_contact_reply')->count_all_results(); echo $r;?></span>
                            </a></li>
                        <li><a style="color: #0A487C" href="#"><i class="fa fa-trash-o"></i> Trash</a></li>
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /. box -->

            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h1 class="box-title">Compose Message for <b><?php echo strtoupper($row->sender_name); ?></b></h1>
                </div>
                <!-- /.box-header -->
                <form role="form" method="post" action="<?php echo base_url()?>contact_mail/send_mail_to_the_user">


                    <div class="box-body">
                        <div class="form-group">
                            <input class="form-control" name="sender_email" placeholder="To:" value="<?php echo $row->sender_email; ?>">
                            <input type="hidden" class="form-control" name="contact_id"
                                   value="<?php echo $row->contact_id; ?>">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" id="subject" name="receiver_subject" placeholder="Subject:">
                        </div>
                        <div class="form-group">
                            <textarea id="ckeditor" name="receiver_reply" class="form-control" style="height: 300px"></textarea>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
                        </div>
                        <input type="reset" onclick="ClearFields();" value="Discard" class="btn btn-info fa fa-times">
                    </div>

                </form>
                <!-- /.box-footer -->
            </div>
            <!-- /. box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

<script>
    function ClearFields() {

        document.getElementById("subject").value = "";
        $('textarea').val('');
    }
</script>
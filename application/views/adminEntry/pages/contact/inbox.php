<div class="container-fluid">
    <div class="row">
        <!--left-->
        <aside class="col-sm-3 col-md-2">
            <a style="color: #0A487C" href="#" class="btn btn-danger btn-sm btn-block" role="button"><i class="glyphicon glyphicon-edit"></i> Compose</a>
            <hr>
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a style="color: #0A487C" href="<?php echo base_url()?>contact_mail"><span class="badge pull-right">32</span> Inbox </a></li>
                <li><a style="color: #0A487C" href="<?php echo base_url()?>contact_mail/sent_mail">Sent</a></li>
            </ul>
            <hr>

        </aside>
        <!--main-->
        <div class="col-sm-9 col-md-10" style="padding-top: 100px;">

            <div class="tab-content">
                <div class="tab-pane fade in active" id="inbox">
                    <table class="table table-striped table-hover">
                        <tbody>

                        <!-- inbox item -->
                        <tr>
                            <td>
                                <label>
                                    <input type="checkbox">
                                </label> <span class="name">Mark Otto</span></td>
                            <td><span class="subject">Nice work on the docs for lastest version</span> <small class="text-muted">- Joe, I just reviewed the last...</small></td>
                            <td><span class="badge">12:10 AM</span><span>&nbsp;&nbsp;</span></td>
                            <td><i class="fa fa-mail-reply"></i></td>
                            <td><i title="delete selected" class="glyphicon glyphicon-trash pull-right"></i></td>
                        </tr>

                        <!-- inbox item -->
                        <tr>
                            <td>
                                <label>
                                    <input type="checkbox">
                                </label> <span class="name">Mark Otto</span></td>
                            <td><span class="subject">Nice work on the docs for lastest version</span> <small class="text-muted">- Joe, I just reviewed the last...</small></td>
                            <td><span class="badge">12:10 AM</span><span>&nbsp;&nbsp;</span></td>
                            <td><i class="fa fa-mail-reply"></i></td>
                            <td><i title="delete selected" class="glyphicon glyphicon-trash pull-right"></i></td>
                        </tr>

                        <!-- inbox item -->
                        <tr>
                            <td>
                                <label>
                                    <input type="checkbox">
                                </label> <span class="name">Mark Otto</span></td>
                            <td><span class="subject">Nice work on the docs for lastest version</span> <small class="text-muted">- Joe, I just reviewed the last...</small></td>
                            <td><span class="badge">12:10 AM</span><span>&nbsp;&nbsp;</span></td>
                            <td><i class="fa fa-mail-reply"></i></td>
                            <td><i title="delete selected" class="glyphicon glyphicon-trash pull-right"></i></td>
                        </tr>

                        <!-- inbox item -->
                        <tr>
                            <td>
                                <label>
                                    <input type="checkbox">
                                </label> <span class="name">Mark Otto</span></td>
                            <td><span class="subject">Nice work on the docs for lastest version</span> <small class="text-muted">- Joe, I just reviewed the last...</small></td>
                            <td><span class="badge">12:10 AM</span><span>&nbsp;&nbsp;</span></td>
                            <td><i class="fa fa-mail-reply"></i></td>
                            <td><i title="delete selected" class="glyphicon glyphicon-trash pull-right"></i></td>
                        </tr>

                        <!-- inbox item -->
                        <tr>
                            <td>
                                <label>
                                    <input type="checkbox">
                                </label> <span class="name">Mark Otto</span></td>
                            <td><span class="subject">Nice work on the docs for lastest version</span> <small class="text-muted">- Joe, I just reviewed the last...</small></td>
                            <td><span class="badge">12:10 AM</span><span>&nbsp;&nbsp;</span></td>
                            <td><i class="fa fa-mail-reply"></i></td>
                            <td><i title="delete selected" class="glyphicon glyphicon-trash pull-right"></i></td>
                        </tr>

                        <!-- inbox item -->
                        <tr>
                            <td>
                                <label>
                                    <input type="checkbox">
                                </label> <span class="name">Mark Otto</span></td>
                            <td><span class="subject">Nice work on the docs for lastest version</span> <small class="text-muted">- Joe, I just reviewed the last...</small></td>
                            <td><span class="badge">12:10 AM</span><span>&nbsp;&nbsp;</span></td>
                            <td><i class="fa fa-mail-reply"></i></td>
                            <td><i title="delete selected" class="glyphicon glyphicon-trash pull-right"></i></td>
                        </tr>

                        </tbody>
                    </table>
                </div>

            </div>
            <div class="row-md-12">

                <div class="well text-right">
                    <small>Last updated: 4/14/2015: 3:02 PM</small>
                </div>

            </div>
        </div>
    </div>
</div>
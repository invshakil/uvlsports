<!--<div class="content-wrapper">-->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                <a style="color: #0A487C" href="compose.html" class="btn btn-primary btn-block margin-bottom">Compose</a>

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Folders</h3>

                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <ul class="nav nav-pills nav-stacked">
                            <li class="active"><a style="color: #0A487C" href="<?php echo base_url()?>contact_mail"><i class="fa fa-inbox"></i> Inbox
                                    <span class="label label-danger pull-right"><?php $r = $this->db->where('deletion_status',1)->from('tbl_contact')->count_all_results(); echo $r;?></span></a></li>
                            <li><a style="color: #0A487C" href="<?php echo base_url()?>contact_mail/sent_mail"><i class="fa fa-envelope-o"></i> Sent
                                    <span class="label label-primary pull-right"><?php $r = $this->db->from('tbl_contact_reply')->count_all_results(); echo $r;?></span>
                                </a></li>
                            <li><a style="color: #0A487C" href="#"><i class="fa fa-trash-o"></i> Trash</a></li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /. box -->

                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Inbox</h3>

                        <h3>

                            <?php
                            $this->session->userdata('message');

                            ?>

                        </h3>

                        <div class="box-tools pull-right">
                            <div class="has-feedback">
                                <input type="text" class="form-control input-sm" placeholder="Search Mail">
                                <span class="glyphicon glyphicon-search form-control-feedback"></span>
                            </div>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="table-responsive mailbox-messages">
                            <table class="table table-hover table-striped">
                                <tbody>
                                <?php foreach ($received_email as $row) { ?>
                                <tr>
                                    <td class="mailbox-star"><a style="color: #0A487C" href="#"><i class="fa fa-star text-yellow"></i></a></td>
                                    <td class="mailbox-name">
                                        <a style="color: #0A487C" href="<?php echo base_url()?>contact_mail/read_mail/<?php echo $row['contact_id'];?>"><?php echo $row['sender_name'];?>
                                            <span class="label label-danger pull-right"><?php if ($row['read_status'] == 0) echo '(NEW)'?></span>
                                        </a>
                                    </td>
                                    <td class="mailbox-subject">
                                        <a style="color: #0A487C" href="<?php echo base_url()?>contact_mail/read_mail/<?php echo $row['contact_id'];?>">

                                        <b><?php echo $row['sender_subject'];?></b> -
                                        <?php
                                        $str = $row['sender_message'];
                                        $length = strlen($str);
                                        if ($length > 50) {
                                            $strCut = substr($str, 0, 50);
                                            $str = substr($strCut, 0, strrpos($strCut, " ")) . "...";
                                        }
                                        echo $str;
                                        ?>
                                        </a>
                                    </td>
                                    <td><a style="color: #0A487C" href="<?php echo base_url();?>contact_mail/reply_mail/<?php echo $row['contact_id']?>"><i class="fa fa-mail-reply"></i></a></td>
                                    <td><a style="color: #0A487C" href="<?php echo base_url();?>contact_mail/delete_mail/<?php echo $row['contact_id']?>" onclick="return checkDelete();"><i title="delete selected" class="glyphicon glyphicon-trash pull-right"></i></a></td>
                                    <td><span class="badge"><?php $date = $row['contact_date']; echo date("F j, Y, g:i a", strtotime($date));
                                            ?></span><span>&nbsp;&nbsp;</span></td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.mail-box-messages -->
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
<!--</div>-->

<script type="text/javascript">

    function checkDelete() {


        var chk = confirm('Are you sure about to delete this mail?');

        if (chk) {

            return true;

        } else {

            return false;

        }

    }


</script>
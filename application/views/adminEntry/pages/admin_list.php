
<!--<div class="content-page">-->
<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
    <!-- Page Heading Start -->
    <div class="page-heading">
        <h1><i class='fa fa-bars'></i> &nbsp&nbsp&nbsp Manage Admin Panel</h1>
    </div>
    <!-- Page Heading End-->				
    <div class="row">

        <h2>

            <?php
             $this->session->userdata('message');

            
            ?>

        </h2>

        <div class="col-md-12">
            <div class="widget">
                <div class="widget-content">
                    <br>					
                    <div class="table-responsive">
<!--                        <table class='form-horizontal' role='form'>-->
                            <table id="datatables-4" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead align="center" >
                                    <tr>
                                        <!--<th style="text-align: center; width: 20px">ID</th>-->
                                        <th style="text-align: center;">Name</th>
                                        <th style="text-align: center;">Email</th>
                                        <th style="text-align: center;">Image</th>
                                        <th style="text-align: center;">Access Level</th>
                                        <th style="text-align: center;">Reg. Time</th>
                                        <!--<th style="text-align: center;">Action</th>-->
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    foreach ($all_admin_user as $user) {
                                        ?>
                                        <tr>
                                            <!--<td><?php echo $user->user_id; ?></td>-->
                                            <td align="center"><strong><?php echo $user->user_name; ?></strong></td>
                                            <td align="center"><a style="color: #0A487C" href="mailto:<?php echo $user->email; ?>"><?php echo $user->email; ?></a></td>
                                            <td align="center">
                                                <?php if($user->oauth_provider != NULL) {?>
                                                    <img src="<?php echo $user->user_avatar; ?>" class="img-responsive img-thumbnail"  width="150">
                                                <?php } else {?>
                                                    <img src="<?php echo base_url() . $user->user_avatar; ?>" class="img-responsive img-thumbnail"   width="150">
                                                <?php }?>
                                            </td>
                                            <td align="center">
                                                <?php if ($user->access_level == 1) { ?>
                                                    <span class="label label-success"><?php echo "Super Admin"; ?></span><br/><br/>

                                                    <!--only super admin can demote-->
                                                    <?php if ($this->session->userdata('access_level') == 1) { ?>

                                                        <a style="color: #0A487C" href="<?php echo base_url(); ?>super_admin_panel/demote_sadmin_to_admin/<?php echo $user->user_id ?>"><span class="label label-warning">Demote to Admin</span></a>

                                                    <?php } ?>

                                                <?php } else { ?>
                                                    <span class="label label-info"><?php echo "Admin"; ?></span><br/><br/>

                                                    <!--only super admin can promote-->
                                                    <?php if ($this->session->userdata('access_level') == 1) { ?>
                                                        <a style="color: #0A487C" style="color: #0A487C" href="<?php echo base_url(); ?>super_admin_panel/promote_admin_to_super_admin/<?php echo $user->user_id ?>"><span class="label label-success">Promote to Super Admin</span></a>
                                                    <?php } ?>

                                                    <!--only super admin can demote-->
                                                    <?php if ($this->session->userdata('access_level') == 1) { ?>

                                                        <br/><br/>
                                                        <a style="color: #0A487C" href="<?php echo base_url(); ?>super_admin_panel/demote_admin_to_user/<?php echo $user->user_id ?>"><span class="label label-danger">Demote to User</span></a>
                                                        <?php
                                                    }
                                                }
                                                ?>

                                            </td>

                                            <td align="center"><div class="alert alert-warning"><strong><?php
                                                        $date = $user->reg_time;
                                                        echo date("F d, Y", strtotime($date));
                                                        ?></strong></div></td>

                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
<!--                        </table>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>

</div>

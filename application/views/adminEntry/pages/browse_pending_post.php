

<div class="content">

    <!-- Page Heading Start -->

    <div class="page-heading">

        <h1><i class='fa fa-bars'></i> &nbsp&nbsp&nbsp Manage Pending Post</h1>

        <?php if ($this->session->userdata('message')) { ?>
            <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                <?php
                 $this->session->userdata('message');

                ?>

            </h3>
        <?php } ?>
    </div>


    <!-- Page Heading End-->
    <div class="row">


        <div class="col-md-12">

            <div class="widget">

                <div class="pull-left">
                    <a class="btn btn-default" href="<?php echo base_url();?>post_controller/manage_post">Browse All Post</a>
                </div>

                <div class="row" style="padding-right: 15px;">

                    <div class="input-group col-md-4 col-md-offset-8">
                        <form role="form" method="get" >
                            <input type="text" name="name" class="form-control" placeholder="Search by Post Title or Part of post description...">
                        </form>
                        <span class="input-group-btn">
                            <button class="btn btn-danger" type="button">
                                <span class=" glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                    </div>


                </div>

                <div class="widget-content">
                    <br/>
                    <!--<div class="table-responsive">-->

                    <form class='form-horizontal' role='form'>

                        <table class="table table-hover table-striped" cellspacing="0" width="100%">

                            <thead align="center">

                            <tr>

                                <th style="text-align: center; width: 20px">ID</th>

                                <th style="text-align: center;">Post Title</th>

                                <th style="text-align: center;">Category Name</th>

                                <th style="text-align: center;">Author Name</th>

                                <th style="text-align: center;">Post Date</th>

                                <th style="text-align: center;">Post Description</th>

                                <th style="text-align: center;">Post Image</th>

                                <th style="text-align: center;">Publication Status</th>

                                <th style="text-align: center;">Currently Editing By</th>

                                <th style="text-align: center;">Action</th>

                            </tr>

                            </thead>


                            <tbody>

                            <?php
                            foreach ($all_pending_post as $post) {
                            ?>

                            <tr>

                                <td><?php echo $post->post_id; ?></td>

                                <td align="center"><strong><?php echo $post->post_title; ?></strong></td>

                                <td align="center"><?php echo $post->category_name; ?></td>

                                <td align="center"><?php echo $post->user_name; ?></td>

                                <td align="center"><?php $date = $post->post_date;
                                    echo date("F j, Y, g:i a", strtotime($date)); ?></td>

                                <td align="justify" style="width: 300px;"><?php
                                    $str = $post->post_description;

                                    $length = strlen($str);


                                    if ($length > 100) {

                                        $strCut = substr($str, 0, 100);

                                        $str = substr($strCut, 0, strrpos($strCut, ' ')) . '... ';
                                    }

                                    echo $str;
                                    ?>


                                </td>

                                <td align="center"><img
                                            src="<?php echo base_url() . './image_upload/post_image/thumbs/' . $post->post_image; ?>">
                                </td>


                                <td align="center">

                                    <?php if ($post->post_publication_status == 1) { ?>

                                        <label class="btn btn-green-1 btn-lg"><a title="change publication status"
                                                                                 href="<?php echo base_url(); ?>post_controller/unpublish_post/<?php echo $post->post_id ?>"><?php echo "Published"; ?></a></label>


                                    <?php } else {
                                        ?>

                                        <label class="btn btn-yellow-1 btn-lg"><a title="change publication status"
                                                                                  href="<?php echo base_url(); ?>post_controller/publish_post/<?php echo $post->post_id ?>"><?php echo "Pending"; ?></a></label>

                                    <?php } ?>

                                </td>

                                <td align="center" style="width: 200px;">
                                    <?php if ($post->last_modified_by == NULL) {
                                        echo '<label class="alert alert-warning">You Can Edit this file</label>';
                                    } else {
                                        echo '<label class="alert alert-danger">'.$post->last_modified_by.' is editing this post.</label>';
                                    } ?>
                                </td>

                                <td>

                                    <div class="btn-group btn-group-xs" style="width: 100px;">

                                        <?php if ($post->edit_lock_status != 1) { ?>

                                            <div class="pull-left"><a data-toggle="tooltip" title="Edit"
                                                                      href="<?php echo base_url(); ?>post_controller/edit_post/<?php echo $post->post_id ?>"
                                                                      class="btn btn-info"><i class="fa fa-edit"></i></a>
                                            </div>

                                        <?php } ?>

                                        <div class="pull-right"><a data-toggle="tooltip" title="Delete"
                                                                   href="<?php echo base_url(); ?>post_controller/delete_post/<?php echo $post->post_id ?>"
                                                                   onclick="return checkDelete();"
                                                                   class="btn btn-danger"><i
                                                        class="fa fa-trash-o"></i></a></div>

                                    </div>

                                </td>

                                <?php } ?>

                            </tr>

                            </tbody>

                        </table>

                    </form>

                </div>

            </div>

        </div>



    </div>

    <hr>



</div>



<!-- ============================================================== -->

<!-- End content here -->

<!-- ============================================================== -->



<!--</div>-->

<script type="text/javascript">

    function checkDelete() {



        var chk = confirm('Are you sure about to delete this post?');

        if (chk)

        {

            return true;

        } else {

            return false;

        }

    }


</script>
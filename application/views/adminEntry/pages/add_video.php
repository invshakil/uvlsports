
<!--<div class="content-page">-->
<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
    <!-- Page Heading Start -->
    <div class="page-heading">
        <h1><i class='fa fa-bars'></i>  &nbsp&nbsp&nbspAdd New Post</h1>
    </div>
    <!-- Page Heading End-->				<div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="widget">
                <div class="widget-content padding">
                    <div class="tab-content tab-boxed">
                        <div class="tab-pane fade active in" id="demo2-home">
                            <!-- Start of Add Category Content -->
                            <form role="form" id="registerForm" action="<?php echo base_url(); ?>super_admin_panel/save_videos.html" method="post">
                                <h3>
                                    <?php
                                    $msg = $this->session->userdata('message');
                                    if ($msg) {
                                        ?>
                                        <span class="label label-success"><?php echo $msg; ?></span>   
                                        <?php
                                        $this->session->unset_userdata('message');
                                    }
                                    ?>
                                </h3>
                                <div class="form-group">
                                    <label class="widget-header"><h2><strong>Video</strong> Title</h2></label>
                                    <input type="text" class="form-control" name="video_title" value="<?php echo set_value('video_title'); ?>">
                                    <div class="label-warning"><?php echo form_error('video_title'); ?></div>
                                </div>
                                <div class="form-group">
                                    <label class="widget-header"><h2><strong>Youtube Embed</strong> Link</h2></label>
                                    <input type="text" class="form-control" name="video_link" value="<?php echo set_value('video_link'); ?>https://www.youtube.com/embed/paste_vdo_id_here">
                                    <div class="label-warning"><?php echo form_error('video_link'); ?></div>
                                </div>
                                <div class="form-group">
                                    <label class="widget-header"><h2><strong>Youtube Image</strong> Thumbnail Link</h2></label>
                                    <input type="text" class="form-control" name="vdo_thumbnail" value="<?php echo set_value('vdo_thumbnail'); ?>http://img.youtube.com/vi/paste_vdo_id_here/hqdefault.jpg">
                                    <div class="label-warning"><?php echo form_error('vdo_thumbnail'); ?></div>
                                </div>
                                <div class="widget">
                                    <div class="widget-header">
                                        <h2><strong>Video</strong> Short Description</h2>
                                        <div class="additional-btn">
                                            <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                                            <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                                            <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
                                        </div>
                                    </div>
                                    <div class="widget-content">
                                        <textarea id="ckeditor" name="vdo_description" value="<?php echo set_value('vdo_description'); ?>"></textarea>
                                        <div class="label-warning"><?php echo form_error('vdo_description'); ?></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="widget-header"><h2><strong>Featured</strong> Status</h2></label>
                                    <div class="radio iradio">
                                        <label>
                                            <input type="radio" name="featured_status" id="optionsRadios1" value="1" checked>&nbsp; Yes, on home page
                                        </label>
                                    </div>
                                    <hr/> 
                                    <div class="radio iradio">
                                        <label>
                                            <input type="radio" name="featured_status" id="optionsRadios2" value="0">&nbsp; Nope, only in gallery
                                        </label>
                                    </div>
                                    <hr/> 
                                </div>
                                <div class="form-group">
                                    <label class="widget-header"><h2><strong>Publication</strong> Status</h2></label>
                                    <div class="radio iradio">
                                        <label>
                                            <input type="radio" name="vdo_publication_status" id="optionsRadios1" value="1" checked>&nbsp; Published
                                        </label>
                                    </div>
                                    <hr/> 
                                    <div class="radio iradio">
                                        <label>
                                            <input type="radio" name="vdo_publication_status" id="optionsRadios2" value="0">&nbsp; Pending
                                        </label>
                                    </div>
                                    <hr/>
                                    <div class="form-group">
                                        <label class="widget-header"><h2><strong>Url </strong> Link</h2></label>
                                        <input type="text" class="form-control" name="vdo_url" value="<?php echo set_value('vdo_url'); ?>" placeholder="it-is-an-example-link">
                                        <div class="label-warning"><?php echo form_error('vdo_url'); ?></div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Add Video</button>
                                <hr/>
                            </form>

                            <!-- End of Add Category Content -->
                        </div> <!-- / .tab-pane -->

                        <!-- / .tab-pane -->
                    </div> <!-- / .tab-content -->
                    <hr>

                </div>
            </div>

        </div>
    </div>
    </div>
    <!-- ============================================================== -->
    <!-- End content here -->
    <!-- ============================================================== -->
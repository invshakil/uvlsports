<!--<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>-->

<div class="content">

    <!-- Page Heading Start -->

    <div class="page-heading">

        <h1><i class='fa fa-bars'></i> &nbsp&nbsp&nbspAdd New Article</h1>

    </div>

    <!-- Page Heading End-->
    <div class="row">

        <div class="col-sm-12">

            <div class="widget">

                <div class="widget-content padding">

                    <div class="tab-content tab-boxed">

                        <div class="tab-pane fade active in" id="demo2-home">

                            <!-- Start of Add Category Content -->

                            <!--If it is User then he will Submit this menu -->
							<?php if ($this -> session -> userdata('access_level') == 3) { ?>
                            <form role="form" id="registerForm"
                                  action="<?php echo base_url(); ?>user_dashboard/save_post.html" method="post"
                                  enctype="multipart/form-data">

                                <!--Otherwise Admin will Submit this form -->
								<?php } else { ?>
                                <form role="form" id="registerForm"
                                      action="<?php echo base_url(); ?>post_controller/save_post.html" method="post"
                                      enctype="multipart/form-data">
                                    <!--<form role="form" id="registerForm"  method="post"  enctype="multipart/form-data">-->
									<?php } ?>

                                    <h3>
										
										<?php
										
										$this -> session -> userdata('message');
										
										
										?>

                                    </h3>

                                    <div class="row">
                                        <div class="col-sm-12">

                                            <!--
                                            ARTICLE TITLE
                                            -->

                                            <div class="panel panel-primary" data-collapsed="0">

                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <strong>Post</strong> Title
                                                    </div>

                                                    <div class="panel-options">
                                                        <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                                    </div>
                                                </div>

                                                <div class="panel-body">

                                                    <input type="text" class="form-control" name="post_title"
                                                           placeholder="Enter Post Title"
                                                           id="post_title"
                                                           value="<?php echo set_value('post_title'); ?>">

                                                    <div class="label-warning"><?php echo form_error('post_title'); ?></div>

                                                </div>

                                            </div>

                                        </div>


                                    </div>

                                    <!--
                                    COVER IMAGE
                                    -->


                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="panel panel-primary" data-collapsed="0">

                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <strong>Cover Image</strong> & Tips
                                                    </div>

                                                    <div class="panel-options">
                                                        <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                                    </div>
                                                </div>

                                                <div class="panel-body">

                                                    <!--Only Admin Can see this option -->
													<?php if ($this -> session -> userdata('access_level') != 3) { ?>


                                                        <input type="file" name="post_image"
                                                               class="btn btn-info btn-block"
                                                               onchange="readURL(this);"
                                                               title="Select Image">

                                                        <br/>
                                                        <br/>
                                                        <div class="label-warning"><?php echo form_error('post_image'); ?></div>


                                                        <img id="blah" src="http://placehold.it/620x348"
                                                             class="img-responsive img-thumbnail" alt="your image"/>
                                                        <br/><br/><br/>
                                                        <h4 class="alert alert-danger">OR Paste Image Source From Image Gallery</h4>
                                                        <br/>

                                                        <input type="text" name="post_image"
                                                               class="form-control"
                                                               onchange="readURL(this);"
                                                               placeholder="Paste Image Source">
													
													<?php } ?>
             
													<?php if ($this -> session -> userdata('access_level') == 3) { ?>

                                                    <div class="alert alert-warning">
                                                    <span style="color: black; font-weight: bolder">
                                                        <p>Please Avoid these common mistakes:</p><br/>
                                                        <p>1. Don't give space before full stop, comma or any expression sign. Use Space after these signs. ( । , ! ? )</p>
                                                        <p>2. Please avoid english words in title, use less english letter in description</p>
                                                        <p>3. Try to use standard Bangla. e.g. করতেছে -> করছে, খাইতেছে->খাচ্ছে</p>
                                                    </span>
                                                    </div>
                                                    <?php }?>

                                                </div>

                                            </div>


                                        </div>

                                        <!--
                                        ARTICLE CONTENT
                                        -->


                                        <div class="col-sm-9">

                                            <div class="panel panel-primary" data-collapsed="0">

                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <strong>Post</strong> Description
                                                    </div>

                                                    <div class="panel-options">
                                                        <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                                    </div>
                                                </div>

                                                <div class="panel-body">

                                                    <textarea id="summernote" name="post_description"
                                                              id="post_description"
                                                              value="<?php echo set_value('post_description'); ?>"></textarea>

                                                    <div class="label-warning"><?php echo form_error('post_description'); ?></div>

                                                </div>

                                            </div>


                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">

                                            <!--
                                            CATEGORY
                                            -->
                                            <div class="panel panel-primary" data-collapsed="0">

                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <strong>Select</strong> Category
                                                    </div>

                                                    <div class="panel-options">
                                                        <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                                    </div>
                                                </div>

                                                <div class="panel-body">

                                                    <select name="post_category_id" class="form-control selectpicker"
                                                            data-live-search="true">
														
														<?php
														foreach ($all_published_category as $category) {
															?>

                                                            <option value="<?php echo $category -> category_id; ?>"><?php echo $category -> category_name; ?></option>
														
														<?php } ?>

                                                    </select>

                                                    <div class="label-warning"><?php echo form_error('post_category_id'); ?></div>

                                                </div>

                                            </div>


                                            <!--
                                            SEO URL
                                            -->


                                            <div class="panel panel-primary" data-collapsed="0">

                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <strong>Most Important two lines
                                                            (160
                                                            character max) about this post which defines the
                                                            post</strong>
                                                    </div>

                                                    <div class="panel-options">
                                                        <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                                    </div>
                                                </div>

                                                <div class="panel-body">

                                                    <input type="text" class="form-control" name="meta_description"
                                                           placeholder="Write Meta Description"
                                                           value="<?php echo set_value('meta_description'); ?>">

                                                    <div class="label-warning"><?php echo form_error('meta_description'); ?></div>

                                                    <hr/>
                                                    <div class="alert alert-warning">
                                                            <span style="color: red; font-weight: bolder">
                                                                <p>[যেমনঃ ইংলিশ প্রিমিয়ার লিগে আজ চেলসির মুখোমুখি আর্সেনাল। বাংলাদেশ সময় রাত ৯টায় এই ম্যাচটি অনুষ্ঠিত হবে।] - এটি দেখে ইনফরমেটিভ মনে হচ্ছে।</p>
                                                            </span>
                                                    </div>
                                                </div>

                                            </div>


                                        </div>

                                        <div class="col-sm-6">

                                            <!--
                                            PUBLICATION STATUS
                                            -->

                                            <div class="panel panel-primary" data-collapsed="0">

                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <strong>Publication</strong> Status
                                                    </div>

                                                    <div class="panel-options">
                                                        <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                                    </div>
                                                </div>

                                                <div class="panel-body">

                                                    <div class="radio iradio">

                                                        <!--Only Admin Can see this option -->
														<?php if ($this -> session -> userdata('access_level') != 3) { ?>
                                                            <label>

                                                                <input type="radio" name="post_publication_status"
                                                                       id="optionsRadios1" value="1">&nbsp; Published

                                                            </label>
														<?php } ?>
                                                    </div>

                                                    <hr/>

                                                    <div class="radio iradio">

                                                        <label>

                                                            <input type="radio" name="post_publication_status"
                                                                   id="optionsRadios2"
                                                                   value="0" checked="checked">&nbsp; Pending

                                                        </label>

                                                    </div>

                                                    <div class="label-warning"><?php echo form_error('post_publication_status'); ?></div>

                                                </div>

                                            </div>


                                            <!--
                                            AUTHOR NAME
                                            -->

                                            <div class="panel panel-primary" data-collapsed="0">

                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <strong>Author</strong> Name: </h2>
                                                    </div>

                                                    <div class="panel-options">
                                                        <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                                    </div>
                                                </div>

                                                <div class="panel-body" style="text-align: center">

                                                    <p style="font-size: 15px; font-weight: bold;" align="center"
                                                       class="btn btn-green-3"><?php echo $this -> session -> userdata('user_name'); ?>
                                                    </p>

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <!--Only Admin Can see this option -->
									<?php if ($this -> session -> userdata('access_level') != 3) { ?>


                                    <div class="row">

                                        <!--
                                        FEATURED STATUS
                                        -->


                                        <div class="col-sm-6">
                                            <div class="panel panel-primary" data-collapsed="0">

                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <strong>Featured</strong> Status
                                                    </div>

                                                    <div class="panel-options">
                                                        <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                                    </div>
                                                </div>

                                                <div class="panel-body">

                                                    <div class="radio iradio">
                                                        <label>
                                                            <input type="radio" name="featured_status"
                                                                   id="optionsRadios1"
                                                                   value="1" checked>&nbsp; Yes, on Slider
                                                        </label>
                                                    </div>
                                                    <hr/>
                                                    <div class="radio iradio">
                                                        <label>
                                                            <input type="radio" name="featured_status"
                                                                   id="optionsRadios2"
                                                                   value="0">&nbsp; Nope!
                                                        </label>
                                                    </div>
                                                    <hr/>

                                                </div>

                                            </div>


                                        </div>


                                        <div class="col-sm-6">


                                            <!--
                                            TAGS
                                            -->

                                            <div class="panel panel-primary" data-collapsed="0">

                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        Select Tags
                                                    </div>

                                                    <div class="panel-options">
                                                        <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                                    </div>
                                                </div>

                                                <div class="panel-body">

                                                    <input type="text" value="" class="tags form-control"
                                                           name="post_tag_id"/>
                                                    <br/><br/>
                                                    <span class="alert alert-info">Enter Tags Name and press enter or comma to separate</span>


                                                    <div class="items"></div>


                                                </div>

                                            </div>


                                            <!--
                                            SEO URL
                                            -->

                                            <div class="panel panel-primary" data-collapsed="0">

                                                <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <strong>SEO URL (English words
                                                            separated by
                                                            dash)</strong> [e.g this-is-the-title-link]
                                                    </div>

                                                    <div class="panel-options">
                                                        <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                                    </div>
                                                </div>

                                                <div class="panel-body">

                                                    <input type="text" class="form-control" name="en_post_url"
                                                           placeholder="Type SEO URL"
                                                           value="<?php echo set_value('en_post_url'); ?>">

                                                </div>

                                            </div>
											<?php } ?>

                                        </div>


                                        <div class="col-md-4 col-md-offset-4">
                                            <button type="submit" name="submit" class="btn btn-danger btn-icon"
                                                    style="width: 100%; font-weight: bold;font-size: 16px;">Save Article
                                                <i class="fa fa-check-circle"></i>
                                            </button>
                                        </div>


                                </form>


                        </div>


                        <!-- End of Add Category Content -->

                    </div> <!-- / .tab-pane -->


                    <!-- / .tab-pane -->

                </div> <!-- / .tab-content -->

                <hr>


            </div>

        </div>


    </div>

</div>

</div>


<!-- ============================================================== -->

<!-- End content here -->

<!-- ============================================================== -->
<style>
    img {
        max-width: 100%;
    }

    input[type=file] {
        padding: 10px;
        background: #2d2d2d;
    }

    .items span {
        display: inline-block;
        margin: 5px;
        margin-top: 20px;
        padding: 5px;
        border: 1px solid #CCC;
    }

    .bootstrap-tagsinput {
        width: 100% !important;
    }

</style>


<script type="application/javascript">


    showAllTags();

    $("#submitTag").click(function () {
        var tag = $('#create_tag').val();

        var status = 1;
        console.log(tag);
        $.ajax({
            type: 'ajax',
            method: 'post',
            data: {tag_name: tag, publication_status: status},
            url: '<?php echo base_url() ?>tags/save_tag',
            async: false,
            success: function (data) {

                $("#result").html("<p class='alert alert-success' style='color: red'>Successfully Added!</p>");

                showAllTags();

            }
        });
        clearInput();
    });

    $("#AddTag").submit(function () {

        return false;
    });

    function showAllTags() {
//        $('#showTags').empty();
        $.ajax({
            type: 'ajax',
            method: 'get',
            url: '<?php echo base_url() ?>tags/fetchAllTags',
            async: false,
            dataType: 'json',
            success: function (data) {

                $.each(data, function (key, data) {

                    $('#showTags').append('<option value="' + data.tag_id + '">' + data.tag_name + '</option>');
                });
            },

            error: function () {
                alert('Could not get Data from Database');
            }

        });

    }

    function clearInput() {
        $("#AddTag :input").each(function () {
            $(this).val('');
        });
    }

    //refreshing tags

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

</script>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js"></script>
<script>
    $(document).ready(function () {
        $('.tags').tagsinput({
            allowDuplicates: true
        });

        $('.tags').on('itemAdded', function (item, tag) {
            $('.items').html('');
            var tags = $('.tags').tagsinput('items');
            $.each(tags, function (index, tag) {
                $('.items').append('<span>' + tag + '</span>');
            });
        });
    });

    $(document).ready(function () {
        $('.tags').tagsinput({
            allowDuplicates: false
        });

    });
</script>


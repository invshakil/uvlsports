<?php
//if (isset($_POST['submit'])) {
//    echo '<pre>';
//    print_r($_POST);
//    print_r($_FILES);
//    exit();}
?>
<!--<div class="content-page">-->
<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
    <!-- Page Heading Start -->
    <div class="page-heading">
        <h1><i class='fa fa-bars'></i>  &nbsp&nbsp Password Change</h1>
    </div>
    <!-- Page Heading End-->				<div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="widget">
                <div class="widget-content padding">
                    <div class="tab-content tab-boxed">
                        <div class="tab-pane fade active in" id="demo2-home">
                            <div class="form-group">
                                <?php echo $this->session->flashdata('verify_msg'); ?>
                                <h3>

                                    <?php
                                         $this->session->userdata('message');

                                    ?>

                                </h3>
                            </div>
                            <!-- Start of Account Settings -->
                            <form role="form" id="registerForm" action="<?php // echo base_url();    ?>password_change/save_password" enctype="multipart/form-data" method="post">
                                <div class="form-group">
                                    <label>Your Current Password</label>
                                    <input type="password" class="form-control" name="user_password" value="<?php echo set_value('user_password'); ?>">
                                    <div ><?php echo form_error('user_password'); ?></div>
                                </div>

                                <div class="form-group">
                                    <label>Your New Password</label>
                                    <input type="password" class="form-control" name="new_password" value="<?php echo set_value('new_password'); ?>">
                                    <div ><?php echo form_error('new_password'); ?></div>
                                </div>

                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control" name="confirm_password" value="<?php echo set_value('confirm_password'); ?>">
                                    <div ><?php echo form_error('confirm_password'); ?></div>
                                </div>

                                <div class="form-group login-input"><?php echo form_error('message'); ?></div>
                                <button type="submit" name="submit" class="btn btn-primary">Change Password</button>

                                <?php echo $this->session->flashdata('msg'); ?>
                            </form>

                            <!-- End of Add Category Content -->
                        </div> <!-- / .tab-pane -->

                        <!-- / .tab-pane -->
                    </div> <!-- / .tab-content -->
                    <hr>

                </div>
            </div>

        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End content here -->
    <!-- ============================================================== -->

    <!--</div>-->
<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
    <!-- Page Heading Start -->
    <div class="page-heading">
        <h1><i class='fa fa-bars'></i> Add & Manage Categories</h1>
    </div>
    <!-- Page Heading End-->
    <div class="row">
        <div class="col-sm-12">
            <div class="widget">
                <div class="widget-content padding">
                    <ul id="demo2" class="nav nav-tabs">
                        <li class="active">
                            <a href="#demo2-profile" data-toggle="tab">Manage Category <span
                                        class="badge badge-primary"></span></a>
                        </li>
                        <li class="">
                            <a href="#demo2-home" data-toggle="tab">Add Category <span
                                        class="label label-success"></span></a>
                        </li>

                    </ul>

                    <div class="tab-content tab-boxed">

                        <div class="tab-pane fade active in" id="demo2-profile">

                            <!-- Start of Manage Category Content -->

                            <div class="widget">
                                <div class="widget-content">
                                    <br>
                                    <div class="table-responsive">
                                        <form class='form-horizontal' role='form'>
                                            <table id="myTable" class="table table-striped table-bordered"
                                                   cellspacing="0" width="100%">
                                                <thead>
                                                <tr>
                                                    <th style="text-align: center; width: 20px">ID</th>
                                                    <th style="text-align: center;">Category Name</th>
                                                    <th style="text-align: center;">Category Description</th>
                                                    <th style="text-align: center;">Publication Status</th>
                                                    <th style="text-align: center;">Action</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <?php
                                                foreach ($all_category as $category) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $category->category_id; ?></td>
                                                        <td><strong><?php echo $category->category_name; ?></strong>
                                                        </td>
                                                        <td align="justify"><?php echo $category->category_description; ?></td>
                                                        <td align="center">
                                                            <?php if ($category->publication_status == 1) {
                                                                ?>
                                                                <span class="label label-success"><?php echo "Published"; ?></span>

                                                                <?php
                                                            } else {
                                                                ?>
                                                                <span class="label label-danger"><?php echo "Pending"; ?></span>
                                                            <?php }
                                                            ?>
                                                        </td>
                                                        <td align="center" style="width: 120px;">

                                                            <div class="pull-right">

                                                                <a data-toggle="tooltip" title="Off"
                                                                   class="btn btn-danger"><i
                                                                            class="fa fa-trash-o"></i></a>
                                                            </div>

                                                            <div class="pull-left">
                                                                <a data-toggle="tooltip" title="Edit" id="<?php echo $category->category_id ?>"
                                                                   class="md-trigger btn btn-success"
                                                                   data-modal="md-slide-stick-top1"><i
                                                                            class="fa fa-edit"></i></a>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                    </div>


                                    <!-- Modal -->
                                    <!-- Modal Start -->

                                    <!-- Modal Task Progress -->

                                    <!-- Modal Logout -->

                                    <div class="md-modal md-slide-stick-top" id="md-slide-stick-top1">

                                        <div class="md-content">

                                            <h3><strong>Update</strong> Category</h3>

                                            <div>

                                                <form role="form" id="registerForm"
                                                      action="<?php echo base_url(); ?>categories/save_category.html"
                                                      method="post">

                                                    <div class="form-group">
                                                        <label class="widget-header"><h2><strong>Category</strong> Name
                                                            </h2></label>
                                                        <input type="text" class="form-control" name="category_name" id="category_name">
                                                        <input type="hidden" class="form-control" name="category_id" id="category_id">
                                                        <div class="label-warning"><?php echo form_error('category_name'); ?></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="widget-header"><h2><strong>Category</strong> Description
                                                            </h2></label>
                                                        <input type="text" class="form-control" name="category_description" id="category_description">
                                                        <div class="label-warning"><?php echo form_error('category_name'); ?></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="widget-header"><h2><strong>Publication</strong>
                                                                Status</h2></label>
                                                        <div class="radio iradio">
                                                            <label>
                                                                <input type="radio" name="publication_status"
                                                                       id="optionsRadios1" value="1"
                                                                       checked>&nbsp; Published
                                                            </label>
                                                        </div>
                                                        <hr/>
                                                        <div class="radio iradio">
                                                            <label>
                                                                <input type="radio" name="publication_status"
                                                                       id="optionsRadios2" value="0">&nbsp;
                                                                Pending
                                                            </label>
                                                        </div>
                                                        <hr/>
                                                    </div>
                                                    <a type="submit" class="btn btn-success">Update Category</a>
                                                    <a class="btn btn-danger md-close">Close</a>
                                                    <hr/>
                                                </form>

                                            </div>

                                        </div>

                                    </div>        <!-- Modal End -->

                                </div>
                            </div>

                            <!-- End of Manage Category Content -->
                        </div> <!-- / .tab-pane -->

                        <div class="tab-pane fade " id="demo2-home">
                            <!-- Start of Add Category Content -->
                            <form role="form" id="registerForm"
                                  action="<?php echo base_url(); ?>categories/save_category.html" method="post">
                                <h3>
                                    <?php
                                    $this->session->userdata('message');

                                    ?>
                                </h3>
                                <div class="form-group">
                                    <label class="widget-header"><h2><strong>Category</strong> Name</h2></label>
                                    <input type="text" class="form-control" name="category_name" id="category_name">
                                    <div class="label-warning"><?php echo form_error('category_name'); ?></div>
                                </div>
                                <div class="form-group">
                                    <label class="widget-header"><h2><strong>Category</strong> Description</h2></label>
                                    <input type="text" class="form-control" name="category_name" id="category_description">
                                    <div class="label-warning"><?php echo form_error('category_name'); ?></div>
                                </div>
                                <div class="form-group">
                                    <label class="widget-header"><h2><strong>Publication</strong> Status</h2></label>
                                    <div class="radio iradio">
                                        <label>
                                            <input type="radio" name="publication_status" id="optionsRadios1" value="1"
                                                   checked>&nbsp; Published
                                        </label>
                                    </div>
                                    <hr/>
                                    <div class="radio iradio">
                                        <label>
                                            <input type="radio" name="publication_status" id="optionsRadios2" value="0">&nbsp;
                                            Pending
                                        </label>
                                    </div>
                                    <hr/>
                                </div>
                                <button type="submit" class="btn btn-primary">Add Category</button>
                                <hr/>
                            </form>

                            <!-- End of Add Category Content -->
                        </div> <!-- / .tab-pane -->
                        <!-- / .tab-pane -->
                    </div> <!-- / .tab-content -->
                    <hr>

                </div>
            </div>

        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End content here -->
    <!-- ============================================================== -->

    <!--</div>-->
    <script>
        $(function () {
            $('#myTable').DataTable({
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })

    </script>
<script type="text/javascript">
    $(document).on('click', '.md-trigger btn btn-success', function(){
        var category_id = $(this).attr("id");
        $.ajax({
            url:"<?php echo base_url(); ?>categories/fetch_category_info_by_id",
            method:"POST",
            data:{category_id:category_id},
            dataType:"json",
            success:function(data)
            {
                $('#md-slide-stick-top1').modal('show');
                $('#category_id').val(category_id);
                $('#category_name').val(data.category_name);
                $('#publication_status').val(data.publication_status);
            }
        })
    });
</script>
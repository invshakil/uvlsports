<?php
//if (isset($_POST['submit'])) {
//    echo '<pre>';
//    print_r($_POST);
//    print_r($_FILES);
//    exit();}
?>
<!--<div class="content-page">-->
<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
    <!-- Page Heading Start -->
    <div class="page-heading">
        <h1><i class='fa fa-bars'></i>  &nbsp&nbsp&nbspAccount Settings</h1>
    </div>
    <!-- Page Heading End-->				<div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="widget">
                <div class="widget-content padding">
                    <div class="tab-content tab-boxed">
                        <div class="tab-pane fade active in" id="demo2-home">
                            <div class="form-group">
                                <?php $this->session->flashdata('verify_msg'); ?>
                                <h3>

                                    <?php
                                        $this->session->userdata('message');

                                    ?>

                                </h3>
                            </div>
                            <!-- Start of Account Settings -->
                            <form role="form" name="edit_post_info" id="registerForm" action="<?php echo base_url();?>account_settings/save_settings" enctype="multipart/form-data" method="post">
                                <div class="form-group">
                                    <label>Change Your Name</label>
                                    <input type="text" class="form-control" name="user_name" value="<?php echo $user_info->user_name ?>" required="">
                                    <div ><?php echo form_error('user_name'); ?></div>


                                </div>

                                <div class="form-group">
                                    <label>Change Your Email</label>
                                    <input type="text" class="form-control" name="email" value="<?php echo $user_info->email ?>" required="">
                                    <div ><?php echo form_error('email'); ?></div>
                                </div>
                                <p class="label label-danger" style="font-size:13px;">Use your facebook Email, so that you can login with your facebook id.</p><br/><br/>

                                <div class="form-group">
                                    <label>Give Your Biography (max 360 letters)</label>
                                    <input type="text" class="form-control" name="user_bio" value="<?php echo $user_info->user_bio ?>" required="">
                                    <div ><?php echo form_error('user_bio'); ?></div>
                                </div>

                                <div class="form-group">
                                    <label>Your Facebook ID Link(https://www.facebook.com/username)</label><br/>

                                    <input type="text" class="form-control" name="user_fb" value="<?php echo $user_info->user_fb ?>">
                                    <div ><?php echo form_error('user_fb'); ?></div>
                                </div>

                                <p class="label label-warning" style="font-size:13px;">it will make sure that your name is being shown in facebook thumbnail.</p><br/><br/>

                                <div class="form-group">
                                    <label>Your Twitter ID(twitter.com/username)</label>
                                    <input type="text" class="form-control" name="user_tw" value="<?php echo $user_info->user_tw ?>">
                                    <div ><?php echo form_error('user_tw'); ?></div>
                                </div>


                                <div class="form-group login-input"><?php echo form_error('message'); ?></div>
                                <button type="submit" name="submit" class="btn btn-primary">Save Settings</button>

                                <?php echo $this->session->flashdata('msg'); ?>
                            </form>

                            <!-- End of Add Category Content -->
                        </div> <!-- / .tab-pane -->

                        <!-- / .tab-pane -->
                    </div> <!-- / .tab-content -->
                    <hr>

                </div>
            </div>

        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End content here -->
    <!-- ============================================================== -->

    <!--</div>-->

    <script type="text/javascript">
//        document.forms['edit_post_info'].elements['post_category_id'].value = '<?php // echo $post_info->post_category_id ?>';
        document.forms['edit_post_info'].elements['user_name'].value = '<?php echo $user_info->user_name ?>';
        document.forms['edit_post_info'].elements['user_bio'].value = '<?php echo $user_info->user_bio ?>';
        document.forms['edit_post_info'].elements['user_fb'].value = '<?php echo $user_info->user_fb ?>';
        document.forms['edit_post_info'].elements['user_tw'].value = '<?php echo $user_info->user_tw ?>';
    </script>
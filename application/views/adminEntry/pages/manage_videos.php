
<!--<div class="content-page">-->
<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
    <!-- Page Heading Start -->
    <div class="page-heading">
        <h1><i class='fa fa-bars'></i> &nbsp&nbsp&nbsp Manage Videos</h1>
    </div>
    <!-- Page Heading End-->				<div class="row">

        <div class="col-md-12">
            <div class="widget">
                <div class="widget-content">
                    <br>					
                    <div class="table-responsive">
                        <form class='form-horizontal' role='form'>
                            <table id="datatables-4" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead align="center" >
                                    <tr>
                                        <th style="text-align: center; width: 20px">ID</th>
                                        <th style="text-align: center;">Video Title</th>
                                        <th style="text-align: center;">Video Description</th>
                                        <th style="text-align: center;">Thumbnail Image</th>
                                        <th style="text-align: center;">Posted By</th>
                                        <th style="text-align: center;">Featured Status</th>
                                        <th style="text-align: center;">Post Time</th>
                                        <th style="text-align: center;">Publication Status</th>
                                        <th style="text-align: center;">Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    foreach ($all_videos as $videos) {
                                        ?>
                                        <tr>
                                            <td><?php echo $videos->video_id; ?></td>
                                            <td align="center"><strong><a href="<?php echo $videos->video_link; ?>" target="blank"><?php echo $videos->video_title; ?></a></strong></td>
                                            <td align="center"><?php echo $videos->vdo_description; ?></td>
                                            <td align="center"><img src="<?php echo $videos->vdo_thumbnail; ?>" style="height: 100px;width: 100px;"></td>
                                            <td align="center"><?php echo $videos->user_name; ?></td>
                                            <td align="center">
                                                <?php if ($videos->featured_status == 1) { ?>
                                                    <span class="label label-success"><?php echo "FEATURED"; ?></span>
                                                <?php } else {
                                                    ?>
                                                    <span class="label label-warning"><?php echo "ONLY GALLERY"; ?></span>   
                                                <?php } ?>
                                            </td>
                                            <td align="center"><button class="btn btn-default"><?php
                                                    $date = $videos->vdo_date;
                                                    echo date("F d, Y", strtotime($date));
                                                    ?></button></td>
                                            <td align="center">
                                                <?php if ($videos->vdo_publication_status == 1) { ?>
                                                    <span class="label label-success"><?php echo "Published"; ?></span>

                                                <?php } else {
                                                    ?>
                                                    <span class="label label-danger"><?php echo "Pending"; ?></span>   
                                                <?php } ?>
                                            </td>
                                            <td align="center">
                                                <div class="btn-group btn-group-xs">
                                                    <a data-toggle="tooltip" title="Off" class="btn btn-default"><i class="fa fa-power-off"></i></a>
                                                    <a data-toggle="tooltip" title="Edit" class="btn btn-default"><i class="fa fa-edit"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>

</div>

<!-- ============================================================== -->
<!-- End content here -->
<!-- ============================================================== -->

<!--</div>-->

<!--<div class="content-page">-->
<!-- ============================================================== -->
<!-- Start Content here -->
<!-- ============================================================== -->
<div class="content">
    <!-- Page Heading Start -->
    <div class="page-heading">
        <h1><i class='fa fa-bars'></i> &nbsp&nbsp&nbsp Manage User</h1>
    </div>
    <!-- Page Heading End-->				
    <div class="row">

        <h2>

            <?php
                $this->session->userdata('message');

            ?>

        </h2>


        <div class="col-md-12">
            <div class="widget">
                <div class="row" style="padding-right: 15px;">

                    <div class="input-group col-md-4 col-md-offset-8">
                        <form role="form" method="get" >
                            <input type="text" name="name" class="form-control" placeholder="Search by name or email id...">
                        </form>
                        <span class="input-group-btn">
                            <button class="btn btn-danger" type="button">
                                <span class=" glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                    </div>


                </div>
                <br>
                
                <div class="widget-content">

                    <div class="table-responsive">



                        <!--<form class='form-horizontal' role='form'>-->
                        <table data-sortable class="table table-hover table-striped" cellspacing="0" width="100%">
                            <thead align="center" >
                                <tr>
                                    <!--<th style="text-align: center; width: 20px">ID</th>-->
                                    <th style="text-align: center;">Name</th>
                                    <th style="text-align: center;">Email</th>
                                    <th style="text-align: center;">FB ID</th>
                                    <th style="text-align: center;">Image</th>
                                    <th style="text-align: center;">Access Level</th>
                                    <th style="text-align: center;">Reg. Time</th>
                                    <th style="text-align: center;">Activation Status</th>
                                    <th style="text-align: center;">Action</th>
                                </tr>
                            </thead>

                            <tbody>

                                <?php
                                if (is_array($all_user) && count($all_user) >= 1) {
                                    foreach ($all_user as $user) {
                                        ?>
                                        <tr>
                                            <!--<td><?php echo $user->user_id; ?></td>-->
                                            <td align="center"><strong><?php echo $user->user_name; ?></strong></td>
                                            <td align="center"><a style="color: #0A487C" href="mailto:<?php echo $user->email; ?>"><?php echo $user->email; ?></a></td>
                                            <td align="center"><a style="color: #0A487C" href="<?php echo $user->user_fb; ?>" target="_blank"><?php echo $user->user_fb; ?></a>
                                            <br/>
                                                <a class="btn btn-danger" href="#" data-id="<?php echo $user->user_id?>"
                                                   data-fb="<?php echo $user->user_fb; ?>"
                                                   data-toggle="modal" data-target="#updateModal">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                    Edit
                                                </a>
                                            </td>
                                            <td align="center">
                                                <?php if($user->oauth_provider != NULL) {?>
                                                    <img src="<?php echo $user->user_avatar; ?>" class="img-responsive img-thumbnail"  width="150">
                                                <?php } else {?>
                                                    <img src="<?php echo base_url() . $user->user_avatar; ?>" class="img-responsive img-thumbnail"   width="150">
                                                <?php }?>
                                            </td>
                                            <td align="center">
                                                <span class="label label-danger"><?php echo "USER"; ?></span> <br/><br/>

                                                <!--only super admin can promote-->
                                                <?php if ($this->session->userdata('access_level') == 1) { ?>
                                                    <a style="color: #0A487C" href="<?php echo base_url(); ?>super_admin_panel/promote_user/<?php echo $user->user_id ?>"><span class="label label-info">Promote to Admin</span></a>
                                                <?php } ?>
                                            </td>
                                            <td align="center"><div class="alert alert-warning"><strong><?php
                                                        $date = $user->reg_time;
                                                        echo date("F d, Y", strtotime($date));
                                                        ?></strong></div></td>
                                            <td align="center">
                                                <?php if ($user->approval_status == 1) { ?>
                                                    <span class="label label-success"><?php echo "Verified"; ?></span> <br/><br/>

                                                    <a style="color: #0A487C" href="<?php echo base_url(); ?>super_admin_panel/ban_user/<?php echo $user->user_id ?>"><span class="label label-warning">Ban this User</span></a>

                                                <?php } else if ($user->approval_status == 0) {
                                                    ?>
                                                    <a style="color: #0A487C" href="<?php echo base_url(); ?>super_admin_panel/verify_user/<?php echo $user->user_id ?>"><span class="label label-warning"><?php echo "Not Verified"; ?></span></a>
                                                <?php } else { ?>
                                                    <span class="label label-danger"><?php echo "Banned"; ?></span>
                                                <?php } ?>
                                            </td>
                                            <td align="center">
                                                <div class="btn-group btn-group-xs">
                                                    <a style="color: #0A487C" data-toggle="tooltip" title="Delete This User" href="<?php echo base_url(); ?>super_admin_panel/delete_user/<?php echo $user->user_id ?>" onclick="return checkDelete();" class="btn btn-default"><i class="fa fa-remove"></i></a>
                                                </div>





                                                <!-- MODAL -->

                                                <div id="updateModal" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;
                                                                </button>
                                                                <h4 class="modal-title">Update User FB</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form role="form" method="post" action="<?php echo base_url()?>account_settings/update_fb">

                                                                    <div class="form-group">
                                                                        <label class="widget-header">
                                                                            <strong>User</strong> FB ID
                                                                        </label>

                                                                        <input type="text" class="form-control" name="user_fb" id="fb">
                                                                        <input type="hidden" class="form-control" name="user_id" id="id">
                                                                    </div>

                                                                    <input type="submit" value="Update"
                                                                           class="btn btn-success"/>
                                                                    <hr/>
                                                                </form>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                                    Close
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>

                                        </tr>


                                        <?php
                                    }
                                } else {
                                    echo "<h1>&nbsp&nbsp&nbsp&nbspThere is no user to show!</h1>";
                                }
                                ?>

                            </tbody>
                        </table>



                        <!--</form>-->
                    </div>


                    <div class="data-table-toolbar">
                        <!--<ul class="pagination">-->
                        <?php
                        foreach ($links as $link) {
                            echo $link;
                        }
                        ?>
                        <!--</ul>-->
                    </div>

                </div>
            </div>
        </div>
    </div>
    <hr>

</div>


<!-- ============================================================== -->
<!-- End content here -->
<!-- ============================================================== -->

<!--</div>-->

<script type="text/javascript">

    function checkDelete() {



        var chk = confirm('Are You Sure about to Delete This User?');

        if (chk)

        {

            return true;

        } else {

            return false;

        }

    }

    $('#updateModal').on('show.bs.modal', function (e) {
        var id = $(e.relatedTarget).data('id'); //<- It's data-id value
        var fb = $(e.relatedTarget).data('fb'); //<- It's data-name value

        $('#id').val(id);
        $('#fb').val(fb);
    });



</script>

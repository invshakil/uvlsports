<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of super_admin_model
 *
 * @author shaki
 */
class Super_admin_model extends CI_Model
{
	
	public function select_all_admin_user()
	{
		$this -> db -> select('*');
		$this -> db -> from('user');
		$where = "access_level !=3";
		$this -> db -> where($where);
		$query_result = $this -> db -> get();
		$result = $query_result -> result();
		return $result;
	}
	
	public function count_user()
	{
		$result = $this -> db -> count_all_results("user");
		return $result;
	}
	
	public function select_all_user($limit, $start)
	{
		$this -> db -> select('*');
		$this -> db -> from('user');
		$this -> db -> where('access_level', 3);
		$this -> db -> limit($limit, $start);
		$query = $this -> db -> get();
		if ($query -> num_rows() > 0) {
			$result = $query -> result();
			return $result;
		}
		return false;
	}
	
	public function promote_user($user_id)
	{
		$this -> db -> set('access_level', 2);
		$this -> db -> where('user_id', $user_id);
		$this -> db -> update('user');
	}
	
	public function promote_admin_to_super_admin($user_id)
	{
		$this -> db -> set('access_level', 1);
		$this -> db -> where('user_id', $user_id);
		$this -> db -> update('user');
	}
	
	public function demote_sadmin_to_admin($user_id)
	{
		$this -> db -> set('access_level', 2);
		$this -> db -> where('user_id', $user_id);
		$this -> db -> update('user');
	}
	
	public function demote_admin_to_user($user_id)
	{
		$this -> db -> set('access_level', 3);
		$this -> db -> where('user_id', $user_id);
		$this -> db -> update('user');
	}
	
	public function ban_user($user_id)
	{
		$this -> db -> set('approval_status', 2);
		$this -> db -> where('user_id', $user_id);
		$this -> db -> update('user');
	}
	
	public function verify_user($user_id)
	{
		$this -> db -> set('approval_status', 1);
		$this -> db -> where('user_id', $user_id);
		$this -> db -> update('user');
	}
	
	public function delete_user($user_id)
	{
		$this -> db -> where('user_id', $user_id);
		$this -> db -> delete('user');
	}
	
	public function searched_user($data)
	{
		$this -> db -> select("*");
		$this -> db -> from('user');
		$this -> db -> group_start();
		$this -> db -> like('user_name', $data);
		$this -> db -> or_like('email', $data);
		$this -> db -> group_end();
		$this -> db -> where('access_level', 3);
//        $this->db->limit(10);
		$this -> db -> order_by('user_id', 'desc');
		$query = $this -> db -> get();
		return $query -> result();
	}
	
	/*
	 * Category Management
	 */
	
	public function save_category_info($data)
	{
		$this -> db -> insert('tbl_category', $data);
	}
	
	public function select_all_category()
	{
		$this -> db -> select('*');
		$this -> db -> from('tbl_category');
		$this -> db -> where('deletion_status', 1);
		$this -> db -> order_by('category_id', 'desc');
		$query_result = $this -> db -> get();
		$result = $query_result -> result();
		return $result;
	}
	
	public function fetch_category_info_by_id($category_id)
	{
		$this -> db -> select('*');
		$this -> db -> from('tbl_category');
		$this -> db -> where('category_id', $category_id);
		$query_result = $this -> db -> get();
		$result = $query_result -> row();
		
		return $result;
	}
	
	/*
	 * Tag Management
	 */
	
	public function save_tag_info($data)
	{
		$this -> db -> insert('tbl_tags', $data);
		return true;
	}
	
	public function select_all_tag()
	{
		$this -> db -> select('*');
		$this -> db -> from('tbl_tags');
		$query_result = $this -> db -> get();
		$result = $query_result -> result();
		return $result;
	}
	
	public function delete_tag($tag_id)
	{
		
		$this -> db -> where('tag_id', $tag_id);
		$this -> db -> delete('tbl_tags');
		return 1;
	}
	
	/*
	 * POST DB CONTROL -> Add Post with category id, tag id, user id
	 */
	
	//published category
	public function select_all_published_category()
	{
		$this -> db -> select('*');
		$this -> db -> from('tbl_category');
		$this -> db -> where('publication_status', 1);
		$query_result = $this -> db -> get();
		$result = $query_result -> result();
		
		return $result;
	}
	
	//published tag
	public function select_all_published_tag()
	{
		$this -> db -> select('*');
		$this -> db -> from('tbl_tags');
		$this -> db -> where('publication_status', 1);
		$query_result = $this -> db -> get();
		$result = $query_result -> result();
		
		return $result;
	}
	
	public function select_all_published_tag_1()
	{
		$this -> db -> select('tag_id,tag_name');
		$this -> db -> from('tbl_tags');
		$query_result = $this -> db -> get();
		$result = $query_result -> result();
		
		return $result;
	}
	
	//inserting post into db
	
	public function save_post_info($data)
	{
		$this -> db -> insert('tbl_post', $data);
		return $insert_id = $this -> db -> insert_id();
	}
	
	//multi tag insertion into db
	
	public function multi_tag_info($tag_data)
	{
		$this -> db -> insert('post_multi_tag', $tag_data);
	}
	
	//retriving all post from db
	
	public function select_all_post($limit, $start)
	{
		$this -> db -> select('*');
		$this -> db -> from('tbl_post');
		$this -> db -> join('user', 'user_id = post_author_id');
		$this -> db -> join('tbl_category', 'category_id = post_category_id');
		$this -> db -> where('tbl_post.deletion_status', 1);
		$this -> db -> limit($limit, $start);
		$this -> db -> order_by('post_date', 'desc');
//        $this->db->order_by('post_time', 'desc');
		$query = $this -> db -> get();
		$result = $query -> result();
		
		return $result;
	}
	
	public function select_all_image($limit, $start)
	{
		$this -> db -> select('post_image, post_title, post_id');
		$this -> db -> from('tbl_post');
		$this -> db -> where('tbl_post.post_publication_status', 1);
		$this -> db -> limit($limit, $start);
		$this -> db -> order_by('post_date', 'desc');
		$query = $this -> db -> get();
		$result = $query -> result();
		
		return $result;
	}
	
	public function edited_by_info($limit, $start)
	{
		$this -> db -> select('*');
		$this -> db -> from('tbl_post');
		$this -> db -> join('user', 'user_id = last_modified_by');
		$this -> db -> where('tbl_post.deletion_status', 1);
		$this -> db -> limit($limit, $start);
		$this -> db -> order_by('post_date', 'desc');
//        $this->db->order_by('post_time', 'desc');
		$query = $this -> db -> get();
		$result = $query -> result();
		
		return $result;
	}
	
	public function select_all_pending_post()
	{
		$this -> db -> select('*');
		$this -> db -> from('tbl_post');
		$this -> db -> join('user', 'user_id = post_author_id');
		$this -> db -> join('tbl_category', 'category_id = post_category_id');
		$where = "tbl_post.deletion_status='1' AND tbl_post.post_publication_status != '1'";
		$this -> db -> where($where);
		$this -> db -> order_by('post_date', 'desc');
		$query = $this -> db -> get();
		$result = $query -> result();
		
		return $result;
	}
	
	public function count_post()
	{
		$result = $this -> db -> count_all_results("tbl_post");
		return $result;
	}
	
	public function searched_post($data)
	{
		$this -> db -> select("*");
		$this -> db -> from('tbl_post');
		$this -> db -> join('user', 'user_id = post_author_id');
		$this -> db -> join('tbl_category', 'category_id = post_category_id');
		$this -> db -> group_start();
		$this -> db -> like('post_title', $data);
		$this -> db -> or_like('post_description', $data);
		$this -> db -> group_end();
//        $this->db->where('access_level', 3);
//        $this->db->limit(10);
		$this -> db -> order_by('post_id', 'desc');
		$query = $this -> db -> get();
		return $query -> result();
	}
	
	public function select_post_multi_tag()
	{
		$this -> db -> select('*');
		$this -> db -> from('post_multi_tag');
		$this -> db -> join('tbl_tags', 'tag_id = tag_id');
		$query = $this -> db -> get();
		
		return $query -> result();
	}
	
	//Published & Unpublish your post
	
	public function published_product_info($post_id)
	{
		$this -> db -> set('post_publication_status', 1);
		$this -> db -> where('post_id', $post_id);
		$this -> db -> update('tbl_post');
	}
	
	public function unpublished_product_info($post_id)
	{
		$this -> db -> set('post_publication_status', 0);
		$this -> db -> where('post_id', $post_id);
		$this -> db -> update('tbl_post');
	}
	
	// Edit Post
	
	public function select_post_info_by_id($post_id)
	{
		$this -> db -> select('*');
		$this -> db -> from('tbl_post');
		$this -> db -> join('user u', 'u.user_id = tbl_post.post_author_id');
		$this -> db -> join('tbl_category c', 'c.category_id = tbl_post.post_category_id');
		$this -> db -> where('post_id', $post_id);
		$query_result = $this -> db -> get();
		$result = $query_result -> row();
		
		return $result;
	}
	
	public function update_post_info($data, $post_id)
	{
		$this -> db -> where('post_id', $post_id);
		$this -> db -> update('tbl_post', $data);
	}
	
	//Delete Post
	
	public function delete_post_info_by_id($post_id)
	{
		$this -> db -> where('post_id', $post_id);
//        $this->db->unlink('post_id', $post_id);
		$this -> db -> delete('tbl_post');
	}
	
	
	/*
	 * !! POST DB CONTROL -> Add Post with category id, tag id, user id
	 */
	
	public function save_last_modified_admin($admin_name, $post_id)
	{
		$this -> db -> set('last_modified_by', $admin_name);
		$this -> db -> set('edit_lock_status', 1);
		$this -> db -> where('post_id', $post_id);
		$this -> db -> update('tbl_post');
	}
	
	public function editor_info($post_id, $user_id)
	{
		$this -> db -> select('*');
		$this -> db -> from('tbl_post');
		$this -> db -> join('user', 'user_id = last_modified_by');
		$where = "tbl_post.post_id='$post_id' AND tbl_post.last_modified_by = '$user_id'";
		$this -> db -> where($where);
		$query_result = $this -> db -> get();
		$result = $query_result -> row();
		
		return $result -> user_name;
	}
	
	public function get_game_week_matches()
	{
		$this -> db -> select('*');
		$this -> db -> from('tbl_match_schedule m');
		$this -> db -> join('tbl_gwtitle g', 'g.gw_id = m.gw_id');
		$query_result = $this -> db -> get();
		$result = $query_result -> result();
		
		return $result;
	}
	
	public function post_update_match_info($data, $match_id)
	{
		$this -> db -> where('match_id', $match_id);
		$this -> db -> update('tbl_match_schedule', $data);
		return true;
	}
	
	public function delete_match($id)
	{
		
		$this -> db -> where('match_id', $id);
		$this -> db -> delete('tbl_match_schedule');
		return 1;
	}
}

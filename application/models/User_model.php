<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user_model
 *
 * @author shaki
 */
class User_model extends CI_Model {

    //insert into user table
    function insertUser($data) {

        if ($this->db->insert('user', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function sendEmail($to_email) {
        $from_email = 'uvlsports@gmail.com'; //change this to yours
        $subject = 'Verify Your Email Address';
        $message = 'Dear User,<br /><br />Please click on the below activation link to verify your email address.<br /><br /> <a href="' . base_url() . '/user_registration/verify/' . ($to_email) . '/' . md5($to_email) . '">Click Here to Verify</a><br /><br /><br />Thanks<br />Universal Sports';

        //configure email settings
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com'; //smtp host name
        $config['smtp_port'] = '465'; //smtp port number
        $config['smtp_user'] = $from_email;
        $config['smtp_pass'] = 'ffbdbcf$143'; //$from_email password
        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n"; //use double quotes
        $this->email->initialize($config);

        //send mail
        $this->email->from($from_email, 'Universal Sports');
        $this->email->to($to_email);
        $this->email->subject($subject);
        $this->email->message($message);
        return $this->email->send();
    }

    //activate user account
    function verifyEmailID($to_email) {

        $data = array('approval_status' => 1);
        $this->db->where('email', $to_email);
        return $this->db->update('user', $data);
    }
    
    function reset_password_email($to_email,$code) {
        $from_email = 'uvlsports@gmail.com'; //change this to yours
        $subject = 'Reset your password';
        $message = 'Dear User,<br /><br />Please click on the below activation link to reset your password.<br /><br /> <a href="' . base_url() . '/reset_password/confirm_password/' . ($to_email) . '/' . ($code) . '">Click Here to Verify</a><br /><br /><br />Thanks<br />Universal Sports';

        //configure email settings
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com'; //smtp host name
        $config['smtp_port'] = '465'; //smtp port number
        $config['smtp_user'] = $from_email;
        $config['smtp_pass'] = 'ffbdbcf$143'; //$from_email password
        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n"; //use double quotes
        $this->email->initialize($config);

        //send mail
        $this->email->from($from_email, 'UVL Sports');
        $this->email->to($to_email);
        $this->email->subject($subject);
        $this->email->message($message);
        return $this->email->send();
    }
	
	function verify_reset_password($to_email,$code){
		$this->db->select('*');
        $this->db->from('user');
        $where = "email='$to_email'";
        $this->db->where($where);
        $query_result=$this->db->get();
		
		if ($query_result->num_rows() > 0) {
            $result = $query_result->row();
			
			return ($code = md5($result->user_name)) ? true : false;
        } else {
			return false;
		}
	}
	function update_password($email,$password){
		$this->db->set('user_password',$password);
		$this->db->where('email', $email);
        $this->db->update('user');
		
		return true;
	}

	function save_new_pro_pic($data, $user_id)
    {
        $this->db->set('user_avatar',$data);
        $this->db->where('user_id', $user_id);
        $this->db->update('user');

        return true;
    }

}

?>

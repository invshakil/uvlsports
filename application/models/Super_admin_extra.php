<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of super_admin_extra
 *
 * @author shaki
 */
class super_admin_extra extends CI_Model {

    public function total_published_post() {
        $this->db->select('post_title');
        $this->db->from('tbl_post');
        $this->db->where('post_publication_status', 1);
        $query = $this->db->get();
        $total_row = $query->num_rows();
        return $total_row;
    }

    public function total_pending_post() {
        $this->db->select('post_title');
        $this->db->from('tbl_post');
        $this->db->where('post_publication_status', 0);
        $query = $this->db->get();
        $total_row = $query->num_rows();
        return $total_row;
    }

    public function total_user_hit() {
        $this->db->select_sum('hit_count');
        $query = $this->db->get('tbl_post');
        $result = $query->row();

        return $result;
    }

    public function total_verified_user() {
        $this->db->select('user_id');
        $this->db->from('user');
        $this->db->where('approval_status', 1);
        $query = $this->db->get();
        $total_row = $query->num_rows();
        return $total_row;
    }

    public function select_all_published_post_by_user($author_id) {
        $this->db->select('post_id');
        $this->db->from('tbl_post');
        $this->db->where('post_author_id', $author_id);
        $this->db->where('post_publication_status', 1);
        $query = $this->db->get();
        $total_row = $query->num_rows();

        return $total_row;
    }

    public function select_all_pending_post_by_user($author_id) {
        $this->db->select('post_id');
        $this->db->from('tbl_post');
        $this->db->where('post_author_id', $author_id);
        $this->db->where('post_publication_status', 0);
        $query = $this->db->get();
        $total_row = $query->num_rows();

        return $total_row;
    }
    
    public function select_user_info_by_id($user_id){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_id',$user_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }

    public function save_settings_info($data,$user_id) {
        $this->db->where('user_id', $user_id);
        $this->db->update('user', $data);
    }
    
    public function check_password($user_password, $user_id){
        $this->db->select('*');
        $this->db->from('user');
        $password = md5($user_password);
        $where = "user_password= '$password' AND user_id='$user_id' ";
        $this->db->where($where);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    
    public function save_password_info($data, $user_id){
        $this->db->where('user_id', $user_id);
        $this->db->update('user', $data);
    }
    
    public function select_all_post_by_user($user_id){
        $this->db->select('*');
        $this->db->from('tbl_post');
        $this->db->join('user', 'user_id = post_author_id');
        $this->db->join('tbl_category', 'category_id = post_category_id');
        $where = "tbl_post.deletion_status='1' AND post_author_id='$user_id'";
        $this->db->where($where);
        $this->db->order_by('post_id', 'desc');
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function total_hits_by_user($author_id)
    {
        $this->db->select_sum('hit_count');
        $this->db->where('post_author_id',$author_id);
        $query = $this->db->get('tbl_post');
        $result = $query->row();

        return $result;
    }

    public function avg_hits_by_user($author_id)
    {
        $this->db->select_avg('hit_count');
        $this->db->where('post_author_id',$author_id);
        $this->db->where('post_publication_status',1);
        $query = $this->db->get('tbl_post');
        $result = $query->row();

        return $result;
    }

    public function popular_post_info_by_user($author_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_post');
//        $this->db->join('user', 'user_id = post_author_id');
//        $this->db->join('tbl_category', 'category_id = post_category_id');
        $where = "tbl_post.deletion_status='1' AND post_author_id='$author_id'";
        $this->db->where($where);
        $this->db->order_by('hit_count', 'desc');
        $this->db->limit(5);
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

}

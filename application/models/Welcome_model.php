<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome_model
 *
 * @author shaki
 */
class Welcome_model extends CI_Model {

    public function searchResult($search) {


        $this->db->select("*");
        $this->db->from('tbl_post');
        $this->db->group_start();
        $this->db->like('post_title', $search);
//        $this->db->or_like('post_description', $search);
        $this->db->group_end();
        $this->db->join('user u', 'u.user_id = tbl_post.post_author_id');
        $this->db->where('post_publication_status', 1);
        $this->db->limit(10);
        $this->db->order_by('post_date', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function select_all_titles() {
        $this->db->select('*');
        $this->db->from('tbl_post');
        $this->db->where('post_publication_status', 1);
        $this->db->order_by('post_date', 'desc');
        $this->db->limit(5);
        $query_result = $this->db->get();
        $result = $query_result->result();

        return $result;
    }

    public function select_all_published_latest() {
        $this->db->select('*');
        $this->db->from('tbl_post');
        $this->db->join('tbl_category c', 'c.category_id = tbl_post.post_category_id');
        $where = "post_publication_status='1' AND featured_status='1'";
        $this->db->where($where);
        $this->db->order_by('post_date', 'desc');
        $this->db->limit(3);
        $query_result = $this->db->get();
        $result = $query_result->result();

        return $result;
    }

    public function select_index_published_popular() {
        $this->db->select('*');
        $this->db->from('tbl_post');
        $where = "post_publication_status='1' AND post_date BETWEEN DATE_SUB(NOW(), INTERVAL 90 DAY) AND NOW()";
        $this->db->where($where);
        $this->db->order_by('hit_count', 'desc');
        $this->db->order_by('post_date', 'desc');
        $this->db->limit(5);
        $query_result = $this->db->get();
        $result = $query_result->result();

        return $result;
    }

    public function transfer_live_latest_info()
    {
        $this->db->select('*');
        $this->db->from('tbl_tweets');
        $this->db->order_by('tweet_id','desc');
        $this->db->limit(3);
        $query_result = $this->db->get();
        $result = $query_result->result();

        return $result;
    }

    public function select_all_published_cricket() {
        $this->db->select('*');
        $this->db->from('tbl_post');
        $this->db->join('user u', 'u.user_id = tbl_post.post_author_id');
        $where = "post_publication_status='1' AND ( post_category_id='13' OR  post_category_id='9') ";
        $this->db->where($where);
        $this->db->order_by('post_date', 'desc');
        $this->db->limit(5);
        $query_result = $this->db->get();
        $result = $query_result->result();

        return $result;
    }


    public function select_all_latest_published_post($start,$limit) {
		$this->db->select('*');
        $this->db->from('tbl_post');
        $this->db->join('user u', 'u.user_id = tbl_post.post_author_id');
        $where = "post_publication_status='1' AND (post_category_id!='13' AND post_category_id != '9' ) ";
        $this->db->where($where);
        $this->db->order_by('post_date', 'desc');
        $this->db->limit($limit,$start);
        $query_result = $this->db->get();
        $result = $query_result->result();
		
        return $result;
	
	}
	
	public function total_post_published() {
        $this->db->where('post_publication_status', 1);
        
        $result = $this->db->count_all_results("tbl_post");
        return $result;
        
    }
	
	public function select_all_top_author() {
	
	//select a.post_author_id, count(a.post_id) from tbl_post a JOIN user b ON a.post_author_id = b.user_id group by post_author_id ORDER by 2 DESC limit 10

		
		$this->db->select('*');
		$this->db->select('post_author_id, count(post_id) as most, sum(hit_count) as top');
        $this->db->from('tbl_post a');
        $this->db->join('user b', 'a.post_author_id = b.user_id');
        $this->db->group_by('post_author_id');
        $this->db->order_by('top', 'desc');
        $this->db->limit(7, 'desc');
        $query_result = $this->db->get();
        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();
			return $result;
        }
		
        return false;
	
	
	}

    public function select_all_published_popular() {
        $this->db->select('*');
        $this->db->from('tbl_post');
        $where = "post_publication_status='1' AND post_date BETWEEN DATE_SUB(NOW(), INTERVAL 90 DAY) AND NOW()";
        $this->db->where($where);
        $this->db->order_by('hit_count', 'desc');
        $this->db->order_by('post_date', 'desc');
        $this->db->limit(10);
        $query_result = $this->db->get();
        $result = $query_result->result();

        return $result;
    }

    public function select_random_articles()
    {
        $this->db->select('*');
        $this->db->from('tbl_post');
        $where = "post_publication_status='1' AND post_category_id='7'";
        $this->db->where($where);
        $this->db->order_by('post_id', 'random');
        $this->db->limit(10);
        $query_result = $this->db->get();
        $result = $query_result->result();

        return $result;
    }

    public function select_all_published_popular_of_this_cat($category_id) {
        $this->db->select('*');
        $this->db->from('tbl_post');
        $where = "post_publication_status='1' AND post_category_id='$category_id'";
        $this->db->where($where);
        $this->db->order_by('hit_count', 'desc');
        $this->db->limit(5);
        $query_result = $this->db->get();
        $result = $query_result->result();

        return $result;
    }

    public function select_all_published_recent() {
        $this->db->select('*');
        $this->db->from('tbl_post');
        $where = "post_publication_status='1'";
        $this->db->where($where);
        $this->db->order_by('post_date', 'desc');
        $this->db->limit(10);
        $query_result = $this->db->get();
        $result = $query_result->result();

        return $result;
    }

    public function select_post_info_by_id($post_id) {
        $this->db->select('*');
        $this->db->from('tbl_post');
        $this->db->join('user u', 'u.user_id = tbl_post.post_author_id');
        $this->db->join('tbl_category c', 'c.category_id = tbl_post.post_category_id');
        $this->db->where('post_id', $post_id);
        $query_result = $this->db->get();
        $result = $query_result->row();

        return $result;
    }

//    public function get_next_post_info($post_id) {
//
//        $this->db->select('post_id');
//        $this->db->from('tbl_post');
//        $where = "post_publication_status = 1 AND post_id > '$post_id'";
//        $this->db->where($where);
//        $id = $this->db->get();
//        $p_id = $id->row()->post_id;
//
//        $this->db->select('*');
//        $this->db->from('tbl_post');
//        $where = "post_publication_status = '1' AND post_id = '$p_id' ";
//        $this->db->where($where);
//        $query_result = $this->db->get();
//        $result = $query_result->row();
//
//        return $result;
//    }
//
//    public function get_prev_post_info($post_id) {
//        $this->db->select('post_id');
//        $this->db->from('tbl_post');
//        $where = "post_publication_status = 1 AND post_id < '$post_id'";
//        $this->db->where($where);
//        $id = $this->db->get();
//        $p_id = $id->row()->post_id;
//
//        $this->db->select('*');
//        $this->db->from('tbl_post');
//        $where = "post_publication_status = '1' AND post_id = '$p_id' ";
//        $this->db->where($where);
//        $query_result = $this->db->get();
//        $result = $query_result->row();
//
//        return $result;
//    }
    public function get_similar_post($category_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_post');
        $where = "post_publication_status='1' AND post_category_id='$category_id'";
        $this->db->where($where);
        $this->db->order_by('post_id', 'random');
        $this->db->limit(6);
        $query_result = $this->db->get();
        $result = $query_result->result();

        return $result;
    }
    public function select_all_tag_by_post_id($post_id) {

        $this->db->select('tg.tag_name');
        $this->db->from('tbl_tags tg');
        $this->db->join('post_multi_tag mt', 'mt.tag_id = tg.tag_id');
        $this->db->join('tbl_post p', 'p.post_id = mt.post_id');
        $this->db->where('p.post_id', $post_id);
        $query = $this->db->get();
        $result = $query->result();
        
        return $result;
    }


    public function select_user_info_by_id($user_id) {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->join('tbl_post p', 'p.post_author_id = user_id');
        $this->db->where('user_id', $user_id);
        $qresutl = $this->db->get();
        
        if ($qresutl->num_rows() > 0) {
            $result = $qresutl->row();
            
            return $result;
        }
        return false;

    }

    public function select_all_post_info_by_user($user_id, $limit, $start) {
        $this->db->select('*');
        $this->db->from('tbl_post');
        $this->db->join('user u', 'u.user_id = tbl_post.post_author_id');
        $this->db->join('tbl_category c', 'c.category_id = tbl_post.post_category_id');
        $this->db->where('post_author_id', $user_id);
        $this->db->where('post_publication_status', 1);
        $this->db->order_by('post_date', 'desc');
        $this->db->limit($limit, $start);
        $query_result = $this->db->get();
        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();
            return $result;
        }
        return false;
    }

    public function select_total_post_by_user($user_id) {
        $this->db->where('post_publication_status', 1);
        $this->db->where('post_author_id', $user_id);

        $result = $this->db->count_all_results("tbl_post");

        if ($result != 0) {

            return $result;
        } else {
            return 0;
        }
    }

    public function update_hit_count($hit_count, $post_id) {
        $this->db->set('hit_count', $hit_count);
        $this->db->where('post_id', $post_id);
        $this->db->update('tbl_post');
    }

    public function update_hit_count_video($hit_count, $vdo_id) {
        $this->db->set('hit_count', $hit_count);
        $this->db->where('video_id', $vdo_id);
        $this->db->update('tbl_videos');
    }
    
    public function get_feeds()
    {
        $this->db->select('*');
        $this->db->from('tbl_post');
        $this->db->join('user u', 'u.user_id = tbl_post.post_author_id');
        $where = "post_publication_status='1'";
        $this->db->where($where);
        $this->db->order_by('post_date', 'desc');
        $this->db->limit(15);
        $query_result = $this->db->get();
        $result = $query_result->result();

        return $result;
    }

    public function get_match_fixture($gw_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_match_schedule t');
        $this->db->join('tbl_gwtitle g', 'g.gw_id = t.gw_id');
        $where = "t.m_publication_status='1' AND t.gw_id = '$gw_id'";
        $this->db->where($where);
        $this->db->order_by('match_time', 'asc');
        $query_result = $this->db->get();
        $result = $query_result->result();

        return $result;
    }

}

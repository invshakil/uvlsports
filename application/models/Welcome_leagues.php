<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome_epl
 *
 * @author shaki
 */
class welcome_leagues extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

// Count all record of table "contact_info" in database.
    public function record_count($category_id)
    {
        $this->db->where('post_publication_status', 1);
        $this->db->where('post_category_id', $category_id);

        $result = $this->db->count_all_results("tbl_post");
        return $result;

    }

// Fetch data according to per_page limit.

    public function all_post_from_league($category_id, $limit, $start)
    {
        $result = array();
        $this->db->select('*');
        $this->db->from('tbl_post');
        $this->db->join('user u', 'u.user_id = tbl_post.post_author_id');
        $where = "post_publication_status='1' AND post_category_id='$category_id'";
        $this->db->order_by('post_id', 'desc');
        $this->db->where($where);
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result;
        }
        return false;
    }

    public function vdo_record_count()
    {
        $this->db->where('vdo_publication_status', 1);

        $result = $this->db->count_all_results("tbl_videos");
        return $result;
    }

    public function all_post_from_videos($limit, $start)
    {
        $result = array();
        $this->db->select('*');
        $this->db->from('tbl_videos');
        $this->db->join('user u', 'u.user_id = tbl_videos.vdo_author');
        $this->db->order_by('video_id', 'desc');
        $this->db->where('vdo_publication_status', 1);
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result;
        }
        return false;
    }

    public function select_latest_featured_video()
    {
        $this->db->select('*');
        $this->db->from('tbl_videos');
        $where = "vdo_publication_status='1' AND featured_status='1'";
        $this->db->where($where);
        $this->db->order_by('hit_count', 'desc');
        $this->db->limit(1);
        $query_result = $this->db->get();
        $result = $query_result->row();

        return $result;
    }

    public function save_tweet_info($data)
    {
        $this->db->insert('tbl_tweets', $data);
        return true;
    }

    public function total_tweet() {
        $result = $this->db->count_all_results("tbl_tweets");
        return $result;

    }

    public function fetch_tweets_info($limit,$start)
    {
        $this->db->select('*');
        $this->db->from('tbl_tweets');
        $this->db->order_by('tweet_id','desc');
        $this->db->limit($start,$limit);
        $query_result = $this->db->get();
        $result = $query_result->result();

//        echo '<pre>';
//        print_r($result);
//        exit();

        return $result;
    }

    public function delete_tweet_info_by_id($tweet_id)
    {
        $this->db->where('tweet_id', $tweet_id);
        $this->db->delete('tbl_tweets');
    }

}

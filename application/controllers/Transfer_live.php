<?php

class transfer_live extends CI_Controller
{
    public function index()
    {

        $data = array();
        $data['title'] = 'খেলার সর্বশেষ সংবাদ || ইউনিভার্সাল স্পোর্টস';

        $content_per_page = 10;
        $count = $this->welcome_leagues->total_tweet();
        $data['total_data'] = ceil($count / $content_per_page);


        $data['popular_post'] = $this->welcome_model->select_all_published_popular();
        $data['recent_post'] = $this->welcome_model->select_all_published_recent();
//        $data['image'] = base_url().'live-transfers.png';
        $data['image'] = 'https://cms.groupeditors.com/img/gh_def20141110-131647-274.jpg';
        $data['url'] = base_url().'latest-short-stories';
        $data['description'] = 'ট্রান্সফার রিউমার, নিউজের লেটেষ্ট আপডেটের তালিকা।';
        $data['keyword'] = "ইউনিভার্সাল স্পোর্টস, ফুটবল, ক্রিকেট, ম্যাচ বিশ্লেষণ, ফুটবল ফ্যানস বাংলাদেশ, এফএফবিডি, ইংলিশ প্রিমিয়ার লিগ, পাঠকদের মতামত, ট্রান্সফার রিউমার";
        $data['maincontent'] = $this->load->view('frontend/transfer_live', $data, true);
        $this->load->view('frontend/index_master', $data);
    }

    public function save_tweet()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('tweet_title', 'Tweet Title', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('tweet_image', 'Tweet Image', 'trim');
        $this->form_validation->set_rules('tweet_description', 'Tag Name', 'trim|required|min_length[3]');


        if ($this->form_validation->run() == FALSE) {
            echo "<pre>";
            print_r('wrong');
            exit();
            $this->index();
        } else {
            $data = array();
            $data['tweet_title'] = $this->input->post('tweet_title', true);
            $data['tweet_image'] = $this->input->post('tweet_image', true);
            $data['tweet_description'] = $this->input->post('tweet_description', true);

            date_default_timezone_set('Asia/Dhaka');
            $currentDateTime = date('Y-m-d H:i:s');
            $data['updated_at'] = $currentDateTime;

            $result = $this->welcome_leagues->save_tweet_info($data);
            if ($result) {
                echo 'Tweet added successfully!';
            } else {
                echo 'Insertion Failed!';
            }
        }
    }

    public function fetch_tweets()
    {
        $group_no = $this->input->post('group_no');
        $content_per_page = 10;
        $start = ceil($group_no * $content_per_page);
        $result = $this->welcome_leagues->fetch_tweets_info($start, $content_per_page);

        if (is_array($result) && count($result) >= 1) {

            foreach ($result as $post) {
                ?>

                <div class="single_page_area">
                    <h2 class="post_titile" id="post_title"
                        style="padding-left: 12px;"><?php echo $post->tweet_title; ?></h2>
                    <img class="img-center img-thumbnail img-responsive" src="<?php echo $post->tweet_image ?>" height="290px" width="480px" alt=""><hr/>
                    <div class="single_page_content"><p align="justify"
                                                        id="post_desc"><?php echo $post->tweet_description; ?></p>

                        <div class="share_post">
                            <a class="facebook" target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo base_url(); ?>latest-short-stories"><i class="fa fa-facebook"></i>Facebook</a>
                            <a class="twitter" target="_blank" href="http://twitter.com/home?status=<?php echo base_url(); ?>latest-short-stories"><i class="fa fa-twitter"></i>Twitter</a>
                            <a class="googleplus" target="_blank" href="https://plus.google.com/share?url=<?php echo base_url(); ?>latest-short-stories"><i class="fa fa-google-plus"></i>Google+</a>

                        </div>

                        <div class="post_commentbox">
                            <span><i class="fa fa-clock-o"
                                                                aria-hidden="true"></i>
                                Updated
                                At: <?php $date = $post->updated_at;
                                echo date("F j, Y, g:i a", strtotime($date)) ?></span>

                            <?php if ($this->session->userdata('access_level') == 1 || $this->session->userdata('access_level') == 2 || $this->session->userdata('user_id') == 10) { ?>
                                <div class="pull-right">
                                    <input type="button" name="edit" value="Edit" id="<?php echo $post->tweet_id; ?>" class="btn btn-info btn-xs edit_data" />
                                </div>

                                <div class="pull-right"><a
                                            href="<?php echo base_url(); ?>transfer_live/delete_this_tweet/<?php echo $post->tweet_id; ?>"
                                            onclick="return checkDelete();" class="fa fa-trash-o"> Delete this Tweet</a>
                                </div>

                                <?php
                            } ?>
                        </div>
                    </div>

                </div>


                <!--only admin can see -->
                <?php


            }
        }
    }

    public function fetch_tweet_by_id()
    {
        $tweet_id = $this->input->post('tweet_id');
        if($tweet_id != ''){
            $info = $this->db->get_where('tbl_tweets', array('tweet_id' => $tweet_id))->row();
            echo json_encode($info);
        }
    }

    public function update_tweet_by_id()
    {
        $tweet_id = $this->input->post('tweet_id');
        $tweet_title = $this->input->post('tweet_title');
        $tweet_description = $this->input->post('tweet_description');
        $tweet_image = $this->input->post('tweet_image');


        $this->db->set('tweet_title',$tweet_title)
            ->set('tweet_description',$tweet_description)
            ->set('tweet_image',$tweet_image)
            ->where('tweet_id',$tweet_id)->update('tbl_tweets');
    }

    public function delete_this_tweet($tweet_id)
    {
        $data = array();

        $this->welcome_leagues->delete_tweet_info_by_id($tweet_id);

        $data['message'] = 'Tweet Deleted Successfully!';
        $this->session->set_userdata($data);
        redirect('latest-short-stories');
    }
}
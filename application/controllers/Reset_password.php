<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user_login
 *
 * @author shaki
 */
class Reset_password extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $access_level = $this->session->userdata('access_level');

        if ($access_level) {
            if ($this->session->userdata('access_level') != 3) {
                redirect('secure_admin_login', 'refresh');
            }
        }
        $user_id = $this->session->userdata('user_id');
        if ($user_id != NULL) {
            redirect('user_dashboard', 'refresh');
        }
    }

    public function index(){
		$data = array();
        $data['title'] = 'Reset Password | User Panel';
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);
        $this->load->view('adminEntry/reset_password', $data);
	}
	
	public function user_email_check(){
		$this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email');
		if ($this->form_validation->run() == FALSE) {
			$this->index();
		} else {
			$user_email_address = $this->input->post('email', true);
			$this->load->model('admin_login');
			$result = $this->admin_login->check_user_email_exist($user_email_address);
			$code = md5($result);
			$sdata = array();

            if ($result) {
				$to_email = $user_email_address;
				$this->user_model->reset_password_email($to_email,$code);

                // successfully sent mail
                $sdata['message'] = '<span class="label label-success">Check your email to reset your password!</span>';
                $this->session->set_userdata($sdata);
                redirect('reset_password');
			} else {
				$sdata['message'] = '<span class="label label-danger">This email is not registered as User!</span>';
                $this->session->set_userdata($sdata);
                redirect('reset_password');
			}
		}
	}
	
	public function confirm_password($to_email, $code){
		if(isset($to_email,$code)){
			$to_email = trim($to_email);
			$verified = $this->user_model->verify_reset_password($to_email,$code);
			if($verified){
				$data = array();
				$data['title'] = 'Confirm Password | User Panel';
				$data['email'] = $to_email;
				$data['code'] = $code;
				$data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
				$data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);
				$this->load->view('adminEntry/confirm_password', $data);
			}
		}else{
			$sdata = array();
			$sdata['message'] = '<span class="label label-danger">This is something wrong with this link, please request reset process again!</span>';
            $this->session->set_userdata($sdata);
            redirect('reset_password');
		}
	}
	
	public function update_password(){
		$user_email = $this->input->post('email');
		$this->load->model('admin_login');
		$result = $this->admin_login->check_user_email_exist($user_email);
		$code = md5($result);
		$user_code = $this->input->post('user_code');
		
		if($code != $user_code){
			//if user change email from input field, will die
			die('Error updating password!');
		} else {
				$this->form_validation->set_rules('email', 'Email-ID', 'trim|required|valid_email');
				$this->form_validation->set_rules('user_code', 'User Code', 'trim|required');
				$this->form_validation->set_rules('user_password', 'Password', 'trim|required|md5');
				$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|md5|matches[user_password]');

				if ($this->form_validation->run() == FALSE) {
					$this->confirm_password($user_email,$code);
				} else {
					$email = $user_email;
					$password = $this->input->post('user_password');
					
					$result = $this->user_model->update_password($email,$password);
					
					if($result){
						$sdata = array();
						$sdata['message'] = '<span class="label label-success">Your Password updated, try login!</span>';
						$this->session->set_userdata($sdata);
						redirect('user_login');
					}else {
						$sdata = array();
						$sdata['message'] = '<span class="label label-danger">Something went wrong, try again resetting password!</span>';
						$this->session->set_userdata($sdata);
						redirect('reset_password');
					}
				}
		}
	}

}
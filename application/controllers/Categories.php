<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of categories
 *
 * @author shaki
 */
class Categories extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
        $admin_id = $this->session->userdata('user_id');
        if ($admin_id == NULL) {
            redirect('secure_admin_login', 'refresh');
        }
    }
    public function index() {

        $data = array();
        $data['title'] = 'Categories | Admin Master';
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);

        $data['all_category'] = $this->super_admin_model->select_all_category();

        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/categories', $data, true);
        $this->load->view('adminEntry/admin_master', $data);
    }

    public function save_category() {
        //set validation rules
        $this->load->library('form_validation');

        $this->form_validation->set_rules('category_name', 'Category Name', 'trim|required|min_length[3]|max_length[30]|is_unique[tbl_category.category_name]');
        $this->form_validation->set_rules('category_description', 'Category description', 'trim|required');
        $this->form_validation->set_rules('publication_status', 'Publication Status', 'trim|required');


        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            
            $data = array();
            $data['category_name'] = $this->input->post('category_name', true);
            $data['category_description'] = $this->input->post('category_description', true);
            $data['publication_status'] = $this->input->post('publication_status', true);
            $this->super_admin_model->save_category_info($data);
            $sdata = array();
            $sdata['message'] = 'Category Saved Successfully!';
            $this->session->set_userdata($sdata);
            redirect('categories');
        }
    }

    public function fetch_category_info_by_id(){
        $category_id = $this->input->post('category_id',true);
        $data = $this->super_admin_model->fetch_category_info_by_id($category_id);

        foreach($data as $row)
        {
            $output['category_name'] = $row->category_name;
            $output['category_description'] = $row->category_description;
            $output['publication_status'] = $row->publication_status;
        }
        echo json_encode($output);
    }
}

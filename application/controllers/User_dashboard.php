<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user_dashboard
 *
 * @author shaki
 */
class User_dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $access_level = $this->session->userdata('access_level');


        if ($access_level != 3) {
            redirect('super_admin_panel', 'refresh');
        } elseif ($access_level == NULL) {
            redirect('sign-in', 'refresh');
        }

    }

    public function index()
    {

        $user_id = $this->session->userdata('user_id');

        $fb_info = $this->db->get_where('user', array('user_id' => $user_id))->row('user_fb');

        if ($fb_info == '') {
            $sdata['message'] = 'Please submit your facebook id and other information if available!';
            $this->session->set_userdata($sdata);
            redirect('account_settings');
        } else {


            $data = array();

            $data['title'] = 'Dashboard | User Panel';

            $author_id = $this->session->userdata('user_id');

            $data['total_published_post_by_user'] = $this->super_admin_extra->select_all_published_post_by_user($author_id);
            $data['total_pending_post_by_user'] = $this->super_admin_extra->select_all_pending_post_by_user($author_id);

            $data['total_hits'] = $this->super_admin_extra->total_hits_by_user($author_id);
            $data['avg_hits'] = $this->super_admin_extra->avg_hits_by_user($author_id);

            $data['popular_post_by_user'] = $this->super_admin_extra->popular_post_info_by_user($author_id);

            $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
            $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);

            $data['admin_maincontent'] = $this->load->view('adminEntry/user_dashboard', $data, true);

            $this->load->view('adminEntry/admin_master', $data);
        }
    }

    public function add_post()
    {
        $data = array();
        $data['title'] = 'Add Post | Admin Master';
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);

        $data['all_published_category'] = $this->super_admin_model->select_all_published_category();
        $data['all_published_tag'] = $this->super_admin_model->select_all_published_tag();


        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/add_post', $data, true);
        $this->load->view('adminEntry/admin_master', $data);
    }

    public function form_validation_for_insert_post()
    {
        //set validation rules
        $this->load->library('form_validation');

        $this->form_validation->set_rules('post_title', 'Post Title', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('post_description', 'Post Description', 'trim|required');
        $this->form_validation->set_rules('post_category_id', 'Category Name', 'trim|required');
        $this->form_validation->set_rules('post_publication_status', 'Publication Status', 'trim|required');
    }

    public function save_post()
    {

        $this->form_validation_for_insert_post();

        if ($this->form_validation->run() == FALSE) {

            $this->add_post();
        } else {

            $this->saveData();
        }
    }

    public function saveData()
    {
        $data = array();
        date_default_timezone_set('Asia/Dhaka');
        $post_time = date('Y-m-d H:i:s');

        $author_id = $this->session->userdata('user_id');
        $data['post_title'] = $this->input->post('post_title', true);
        $data['post_description'] = $this->input->post('post_description', true);
        $data['post_category_id'] = $this->input->post('post_category_id', true);
        $data['post_date'] = $post_time;
        $data['post_author_id'] = $author_id;
        $data['post_publication_status'] = $this->input->post('post_publication_status', true);


        $this->super_admin_model->save_post_info($data);

        $sdata = array();
        $sdata['message'] = 'Post Submitted for Approval!';
        $this->session->set_userdata($sdata);
        redirect('user_dashboard/add_post');
    }

    public function my_post()
    {
        $data = array();
        $data['title'] = 'My Post | User Panel';
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);

        $user_id = $this->session->userdata('user_id');

        $data['all_post'] = $this->super_admin_extra->select_all_post_by_user($user_id);

        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/my_post', $data, true);
        $this->load->view('adminEntry/admin_master', $data);
    }

    public function edit_post($post_id)
    {
        $data = array();
        $data['title'] = 'Edit Post | Admin Master';

        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);

        $data['all_published_category'] = $this->super_admin_model->select_all_published_category();
        $data['all_published_tag'] = $this->super_admin_model->select_all_published_tag();
        $data['post_info'] = $this->super_admin_model->select_post_info_by_id($post_id);

        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/edit_pages/edit_post', $data, true);
        $this->load->view('adminEntry/admin_master', $data);
    }

    public function update_post()
    {
        //set validation rules

        $data = array();
        $post_id = $this->input->post('post_id', true);

        date_default_timezone_set('Asia/Dhaka');
        $post_time = date('Y-m-d H:i:s');

        $data['post_title'] = $this->input->post('post_title', true);
        $data['post_description'] = $this->input->post('post_description', true);
        $data['post_category_id'] = $this->input->post('post_category_id', true);
        $data['post_date'] = $post_time;
        $data['post_publication_status'] = $this->input->post('post_publication_status', true);


        $this->super_admin_model->update_post_info($data, $post_id);
        $sdata = array();
        $sdata['message'] = 'Post Updated Successfully!';
        $this->session->set_userdata($sdata);
        redirect('user_dashboard/my_post');
    }

    public function change_profile_picture()
    {
        $user_id = $this->session->userdata('user_id');


        // Image Upload

        $config['upload_path'] = './image_upload/users/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 200;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $error = array();
        $fdata = array();

        if (!$this->upload->do_upload('user_avatar')) {
            $error = array('error' => $this->upload->display_errors());

            $this->session->set_userdata('error', $error['error']);
            if($this->session->userdata('access_level') == 3) {
                redirect('user_dashboard');
            } else {
                redirect('super_admin_panel');
            }


        } else {
            $fdata = $this->upload->data();
            $user_avatar = $config['upload_path'] . $fdata['file_name'];

            //Dropping old image from server

            $image = $this->session->userdata('user_avatar');
            if($image){
                if (file_exists($image)) {
                    unlink(FCPATH . $image);
                }
            }

            //
        }

        $result = $this->user_model->save_new_pro_pic($user_avatar, $user_id);

        if ($result){
            $this->session->set_userdata('message', 'Successfully changed!!!');

            $this->session->set_userdata('user_avatar',$user_avatar);

            if($this->session->userdata('access_level') == 3) {
                redirect('user_dashboard');
            } else {
                redirect('super_admin_panel');
            }

        }else{
            $this->session->set_userdata('message', 'Something went wrong!!!');

            if($this->session->userdata('access_level') == 3) {
                redirect('user_dashboard');
            } else {
                redirect('super_admin_panel');
            }
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('user_name');
        $this->session->unset_userdata('user_avatar');
        $this->session->unset_userdata('access_level');
        $sdata = array();
        $sdata['message'] = 'Successfully Logged Out!';
        $this->session->set_userdata($sdata);
        redirect('sign-in');
    }



}

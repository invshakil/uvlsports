<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of post_controller
 *
 * @author shaki
 */
class Post_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $admin_id = $this->session->userdata('user_id');
        if ($admin_id == NULL) {
            redirect('secure_admin_login', 'refresh');
        }

        $access_level = $this->session->userdata('access_level');

        if ($access_level == 3) {
            redirect('user_dashboard', 'refresh');
        }
        $this->load->library('image_lib');
    }

    //put your code here
    /*
     * POST ELEMENT -> Add Post & Manage Post
     */
    public function draft_post()
    {
        $data['title'] = 'Draft Post | Admin Master';
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);
        $data['all_published_category'] = $this->super_admin_model->select_all_published_category();
        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/draft_post', $data, true);
        $this->load->view('adminEntry/admin_master', $data);
    }

    function save_draft_post()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('post_title', 'Post Title', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('post_description', 'Post Description', 'trim|required');

        if ($this->form_validation->run() == FALSE) {

            $this->draft_post();
        } else {

            $data = array();
            date_default_timezone_set('Asia/Dhaka');
            $post_time = date('Y-m-d H:i:s');

            $author_id = $this->session->userdata('user_id');
            $data['post_title'] = $this->input->post('post_title', true);
            $data['post_description'] = $this->input->post('post_description');
            $data['post_category_id'] = $this->input->post('post_category_id', true);
            $data['post_date'] = $post_time;
            $data['post_author_id'] = $author_id;

            $this->db->insert('tbl_post', $data);

            $sdata = 'Post has been drafted! You can edit this content again.';
            $this->session->set_userdata('message',$sdata);
            redirect('post_controller/draft_post');

        }
    }

    public function add_post()
    {

        $data = array();
        $data['title'] = 'Add Post | Admin Master';
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);

        $data['all_published_category'] = $this->super_admin_model->select_all_published_category();
        $data['all_published_tag'] = $this->super_admin_model->select_all_published_tag();


        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/add_post', $data, true);
        $this->load->view('adminEntry/admin_master', $data);
    }

    public function form_validation_for_insert_post()
    {
        //set validation rules
        $this->load->library('form_validation');

        $this->form_validation->set_rules('post_title', 'Post Title', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('en_post_url', 'Post URL', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('meta_description', 'Meta Description', 'trim|required|max_length[250]');
        $this->form_validation->set_rules('post_description', 'Post Description', 'trim|required');
        $this->form_validation->set_rules('post_category_id', 'Category Name', 'trim|required');
        $this->form_validation->set_rules('post_tag_id', 'Tag Name');
        $this->form_validation->set_rules('featured_status', 'Featured Status');
        $this->form_validation->set_rules('post_image', 'Post Image');
        $this->form_validation->set_rules('post_publication_status', 'Publication Status', 'trim|required');
    }

    public function save_post()
    {

        $this->form_validation_for_insert_post();

        if ($this->form_validation->run() == FALSE) {

            $this->add_post();
        } else {

            $this->saveData();
        }
    }

    public function saveData()
    {
        $data = array();
        date_default_timezone_set('Asia/Dhaka');
        $post_time = date('Y-m-d H:i:s');

        $author_id = $this->session->userdata('user_id');
        $data['post_title'] = $this->input->post('post_title', true);
        $data['en_post_url'] = $this->input->post('en_post_url', true);
        $data['meta_description'] = $this->input->post('meta_description', true);
        $data['post_description'] = $this->input->post('post_description');
        $data['post_category_id'] = $this->input->post('post_category_id', true);
        $data['post_date'] = $post_time;
        $data['post_author_id'] = $author_id;
        $data['featured_status'] = $this->input->post('featured_status', true);
        $data['post_publication_status'] = $this->input->post('post_publication_status', true);
        $data['post_tag_id'] = $this->input->post('post_tag_id', true);
	
		if (!file_exists($_FILES['post_image']['tmp_name'])) {
			$data['post_image'] = $this->input->post('post_image', true);
			
			$this->super_admin_model->save_post_info($data); //getting post id from post insert function
			
			$sdata = array();
			$sdata['message'] = 'Post Saved Successfully!';
			$this->session->set_userdata($sdata);
			redirect('post_controller/add_post');
		}

        /*
         * IF FILE IS NOT UPLOADED
         * Image Upload
         */

        $this->imageUp($data);
    }

    public function imageUP($data)
    {

        $this->load->library('image_lib');
        $config['upload_path'] = './image_upload/post_image/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 250;
        $config['maintain_ratio'] = TRUE;
        $config['remove_spaces'] = TRUE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);



        if (!$this->upload->do_upload('post_image')) {
			$error = array('message' => $this->upload->display_errors());
	
			$this->session->set_userdata('message', $error['message']);
			
            redirect('post_controller/add_post');
        } else {
            $fdata = array();
            $fdata = $this->upload->data();
//            $data['post_image'] = $config['upload_path'] . $fdata['file_name'];
            $data['post_image'] = $fdata['file_name'];

            $img_data = $fdata['file_name'];

            $this->thumbnail($img_data);
        }


        /*
         * ! Image Upload
         */

        
        
        $this->super_admin_model->save_post_info($data); //getting post id from post insert function
	
		$sdata = array();
		$sdata['message'] = 'Post Saved Successfully!';
		$this->session->set_userdata($sdata);
		redirect('post_controller/add_post');

    }

    public function thumbnail($img_data)
    {

        //path of the source file
        $config['source_image'] = './image_upload/post_image/' . $img_data;
        $config['new_image'] = './image_upload/post_image/resized/';
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 350;
        $config['height'] = 200;

        $this->load->library('image_lib', $config);

        $this->image_lib->initialize($config);
        $this->image_lib->resize();

        $config['source_image'] = './image_upload/post_image/' . $img_data;
        $config['new_image'] = './image_upload/post_image/thumbs/';
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 150;
        $config['height'] = 84;

        $this->load->library('image_lib', $config);

        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

    public function manage_post()
    {

        $data = array();
        $data['title'] = 'Manage Post | Admin Master';
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);

        $data['str'] = $this->input->get('name', true);


        /* Pagination Config */
        $config = array();
        $config["base_url"] = base_url() . "post_controller/manage_post/";
        $config["total_rows"] = $this->super_admin_model->count_post();
        $config["per_page"] = 15;
        $config["num_links"] = TRUE;

        /* BootStrap pagination Button Settings */
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        /* !-BootStrap pagination Button Settings */

        /* !- Pagination Config */
        $this->pagination->initialize($config);

        $str_links = $this->pagination->create_links();
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = explode('&nbsp;', $str_links);

        if ($data['str'] == NULL) {
            $data['all_post'] = $this->super_admin_model->select_all_post($config["per_page"], $page);

        } else {
            $data['all_post'] = $this->super_admin_model->searched_post($data['str']);
        }
        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/manage_post', $data, true);
        $this->load->view('adminEntry/admin_master', $data);
    }

    // Publish & Unpublish Your Post

    public function publish_post($post_id)
    {
        $this->super_admin_model->published_product_info($post_id);
        redirect('post_controller/manage_post');
    }

    public function unpublish_post($post_id)
    {
        $this->super_admin_model->unpublished_product_info($post_id);
        redirect('post_controller/manage_post');
    }

    // Edit Post

    public function edit_post($post_id)
    {

        $data['post_info'] = $this->super_admin_model->select_post_info_by_id($post_id);

        if ($data['post_info']->last_modified_by == NULL || $data['post_info']->last_modified_by == $this->session->userdata('user_name')) {

            $admin_name = $this->session->userdata('user_name');
            $this->super_admin_model->save_last_modified_admin($admin_name,$post_id); //This will save which admin is currently editing the file

            $data = array();
            $data['title'] = 'Edit Post | Admin Master';

            $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
            $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);

            $data['all_published_category'] = $this->super_admin_model->select_all_published_category();
            $data['all_published_tag'] = $this->super_admin_model->select_all_published_tag();
            $data['post_info'] = $this->super_admin_model->select_post_info_by_id($post_id);

            $data['admin_maincontent'] = $this->load->view('adminEntry/pages/edit_pages/edit_post', $data, true);
            $this->load->view('adminEntry/admin_master', $data);
        } else {
            $data = array();
            $data['message'] = '<p style="background: red; font-size: 18px;">Someone is editing the content!!</p>';
            $this->session->set_userdata($data);
            redirect('post_controller/manage_post');
        }

    }

    public function update_post()
    {
        //set validation rules

        $data = array();
        $post_id = $this->input->post('post_id', true);
        $image = $this->super_admin_model->select_post_info_by_id($post_id);

        date_default_timezone_set('Asia/Dhaka');
        $post_time = date('Y-m-d H:i:s');

        $data['post_title'] = $this->input->post('post_title', true);
        $data['en_post_url'] = $this->input->post('en_post_url', true);
        $data['meta_description'] = $this->input->post('meta_description', true);
        $data['post_description'] = $this->input->post('post_description');
        $data['post_category_id'] = $this->input->post('post_category_id', true);
        $data['featured_status'] = $this->input->post('featured_status', true);
        $data['post_tag_id'] = $this->input->post('post_tag_id', true);

        if ($image->post_publication_status == 0){
            $data['post_date'] = $post_time;
        }


        $data['post_publication_status'] = $this->input->post('post_publication_status', true);
        $data['edit_lock_status'] = 0;
        $data['last_modified_by'] = NULL;

        //checks if there is any file to upload otherwise it send previous image as post image
        if ($_FILES['post_image']['size'] !== 0) {
            /*
             * Image Upload
             */

            $config['upload_path'] = './image_upload/post_image/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 512;
            $config['overwrite'] = true;
            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $error = array();
//            $fdata = array();

            if (!$this->upload->do_upload('post_image')) {
                $sdata = array();
                $sdata['message'] = $error;
                $this->session->set_userdata($sdata);
                redirect('post_controller/edit_post/' . $post_id);
            } else {
                $fdata = $this->upload->data();
                $data['post_image'] = $fdata['file_name'];

                $img_data = $fdata['file_name'];

                $this->thumbnail($img_data);
            }


            /*
             * ! Image Upload
             */
        } else {
            $image_path = $image->post_image;
            $data['post_image'] = $image_path;
        }

        //Updates Post info
        $this->super_admin_model->update_post_info($data, $post_id);
        
        $sdata = array();
        $sdata['message'] = 'Post Updated Successfully!';
        $this->session->set_userdata($sdata);
        redirect('post_controller/manage_post');
    }

    public function post_editor($post_id)
    {
        $user_id = $this->input->post('last_modified_by');
        $result = $this->super_admin_model->editor_info($post_id, $user_id);
        if($result){
            echo $result;
        }else{
            echo 'No one is editing';
        }
    }

    //Delete Post

    public function delete_post($post_id)
    {
        $sdata = array();

        //DELETES IMAGE FROM SERVER
        $data = $this->super_admin_model->select_post_info_by_id($post_id);
        $image = $data->post_image;
        if (file_exists($image)) {
            unlink(FCPATH . $image);
        }

        $this->super_admin_model->delete_post_info_by_id($post_id);

        $sdata['message'] = 'Post Deleted Successfully!';
        $this->session->set_userdata($sdata);
        redirect('post_controller/manage_post');
    }

    /*
     * !- POST ELEMENT -> Add Post & Manage Post
     */

    public function browse_pending_post()
    {
        $data = array();
        $data['title'] = 'Pending Post | Admin Master';
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);

        $data['all_pending_post'] = $this->super_admin_model->select_all_pending_post();

        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/browse_pending_post', $data, true);
        $this->load->view('adminEntry/admin_master', $data);
    }

    public function save_tag()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('tag_name', 'Tag Name', 'trim|required|min_length[3]|max_length[30]|is_unique[tbl_tags.tag_name]');


        if ($this->form_validation->run() == FALSE) {
            $this->add_post();
        } else {
            $data = array();
            $data['tag_name'] = ucwords($this->input->post('tag_name', true));
            $data['publication_status'] = 1;
            $result = $this->super_admin_model->save_tag_info($data);
            if ($result) {
                echo 'Tag Added to Database, now you can use this tag!';
            } else {
                echo 'Insertion Failed!';
            }
        }
    }

        public function updated_tags()
        {
            $data = array();
            $data['all_published_tag'] = $this->super_admin_model->select_all_published_tag();
            $data['admin_maincontent'] = $this->load->view('adminEntry/pages/add_post', $data, true);
            $this->load->view('adminEntry/admin_master', $data);

//            echo json_encode($result);
        }

    }
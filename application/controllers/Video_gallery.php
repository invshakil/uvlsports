<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of video_gallery
 *
 * @author shaki
 */
class Video_gallery extends CI_Controller{
    //put your code here
    public function index(){
        $config = array();
        $config["base_url"] = base_url() . "/videos_gallery/";
        $config["total_rows"] = $this->welcome_leagues->vdo_record_count();
        $config["per_page"] = 20;
        $config["num_links"] = TRUE;
        $config['prefix'] = '/post_after/';

        /* !- Pagination Config */
        $this->pagination->initialize($config);

        $str_links = $this->pagination->create_links();
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = explode('&nbsp;', $str_links);
        $data['all_post_from_league'] = $this->welcome_leagues->all_post_from_videos($config["per_page"], $page);
        $data['title'] = 'Videos || Universal Sports';
        $data['maincontent'] = $this->load->view('video_gallery', $data, true);
        $this->load->view('index_master', $data);
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: shaki
 * Date: 14-Aug-17
 * Time: 1:20 AM
 */
class Contact extends CI_Controller
{


    public function index()
    {
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);
        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/contact/inbox', $data, true);
        $this->load->view('adminEntry/admin_master', $data);
    }
}
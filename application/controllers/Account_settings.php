<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of account_settings
 *
 * @author shaki
 */
class Account_settings extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();

        $admin_id = $this->session->userdata('user_id');
        if ($admin_id == NULL) {
            redirect('user_login', 'refresh');
        }

    }

    public function index() {

        $data = array();
        
        $data['title'] = 'Account Settings | User Panel';
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);
        
        $user_id = $this->session->userdata('user_id');
        
        $data['user_info'] = $this->super_admin_extra->select_user_info_by_id($user_id);
        
        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/account_settings', $data, true);

        $this->load->view('adminEntry/admin_master', $data);
    }

    public function settings_validation() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('user_name', 'Name', 'trim|required');
        $this->form_validation->set_rules('user_bio', 'Biography', 'trim|required|max_length[360]');
//        $this->form_validation->set_rules('user_fb', 'Facebook ID', 'trim|required');
//        $this->form_validation->set_rules('user_tw', 'Twitter ID', 'trim|required');
    }

    public function save_settings() {
        $this->settings_validation();

        if ($this->form_validation->run() == FALSE) {

            $this->settings_validation();
        } else {
            $data = array();
            $data['user_name'] = $this->input->post('user_name', true);
            $data['email'] = $this->input->post('email', true);
            $data['user_bio'] = $this->input->post('user_bio', true);
            $data['user_fb'] = $this->input->post('user_fb', true);
            $data['user_tw'] = $this->input->post('user_tw', true);
            $user_id = $this->session->userdata('user_id');
            
            $this->super_admin_extra->save_settings_info($data,$user_id);
            $sdata = array();
            $sdata['message'] = 'Account Information Updated!';
            $this->session->set_userdata($sdata);
            $this->session->set_userdata('user_name',$data['user_name']);
            redirect('account_settings');
        }
    }

    public function update_fb()
    {
        $data = array();
        $data['user_fb'] = $this->input->post('user_fb', true);

        $user_id = $this->input->post('user_id', true);
        $this->db->set($data)->where('user_id',$user_id)->update('user');


        $sdata = array();
        $sdata['message'] = 'Users FB ID Link Updated!';
        $this->session->set_userdata($sdata);

        redirect('super_admin_panel/manage_user');
    }

}

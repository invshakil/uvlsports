<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of user_registration
 *
 * @author shaki
 */
class User_registration extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session', 'form_validation', 'email'));
        $this->load->database();
        $this->load->model('user_model');
    }

    public function index() {
//        echo 'Registration process is in revamp! come back later. Thanks for your assistance!';
        $this->register();
    }

    function register() {

        $this->load->database();

        //set validation rules
        $this->load->library('form_validation');

        $this->form_validation->set_rules('user_name', 'First Name', 'trim|required|min_length[3]|max_length[30]');
        $this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email|is_unique[user.email]');
        $this->form_validation->set_rules('user_password', 'Password', 'trim|required|md5');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|md5|matches[user_password]');
//        $this->form_validation->set_rules('user_avatar', 'User Image');
        //validate form input

        if ($this->form_validation->run() == FALSE) {


            // fails
            $data = array();
            $data['title'] = 'Registration | Admin Panel';
            $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
            $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);

            $this->load->view('adminEntry/user_registration', $data);
        } else {

            //insert the user registration details into database
            date_default_timezone_set('Asia/Dhaka');
            $reg_time = date("Y-m-d");


            $data = array(
                'user_name' => $this->input->post('user_name', TRUE),
                'email' => $this->input->post('email', TRUE),
                'user_password' => $this->input->post('user_password', TRUE),
                'access_level' => 3,
                'reg_time' => $reg_time
            );

//            Check whether user upload picture
            $config['upload_path'] = './image_upload/users/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 200;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);


            if ($this->user_model->insertUser($data)) {

                $to_email = $this->input->post('email', TRUE);

                $this->user_model->sendEmail($to_email);

                // successfully sent mail
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">You are Successfully Registered! Please confirm the mail sent to your Email-ID, '
                        . 'It May Take upto several Mins to receive confirmation mail.</div>');
                redirect('user_registration');

            } else {
                // error
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                redirect('user_registration');
            }
        }
    }

    function verify($hash = NULL) {
        if ($this->user_model->verifyEmailID($hash)) {
            $this->session->set_flashdata('verify_msg', '<div class="alert alert-success">Your Email Address is successfully verified! Please login to access your account!</div>');
            redirect('user_login');
        } else {
            $this->session->set_flashdata('verify_msg', '<div class="alert alert-danger">Sorry! There is error verifying your Email Address!</div>');
            redirect('user_registration');
        }
    }

}

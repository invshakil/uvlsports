<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of super_admin_controller
 *
 * @author shakil
 */
class Social_verification extends CI_Controller {

    public function index() {
        $data = array();
        $data['title'] = 'Login | Admin Panel';
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);
        $this->load->view('adminEntry/user_login', $data);
//        $this->load->view('adminEntry/admin_master','',TRUE);
    }

    public function web_login()
    {
        $data['user'] = array();

        // Check if user is logged in
        if ($this->facebook->is_authenticated())
        {

            // User logged in, get user details
            $user = $this->facebook->request('get', '/me?fields=id,name,email,picture');
            if (!isset($user['error']))
            {
                $data['user'] = $user;

                if (isset($user['email']))
                {
                    $email = $user['email'];
                }
                else
                {
                    $email = $user['id'];
                }

                $result = $this->db->where('email',$email)->get('user')->row();
                
                if($result){

                    if (isset($result->oauth_provider))
                    {
                        $fb['user_avatar'] = 'https://graph.facebook.com/'.$user['id'].'/picture?width=720&height=720';
                        $fb['user_fb'] = 'http://www.facebook.com/'.$user['id'];
                        $fb['approval_status'] = 1;
                        $fb['oauth_provider'] = 'facebook';
                    }
                    else
                    {
                        $fb['user_avatar'] = 'https://graph.facebook.com/'.$user['id'].'/picture?width=720&height=720';
                        $fb['user_fb'] = 'https://www.facebook.com/'.$user['id'];
                        $fb['oauth_provider'] = 'facebook';
                    }
                    

                    $this->db->where('user_id',$result->user_id)->update('user',$fb);
                    

                    if ($result->access_level != 3)
                    {
                        $sdata['user_id'] = $result->user_id;
                        $sdata['user_name'] = $result->user_name;
                        $sdata['user_avatar'] = $fb['user_avatar'];
                        $sdata['access_level'] = $result->access_level;
                        $sdata['login_type'] = 1;

                        $this->session->set_userdata($sdata);
                        
                        

                        redirect('super_admin_panel');
                    }
                    else
                    {
                        $sdata['user_id'] = $result->user_id;
                        $sdata['user_name'] = $result->user_name;
                        $sdata['user_avatar'] = $fb['user_avatar'];
                        $sdata['access_level'] = 3;
                        $sdata['login_type'] = 1;

                        $this->session->set_userdata($sdata);
                        
                        redirect('user_dashboard');
                    }

                }
                else{
                    $fb['email'] = $email;
                    $fb['user_name'] = $user['name'];
                    $fb['user_fb'] = 'https://www.facebook.com/'.$user['id'];
                    $fb['access_level'] = 3;
                    $fb['reg_time'] = date("Y-m-d");
                    $fb['oauth_provider'] = 'facebook';
                    $fb['approval_status'] = 1;
                    $fb['user_avatar'] = 'https://graph.facebook.com/'.$user['id'].'/picture?width=720&height=720';

                    $this->db->insert('user',$fb);

                    $sdata['user_id'] = $this->db->insert_id();
                    $sdata['user_name'] = $fb['user_name'];
                    $sdata['user_avatar'] = $fb['user_avatar'];
                    $sdata['access_level'] = 3;
                    $sdata['login_type'] = 1;

                    $this->session->set_userdata($sdata);
                    redirect('user_dashboard');
                }

                exit;
            }

        }

        // display view
        $this->load->view('adminEntry/login');
    }

    public function logout()
    {
        
        $this->facebook->destroy_session();

        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('user_name');
        $this->session->unset_userdata('user_avatar');
        $this->session->unset_userdata('access_level');
        $sdata = array();
        $sdata['message'] = '<span class="label label-success">Successfully Logged Out!</span>';
        $this->session->set_userdata($sdata);
        
        redirect('sign-in', 'refresh');
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of super_admin
 *
 * @author shakil
 */
class Super_admin_panel extends CI_Controller
{
	
	public function __construct()
	{
		parent ::__construct();
		
		
		$access_level = $this -> session -> userdata('access_level');
		
		if ($access_level == 3) {
			redirect('user_dashboard', 'refresh');
		} elseif ($access_level == NULL) {
			redirect('sign-in', 'refresh');
		}
		
		
	}
	
	public function index()
	{
		$data = array ();
		
		$data['title'] = 'Dashboard | Admin Master';
		
		$data['total_published_post'] = $this -> super_admin_extra -> total_published_post();
		$data['total_pending_post'] = $this -> super_admin_extra -> total_pending_post();
		$data['total_user_hit'] = $this -> super_admin_extra -> total_user_hit();
		$data['total_registered_user'] = $this -> super_admin_extra -> total_verified_user();
		
		$author_id = $this -> session -> userdata('user_id');
		
		$data['total_published_post_by_user'] = $this -> super_admin_extra -> select_all_published_post_by_user($author_id);
		$data['total_pending_post_by_user'] = $this -> super_admin_extra -> select_all_pending_post_by_user($author_id);
		$data['total_hits'] = $this -> super_admin_extra -> total_hits_by_user($author_id);
		$data['avg_hits'] = $this -> super_admin_extra -> avg_hits_by_user($author_id);
		
		$data['popular_post_by_user'] = $this -> super_admin_extra -> popular_post_info_by_user($author_id);

//        echo '<pre>';
//        print_r($data['popular_post_by_user']);
//        exit();
		
		$data['header_content'] = $this -> load -> view('adminEntry/header_content', '', true);
		$data['footer_content'] = $this -> load -> view('adminEntry/footer_content', '', true);
		$data['admin_maincontent'] = $this -> load -> view('adminEntry/dashboard', $data, true);
		$this -> load -> view('adminEntry/admin_master', $data);
	}
	
	/*
	 * User -> Add Admin & Manage User
	 */
	
	public function admin_list()
	{
		
		$data = array ();
		$data['title'] = 'Admin List | Admin Master';
		$data['header_content'] = $this -> load -> view('adminEntry/header_content', '', true);
		$data['footer_content'] = $this -> load -> view('adminEntry/footer_content', '', true);
		
		$data['all_admin_user'] = $this -> super_admin_model -> select_all_admin_user();
		
		$data['admin_maincontent'] = $this -> load -> view('adminEntry/pages/admin_list', $data, true);
		
		$this -> load -> view('adminEntry/admin_master', $data);
	}
	
	public function manage_user()
	{
		
		$data = array ();
		$data['title'] = 'Manage User | Admin Master';
		$data['header_content'] = $this -> load -> view('adminEntry/header_content', '', true);
		$data['footer_content'] = $this -> load -> view('adminEntry/footer_content', '', true);
		
		$data['str'] = $this -> input -> get('name', true);
		
		
		/* Pagination Config */
		$config = array ();
		$config["base_url"] = base_url() . "super_admin_panel/manage_user/";
		$config["total_rows"] = $this -> super_admin_model -> count_user();
		$config["per_page"] = 15;
		$config["num_links"] = TRUE;
		
		/* BootStrap pagination Button Settings */
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] = "</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		/* !-BootStrap pagination Button Settings */
		
		/* !- Pagination Config */
		$this -> pagination -> initialize($config);
		
		$str_links = $this -> pagination -> create_links();
		$page = ($this -> uri -> segment(3)) ? $this -> uri -> segment(3) : 0;
		$data["links"] = explode('&nbsp;', $str_links);
		
		if ($data['str'] == NULL) {
			
			$data['all_user'] = $this -> super_admin_model -> select_all_user($config["per_page"], $page);
		} else {
			$data['all_user'] = $this -> super_admin_model -> searched_user($data['str']);
		}
		
		$data['admin_maincontent'] = $this -> load -> view('adminEntry/pages/manage_user', $data, true);
		$this -> load -> view('adminEntry/admin_master', $data);
	}
	
	public function promote_user($user_id)
	{
		$this -> super_admin_model -> promote_user($user_id);
		$sdata['message'] = 'This user has been promoted to Admin!';
		$this -> session -> set_userdata($sdata);
		redirect('super_admin_panel/admin_list');
	}
	
	public function promote_admin_to_super_admin($user_id)
	{
		$this -> super_admin_model -> promote_admin_to_super_admin($user_id);
		$sdata['message'] = 'This user has been promoted to Super Admin!';
		$this -> session -> set_userdata($sdata);
		redirect('super_admin_panel/admin_list');
	}
	
	public function demote_sadmin_to_admin($user_id)
	{
		$this -> super_admin_model -> demote_sadmin_to_admin($user_id);
		$sdata['message'] = 'This user has been demoted to Admin!';
		$this -> session -> set_userdata($sdata);
		redirect('super_admin_panel/admin_list');
	}
	
	public function demote_admin_to_user($user_id)
	{
		$this -> super_admin_model -> demote_admin_to_user($user_id);
		$sdata['message'] = 'This user has been demoted to Admin!';
		$this -> session -> set_userdata($sdata);
		redirect('super_admin_panel/admin_list');
	}
	
	public function ban_user($user_id)
	{
		$this -> super_admin_model -> ban_user($user_id);
		$sdata['message'] = 'This user has been Banned from Login!';
		$this -> session -> set_userdata($sdata);
		redirect('super_admin_panel/manage_user');
	}
	
	public function verify_user($user_id)
	{
		$this -> super_admin_model -> verify_user($user_id);
		$sdata['message'] = 'This users Activation status has been changed to verified!';
		$this -> session -> set_userdata($sdata);
		redirect('super_admin_panel/manage_user');
	}
	
	public function delete_user($user_id)
	{
		$sdata = array ();
		
		//DELETES USER IMAGE FROM SERVER
		$data = $this -> super_admin_extra -> select_user_info_by_id($user_id);
		$image = $data -> user_avatar;
		if (file_exists($image)) {
			unlink(FCPATH . $image);
		}
		
		$this -> super_admin_model -> delete_user($user_id);
		$sdata['message'] = 'This user has been Deleted from DATABASE!';
		$this -> session -> set_userdata($sdata);
		redirect('super_admin_panel/manage_user');
	}
	
	/*
	 * !- POST ELEMENT -> Add Admin & Manage User
	 */
	
	
	/*
	 * !- POST ELEMENT -> Add Post & Manage Post
	 */
	
	public function my_post()
	{
		$data = array ();
		$data['title'] = 'My Post | User Panel';
		$data['header_content'] = $this -> load -> view('adminEntry/header_content', '', true);
		$data['footer_content'] = $this -> load -> view('adminEntry/footer_content', '', true);
		
		$user_id = $this -> session -> userdata('user_id');
		
		$data['all_post'] = $this -> super_admin_extra -> select_all_post_by_user($user_id);
		
		$data['admin_maincontent'] = $this -> load -> view('adminEntry/pages/my_post', $data, true);
		$this -> load -> view('adminEntry/admin_master', $data);
	}
	
	public function logout()
	{
		$this -> session -> unset_userdata('user_id');
		$this -> session -> unset_userdata('user_name');
		$this -> session -> unset_userdata('user_avatar');
		$this -> session -> unset_userdata('access_level');
		$sdata = array ();
		$sdata['message'] = '<span class="label label-success">Successfully Logged Out!</span>';
		$this -> session -> set_userdata($sdata);
		redirect('sign-in');
	}
	
	public function change_profile_picture()
	{
		$user_id = $this -> session -> userdata('user_id');
		
		
		// Image Upload
		
		$config['upload_path'] = './image_upload/users/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = 200;
		
		$this -> load -> library('upload', $config);
		$this -> upload -> initialize($config);
		$error = array ();
		$fdata = array ();
		
		if (!$this -> upload -> do_upload('user_avatar')) {
			$error = array ('error' => $this -> upload -> display_errors());
			
			$this -> session -> set_userdata('error', $error['error']);
			if ($this -> session -> userdata('access_level') == 3) {
				redirect('user_dashboard');
			} else {
				redirect('super_admin_panel');
			}
			
			
		} else {
			$fdata = $this -> upload -> data();
			$user_avatar = $config['upload_path'] . $fdata['file_name'];
			
			//Dropping old image from server
			
			$image = $this -> session -> userdata('user_avatar');
			if ($image) {
				if (file_exists($image)) {
					unlink(FCPATH . $image);
				}
			}
			
			//
		}
		
		$result = $this -> user_model -> save_new_pro_pic($user_avatar, $user_id);
		
		if ($result) {
			$this -> session -> set_userdata('message', 'Successfully changed!!!');
			
			$this -> session -> set_userdata('user_avatar', $user_avatar);
			
			if ($this -> session -> userdata('access_level') == 3) {
				redirect('user_dashboard');
			} else {
				redirect('super_admin_panel');
			}
			
		} else {
			$this -> session -> set_userdata('message', 'Something went wrong!!!');
			
			if ($this -> session -> userdata('access_level') == 3) {
				redirect('user_dashboard');
			} else {
				redirect('super_admin_panel');
			}
		}
	}
	
	/*
	 * IMAGE GALLERY
	 */
	
	public function image_gallery()
	{
		$data = array ();
		
		$data = array ();
		$data['title'] = 'Image Gallery | Admin Master';
		$data['header_content'] = $this -> load -> view('adminEntry/header_content', '', true);
		$data['footer_content'] = $this -> load -> view('adminEntry/footer_content', '', true);
		
		$count = $this -> db -> get_where('tbl_post', array ('post_publication_status' => 1)) -> result();
		
		/* Pagination Config */
		$config = array ();
		$config["base_url"] = base_url() . "super_admin_panel/image_gallery/";
		$config["total_rows"] = count($count);
		$config["per_page"] = 100;
		$config["num_links"] = TRUE;
		
		/* BootStrap pagination Button Settings */
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] = "</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		/* !-BootStrap pagination Button Settings */
		
		/* !- Pagination Config */
		$this -> pagination -> initialize($config);
		
		$str_links = $this -> pagination -> create_links();
		$page = ($this -> uri -> segment(3)) ? $this -> uri -> segment(3) : 0;
		$data["links"] = explode('&nbsp;', $str_links);
		
		$data['all_images'] = $this->super_admin_model->select_all_image($config["per_page"], $page);
		
		
		$data['admin_maincontent'] = $this -> load -> view('adminEntry/pages/image_gallery', $data, true);
		$this -> load -> view('adminEntry/admin_master', $data);
	}
	
}

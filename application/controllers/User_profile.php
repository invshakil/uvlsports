<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user_profile
 *
 * @author shaki
 */
class User_profile extends CI_Controller {

    //put your code here

    public function index($user_id) {
        
        
        
        $data = array();
        $data['user_info'] = $this->welcome_model->select_user_info_by_id($user_id);
        $user_name = $data['user_info']->user_name;
        $name = url_title($user_name);
        $data['user_total_post'] = $this->welcome_model->select_total_post_by_user($user_id);

        $data['total_hits'] = $this->super_admin_extra->total_hits_by_user($user_id);

        
        /* Pagination Config */
        $config = array();
        $config["base_url"] = base_url() . "/user-profile/index/$user_id/$name";
        $config["total_rows"] = $data['user_total_post'];
        $config["per_page"] = 10;
        $config["num_links"] = TRUE;

        /* BootStrap pagination Button Settings */
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span aria-hidden='true'>&raquo;</span><span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        /* !-BootStrap pagination Button Settings */

        /* !- Pagination Config */
        $this->pagination->initialize($config);

        $str_links = $this->pagination->create_links();
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $data["links"] = explode('&nbsp;', $str_links);
        
        $data['user_post_info'] = $this->welcome_model->select_all_post_info_by_user($user_id, 10, $page);

        $data['image'] = base_url().$data['user_info']->user_avatar;
        $data['url'] = base_url().'user_profile/index/'.$user_id.'/'.$data['user_info']->user_name;

        $data['recent_post'] = $this->welcome_model->select_all_published_recent();
        
        $data['popular_post'] = $this->welcome_model->select_all_published_popular();
        $data['keyword'] = "ইউনিভার্সাল স্পোর্টস, ফুটবল, ক্রিকেট, ম্যাচ বিশ্লেষণ, ফুটবল ফ্যানস বাংলাদেশ, এফএফবিডি, ইংলিশ প্রিমিয়ার লিগ, পাঠকদের মতামত, ট্রান্সফার রিউমার";
        $data['description'] = $data['user_info']->user_name.' এর ইউনিভার্সাল স্পোর্টস ওয়েবসাইট প্রোফাইল।';
        $data['title'] = $data['user_info']->user_name . ' || ইউনিভার্সাল স্পোর্টস';
        $data['maincontent'] = $this->load->view('frontend/user_profile', $data, true);
        $this->load->view('frontend/index_master', $data);
    }

}

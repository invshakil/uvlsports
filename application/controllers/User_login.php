<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user_login
 *
 * @author shaki
 */
class User_login extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $access_level = $this->session->userdata('access_level');

        if ($access_level) {
            if ($this->session->userdata('access_level') != 3) {
                redirect('secure_admin_login', 'refresh');
            }
        }
        $user_id = $this->session->userdata('user_id');
        if ($user_id != NULL) {
            redirect('user_dashboard', 'refresh');
        }
    }

    public function index() {
        $data = array();
        $data['title'] = 'Login | User Panel';
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);
        $this->load->view('adminEntry/user_login', $data);
    }

    public function user_login_check() {

        $this->form_validation->set_rules('email', 'Email ID', 'trim|required');
        $this->form_validation->set_rules('user_password', 'Password', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            // fails
            $data = array();
            $data['title'] = 'Registration | Admin Panel';
            $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
            $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);

            $this->load->view('adminEntry/user_login', $data);
        } else {

            $admin_email_address = $this->input->post('email', true);
            $admin_password = $this->input->post('user_password', true);
            $this->load->model('admin_login');
            $result = $this->admin_login->check_admin_login_info($admin_email_address, $admin_password);
            $sdata = array();

            if ($result) {

                $approval_status = $result->approval_status;

                if ($approval_status == 0) {
                    $sdata['message'] = 'Please verify your email id to login!';
                    $this->session->set_userdata($sdata);
                    redirect('user_login');
                } else if ($approval_status == 2) {
                    $sdata['message'] = '<span class="label label-danger">You email id is banned here!</span>';
                    $this->session->set_userdata($sdata);
                    redirect('user_login');
                } else {

                    $access_level = $result->access_level;

                    if ($access_level != 3) {
                        $sdata['message'] = 'Contact Administrator for admin login';
                        $this->session->set_userdata($sdata);
                        redirect('user_login');
                    } else {
                        $sdata['user_id'] = $result->user_id;
                        $sdata['user_name'] = $result->user_name;
                        $sdata['user_avatar'] = $result->user_avatar;
                        $sdata['access_level'] = $result->access_level;
                        $this->session->set_userdata($sdata);

                        $fb_info = $this->db->get_where('user', array('user_id' => $sdata['user_id']))->row('user_fb');

                        if ($fb_info != ''){
                            redirect('user_dashboard');
                        } else{
                            $sdata['message'] = 'Please submit your facebook id and other information if available!';
                            $this->session->set_userdata($sdata);
                            redirect('account_settings');
                        }


                    }
                }
            } else {
                $sdata['message'] = 'Wrong user email or password!';
                $this->session->set_userdata($sdata);
                redirect('user_login');
            }
        }
    }

    public function web_login()
    {
        $data['user'] = array();

        // Check if user is logged in
        if ($this->facebook->is_authenticated())
        {

            // User logged in, get user details
            $user = $this->facebook->request('get', '/me?fields=id,name,email,picture,profile');
            if (!isset($user['error']))
            {
                $data['user'] = $user;

                echo '<pre>';
                print_r($data);
                exit;
            }

        }

        // display view
        $this->load->view('adminEntry/login');
    }



}

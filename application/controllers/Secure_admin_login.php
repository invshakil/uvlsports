<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of super_admin_controller
 *
 * @author shakil
 */
class Secure_admin_login extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $admin_id = $this->session->userdata('user_id');
        if ($admin_id != NULL) {
            redirect('super_admin_panel', 'refresh');
        }
    }

    public function index() {
        $data = array();
        $data['title'] = 'Login | Admin Panel';
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);
        $this->load->view('adminEntry/login', $data);
//        $this->load->view('adminEntry/admin_master','',TRUE);
    }

    public function admin_login_check() {

        $this->form_validation->set_rules('email', 'Email ID', 'trim|required');
        $this->form_validation->set_rules('user_password', 'Password', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            // fails
            $data = array();
            $data['title'] = 'Registration | Admin Panel';
            $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
            $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);

            $this->load->view('adminEntry/login', $data);
        } else {

            $admin_email_address = $this->input->post('email', true);
            $admin_password = $this->input->post('user_password', true);
            $this->load->model('admin_login');
            $result = $this->admin_login->check_admin_login_info($admin_email_address, $admin_password);
            $sdata = array();
            if ($result) {

                $access_level = $result->access_level;
                if ($access_level == 3) {
                    $sdata['message'] = 'You are not authorized to login here, <a href="user_login.html" style="color: red;">click here to login</>';
                    $this->session->set_userdata($sdata);
                    redirect('secure_admin_login');
                    
                } else {
                    $sdata['user_id'] = $result->user_id;
                    $sdata['user_name'] = $result->user_name;
                    $sdata['user_avatar'] = $result->user_avatar;
                    $sdata['access_level'] = $result->access_level;
                    $this->session->set_userdata($sdata);

                    $fb_info = $this->db->get_where('user', array('user_id' => $sdata['user_id']))->row('user_fb');

                    if ($fb_info != ''){
                        redirect('super_admin_panel');
                    } else{
                        $sdata['message'] = 'Please submit your facebook id and other information if available!';
                        $this->session->set_userdata($sdata);
                        redirect('account_settings');
                    }
                }
            } else {
                $sdata['message'] = '<span class="label label-danger">Wrong user email or password!</span>';
                $this->session->set_userdata($sdata);
                redirect('secure_admin_login');
            }
        }
    }

}

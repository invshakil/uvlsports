<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of tags
 *
 * @author shaki
 */
class Tags extends CI_Controller{
    //put your code here

    public function __construct() {
        parent::__construct();

        $admin_id = $this->session->userdata('user_id');
        if ($admin_id == NULL) {
            redirect('secure_admin_login', 'refresh');
        }

        $access_level = $this->session->userdata('access_level');

        if ($access_level == 3) {
            redirect('user_dashboard', 'refresh');
        }
    }
    
    public function index() {
        $data = array();
        $data['title'] = 'Tags | Admin Master';
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);
        $data['all_tag'] = $this->super_admin_model->select_all_tag();

        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/tags', $data, true);
        $this->load->view('adminEntry/admin_master', $data);
    }

    public function search()
    {

        $search = $this->input->post('tag_name');
        $query = $this->db->group_start()->like('tag_name', $search)->group_end()->limit(15)->get('tbl_tags')->result();
        foreach ($query as $row):
            echo "<li class='list-group-item'><input type='hidden' name='post_tag_id[]' value='$row->tag_id'>" . $row->tag_name . "</li>";
        endforeach;

    }

    public function save_tag() {
        //set validation rules
        $this->load->library('form_validation');

        $this->form_validation->set_rules('tag_name', 'Tag Name', 'trim|required|min_length[3]|max_length[30]|is_unique[tbl_tags.tag_name]');
        $this->form_validation->set_rules('publication_status', 'Publication Status', 'trim|required');


        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $data = array();
            $data['tag_name'] = $this->input->post('tag_name', true);
            $data['publication_status'] = $this->input->post('publication_status', true);
            $this->super_admin_model->save_tag_info($data);
            $sdata = array();
            $sdata['message'] = 'Tag Saved Successfully!';
            $this->session->set_userdata($sdata);
            redirect('tags');
        }
    }

    public function fetch_tag(){

        $tag_id = $this->input->post('tag_id');
        if($tag_id != ''){
            $info = $this->db->get_where('tbl_tags', array('tag_id' => $tag_id))->row();
            echo json_encode($info);
        }

    }

    public function update_tag(){
        $tag_id = $this->input->post('tag_id');
        $tag_name = $this->input->post('tag_name');
        $status = $this->input->post('publication_status');


        $this->db->set('tag_name',$tag_name)->set('publication_status',$status)->where('tag_id',$tag_id)->update('tbl_tags');

    }

    public function fetchAllTags()
    {
        $tags = $this->db->get_where('tbl_tags', array('publication_status' => 1))->result();

        echo json_encode($tags);
    }
    //Delete Post

    public function delete_tag($tag_id)
    {
        $data = array();

        $result = $this->super_admin_model->delete_tag($tag_id);

        if ($result == 1){
            $data['message'] = 'Tag Deleted Successfully!';
            $this->session->set_userdata($data);
            redirect('tags');
        } else{
            $data['message'] = 'Something went wrong!';
            $this->session->set_userdata($data);
            redirect('tags');
        }

    }
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User_authentication
 *
 * @author shaki
 */


class User_authentication extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $access_level = $this->session->userdata('access_level');

        if (isset($access_level)) {
            if ($this->session->userdata('access_level') != 3) {
                redirect('super_admin_panel', 'refresh');
            }
            elseif($this->session->userdata('access_level') == 3){
                redirect('user_dashboard', 'refresh');
            }
            elseif($this->session->userdata('access_level') == NULL)
            {
                redirect('sign-in', 'refresh');
            }
        }

    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Login | User Panel';
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);
        $this->load->view('adminEntry/user_login', $data);
    }

    public function user_login_check()
    {

        $this->form_validation->set_rules('email', 'Email ID', 'trim|required');
        $this->form_validation->set_rules('user_password', 'Password', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            // fails
            $data = array();
            $data['title'] = 'Registration | Admin Panel';
            $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
            $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);

            $this->load->view('adminEntry/user_login', $data);
        } else {

            $admin_email_address = $this->input->post('email', true);
            $admin_password = $this->input->post('user_password', true);
            $this->load->model('admin_login');
            $result = $this->admin_login->check_admin_login_info($admin_email_address, $admin_password);

            $sdata = array();

            if ($result) {

                $approval_status = $result->approval_status;

                if ($approval_status == 0) {
                    $sdata['message'] = 'Please verify your email id to login!';
                    $this->session->set_userdata($sdata);
                    redirect('sign-in');
                } else if ($approval_status == 2) {
                    $sdata['error'] = 'You email id is banned here!';
                    $this->session->set_userdata($sdata);
                    redirect('sign-in');
                } else {

                    $access_level = $result->access_level;

                    if ($access_level != 3) {
                        $sdata['user_id'] = $result->user_id;
                        $sdata['user_name'] = $result->user_name;
                        $sdata['user_avatar'] = $result->user_avatar;
                        $sdata['access_level'] = $result->access_level;
                        $sdata['login_type'] = 0;
                        $this->session->set_userdata($sdata);

                        redirect('super_admin_panel');
                    } else {
                        $sdata['user_id'] = $result->user_id;
                        $sdata['user_name'] = $result->user_name;
                        $sdata['user_avatar'] = $result->user_avatar;
                        $sdata['access_level'] = $result->access_level;
                        $sdata['login_type'] = 0;
                        $this->session->set_userdata($sdata);

                        $fb_info = $this->db->get_where('user', array('user_id' => $sdata['user_id']))->row('user_fb');

                        if ($fb_info != '') {
                            redirect('user_dashboard');
                        } else {
                            $sdata['message'] = 'Please submit your facebook id and other information if available!';
                            $this->session->set_userdata($sdata);
                            redirect('account_settings');
                        }


                    }
                }
            } else {
                $sdata['error'] = 'Wrong user email or password!';
                $this->session->set_userdata($sdata);
                redirect('sign-in');
            }
        }
    }

}



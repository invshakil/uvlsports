<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{
	/*
	 * Function of Index Page
	 * @Author Shakil
	 */
	
	
	public function index()
	{
		
		$data = array ();
		$data['title'] = 'হোম || ইউনিভার্সাল স্পোর্টস';
		$data['published_latest'] = $this -> welcome_model -> select_all_published_latest();
		
		$data['cricket'] = $this -> welcome_model -> select_all_published_cricket();
		
		$data['transfer_live_latest'] = $this -> welcome_model -> transfer_live_latest_info();
		
		$content_per_page = 14;
		$count = $this -> welcome_model -> total_post_published();
		$data['total_data'] = ceil($count / $content_per_page);
		$data['popular_post'] = $this -> welcome_model -> select_index_published_popular();
		$data['top_author'] = $this -> welcome_model -> select_all_top_author();
		
		$data['image'] = base_url() . '/image_upload/post_image/' . $data['published_latest'][0] -> post_image;
		$data['url'] = base_url();
		$data['keyword'] = "ইউনিভার্সাল স্পোর্টস, ফুটবল, ক্রিকেট, ম্যাচ বিশ্লেষণ, ফুটবল ফ্যানস বাংলাদেশ, খেলার খবর সরাসরি, খেলার খবর আজ,
        লা লিগা আজকের খেলা, আজকের খেলার খবর, খেলার সংবাদ, খেলার খবর ফুটবল, খেলার সময়সূচী, খেলার খবর,
        ইংলিশ প্রিমিয়ার লিগ, লা লিগা, চ্যাম্পিয়ন্স লিগ, ট্রান্সফার রিউমার";
		
		$data['description'] = "জনপ্রিয় খেলার সংবাদ, ফুটবল ম্যাচ বিশ্লেষণ, হাইলাইটস, মতামত, আর্টিক্যাল, ট্রান্সফার লাইভ ও আরো অনেক
ফিচার নিয়ে ইউনিভার্সাল স্পোর্টস আপনার জন্যে উপহার হিসেবে উপস্থাপন করছে এই ওয়েবসাইট।";
		
		$data['maincontent'] = $this -> load -> view('frontend/index_content', $data, true);
		$this -> load -> view('frontend/index_master', $data);
	}
	
	public function load_more_post()
	{
		
		$group_no = $this -> input -> post('group_no');
		$content_per_page = 14;
		$start = ceil($group_no * $content_per_page);
		$select_all_latest_published_post = $this -> welcome_model -> select_all_latest_published_post($start, $content_per_page);
		
		?>
		<?php
		if (is_array($select_all_latest_published_post) && count($select_all_latest_published_post) >= 1) {
			
			foreach ($select_all_latest_published_post as $post) {
				?>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 wow fadeInDown" id="sideDivEffect" style="height: 320px!important;">
                    <ul class="fashion_catgnav">
                        <li>
                            <div class="catgimg2_container">
                                <a href="<?php echo base_url(); ?>news-details/<?php echo $post -> post_id; ?>/<?php echo $post -> post_category_id; ?>/<?php echo $post -> en_post_url; ?>">
                                    <img alt="<?php echo $post -> post_title; ?>" class="img-responsive img-thumbnail"
                                         src="<?php echo base_url() . './image_upload/post_image/resized/' . $post -> post_image; ?>">
                                </a>
                            </div>
                            <br/>
                            <h4 class="catg_titile">
                                <a href="<?php echo base_url(); ?>news-details/<?php echo $post -> post_id; ?>/<?php echo $post -> post_category_id; ?>/<?php echo $post -> en_post_url; ?>">
									<?php
									$str = $post -> post_title;
									$length = strlen($str);
									if ($length > 150) {
										$strCut = substr($str, 0, 150);
										$str = substr($strCut, 0, strrpos($strCut, " ")) . "...";
									}
									echo $str;
									?>
                                </a>
                            </h4>
                            <div class="comments_box"><span
                                        class="fa fa-calendar-o"> <?php $date = $post -> post_date;
									echo date("M d", strtotime($date)); ?></span>
                                <span class="fa fa-user">
                                    <a href="<?php echo base_url(); ?>user-profile/index/<?php echo $post -> user_id; ?>">
                                        <?php $str = $post -> user_name;
										$length = strlen($str);
										if ($length > 16) {
											$strCut = substr($str, 0, 14);
											$str = substr($strCut, 0, strrpos($strCut, " "));
										}
										echo $str; ?>
                                    </a>
                                </span>&nbsp;&nbsp;
                                <span class="meta_more">
                                    <a href="<?php echo base_url(); ?>news-details/<?php echo $post -> post_id; ?>/<?php echo $post -> post_category_id; ?>/<?php echo $post -> en_post_url; ?> ">Read
                                More...</a>
                                </span>
                            </div>
                            <hr/>
                        </li>
                    </ul>
                </div>
				<?php
			}
		}
	}
	
	public function search()
	{
		
		$search = $this -> input -> post('search_data');
		$query = $this -> welcome_model -> searchResult($search);
		foreach ($query as $row):
			echo "<li class='list-group-item'><a href=" . base_url() . 'news-details/' . $row -> post_id . '/' . $row -> post_category_id . '/' . $row -> en_post_url . ">" . $row -> post_title . "</a></li>";
		endforeach;
	}
	
	public function search_result()
	{
		$data = array ();
		$search = $this -> input -> get('search_data');
		
		$data['title'] = 'Search Result on "' . $search . '"';
		$data['searched_result'] = $this -> welcome_model -> searchResult($search);
		$data['popular_post'] = $this -> welcome_model -> select_all_published_popular();
		$data['recent_post'] = $this -> welcome_model -> select_all_published_recent();
		
		$data['image'] = base_url() . '/image_upload/post_image/' . $data['searched_result'][0] -> post_image;
		$data['url'] = base_url() . 'search_result';
		$data['description'] = 'Result of ' . $search;
		$data['keyword'] = "ইউনিভার্সাল স্পোর্টস, ফুটবল, ক্রিকেট, ম্যাচ বিশ্লেষণ, ফুটবল ফ্যানস বাংলাদেশ, এফএফবিডি, ইংলিশ প্রিমিয়ার লিগ, লা লিগা, চ্যাম্পিয়ন্স লিগ, পাঠকদের মতামত, ট্রান্সফার রিউমার, খেলার খবর সরাসরি, খেলার খবর আজ,
        লা লিগা আজকের খেলা, আজকের খেলার খবর, খেলার সংবাদ, খেলার খবর ফুটবল, খেলার সময়সূচী, খেলার খবর";
		
		$data['maincontent'] = $this -> load -> view('frontend/searched_item', $data, true);
		$this -> load -> view('frontend/index_master', $data);
	}
	
	public function page_404()
	{
		$data = array ();
		$data['title'] = 'lost?';
		
		$data['popular_post'] = $this -> welcome_model -> select_all_published_popular();
		
		$data['image'] = base_url() . '/image_upload/post_image/' . $data['popular_post'][0] -> post_image;
		$data['url'] = base_url() . 'page_404';
		$data['description'] = 'জনপ্রিয় খেলার সংবাদ, ফুটবল ম্যাচ বিশ্লেষণ, হাইলাইটস, মতামত, আর্টিক্যাল, ট্রান্সফার লাইভ ও আরো অনেক
ফিচার নিয়ে ইউনিভার্সাল স্পোর্টস আপনার জন্যে উপহার হিসেবে উপস্থাপন করছে এই ওয়েবসাইট।';
		$data['keyword'] = "ইউনিভার্সাল স্পোর্টস, ফুটবল, ক্রিকেট, ম্যাচ বিশ্লেষণ, ফুটবল ফ্যানস বাংলাদেশ, এফএফবিডি, ইংলিশ প্রিমিয়ার লিগ, লা লিগা, চ্যাম্পিয়ন্স লিগ, পাঠকদের মতামত, ট্রান্সফার রিউমার";
		$data['maincontent'] = $this -> load -> view('frontend/page_404', $data, true);
		$this -> load -> view('frontend/index_master', $data);
	}
	
	/*
	 * Function of League Fixtures & Point Table
	 * @Author Shakil
	 */
	
	public function point_table()
	{
		$data = array ();
		$data['title'] = 'পয়েন্ট তালিকা || ইউনিভার্সাল স্পোর্টস';
		$data['image'] = NULL;
		$data['url'] = base_url() . 'point_table';
		$data['description'] = 'ইউরোপিয়ান শীর্ষ লিগগুলোর পয়েন্ট টেবিলের আপডেট পাবেন এখানে।';
		$data['keyword'] = "ইউনিভার্সাল স্পোর্টস, ফুটবল, ক্রিকেট, ম্যাচ বিশ্লেষণ, ফুটবল ফ্যানস বাংলাদেশ, এফএফবিডি, ইংলিশ প্রিমিয়ার লিগ, লা লিগা, চ্যাম্পিয়ন্স লিগ, পাঠকদের মতামত, ট্রান্সফার রিউমার, পয়েন্ট টেবিল, খেলার খবর সরাসরি, খেলার খবর আজ,
        লা লিগা আজকের খেলা, আজকের খেলার খবর, খেলার সংবাদ, খেলার খবর ফুটবল, খেলার সময়সূচী, খেলার খবর";
		$data['maincontent'] = $this -> load -> view('frontend/point_table', $data, true);
		$this -> load -> view('frontend/index_master', $data);
	}
	
	public function league_fixture()
	{
		$data = array ();
		$data['title'] = 'খেলার সময়সূচী || ইউনিভার্সাল স্পোর্টস';
		$data['image'] = NULL;
		$data['url'] = base_url() . 'league_fixture';
		$data['description'] = 'ইউরোপিয়ান শীর্ষ লিগগুলোর ম্যাচ ফিকচারগুলোর আপডেট পাবেন এখানে।';
		$data['keyword'] = "ইউনিভার্সাল স্পোর্টস, ফুটবল, ক্রিকেট, ম্যাচ বিশ্লেষণ, ফুটবল ফ্যানস বাংলাদেশ, এফএফবিডি, ইংলিশ প্রিমিয়ার লিগ, লা লিগা, চ্যাম্পিয়ন্স লিগ, পাঠকদের মতামত, ট্রান্সফার রিউমার, লিগ ফিকচার, খেলার খবর সরাসরি, খেলার খবর আজ,
        লা লিগা আজকের খেলা, আজকের খেলার খবর, খেলার সংবাদ, খেলার খবর ফুটবল, খেলার সময়সূচী, খেলার খবর";
		$data['maincontent'] = $this -> load -> view('frontend/league_fixture', '', true);
		$this -> load -> view('frontend/index_master', $data);
	}
	
	public function privacy_policy()
	{
		$data = array ();
		$data['title'] = 'Privacy Policy || Universal Sports';
		$data['image'] = NULL;
		$data['url'] = base_url() . 'privacy_policy';
		$data['popular_post'] = $this -> welcome_model -> select_all_published_popular();
		$data['recent_post'] = $this -> welcome_model -> select_all_published_recent();
		$data['description'] = 'ইউরোপিয়ান শীর্ষ লিগগুলোর ম্যাচ ফিকচারগুলোর আপডেট পাবেন এখানে।';
		$data['keyword'] = "ইউনিভার্সাল স্পোর্টস, ফুটবল, ক্রিকেট, ম্যাচ বিশ্লেষণ, ফুটবল ফ্যানস বাংলাদেশ, এফএফবিডি, ইংলিশ প্রিমিয়ার লিগ, লা লিগা, চ্যাম্পিয়ন্স লিগ, পাঠকদের মতামত, ট্রান্সফার রিউমার, লিগ ফিকচার";
		$data['maincontent'] = $this -> load -> view('frontend/privacy_policy', $data, true);
		$this -> load -> view('frontend/index_master', $data);
	}
	
	/*
	 * Function of Twitter
	 * @Author Shakil
	 */
	
	public function twitter()
	{
		$data = array ();
		$data['title'] = 'Tweets || Universal Sports';
		$data['image'] = NULL;
		$data['url'] = base_url() . 'twitter';
		$data['description'] = 'ট্রান্সফার রিউমার গুলোর টুইটার আপডেট পাবেন এখানে।';
		$data['keyword'] = "ইউনিভার্সাল স্পোর্টস, ফুটবল, ক্রিকেট, ম্যাচ বিশ্লেষণ, ফুটবল ফ্যানস বাংলাদেশ, এফএফবিডি, ইংলিশ প্রিমিয়ার লিগ, লা লিগা, চ্যাম্পিয়ন্স লিগ, পাঠকদের মতামত, ট্রান্সফার রিউমার, টুইটার";
		$data['maincontent'] = $this -> load -> view('frontend/twitter_live', '', true);
		$this -> load -> view('frontend/index_master', $data);
	}
	
	/*
	 * Function of Video Gallery
	 * @Author Shakil
	 */
	
	
	/*
	 * Function of Contact
	 * @Author Shakil
	 */
	
	public function contact()
	{
		
		$data = array ();
		$data['title'] = "মতামত || ইউনিভার্সাল স্পোর্টস";
		$data['popular_post'] = $this -> welcome_model -> select_all_published_popular();
		$data['image'] = base_url() . 'contact.jpg';
		$data['url'] = base_url() . 'contact';
		$data['description'] = 'আমাদের সাথে এখানে যোগাযোগ করুন';
		$data['keyword'] = "ইউনিভার্সাল স্পোর্টস, ফুটবল, ক্রিকেট, ম্যাচ বিশ্লেষণ, ফুটবল ফ্যানস বাংলাদেশ, এফএফবিডি, ইংলিশ প্রিমিয়ার লিগ, লা লিগা, চ্যাম্পিয়ন্স লিগ, পাঠকদের মতামত, ট্রান্সফার রিউমার, যোগাযোগ করুন";
		$data['maincontent'] = $this -> load -> view('frontend/contact', $data, true);
		$this -> load -> view('frontend/index_master', $data);
	}
	
	public function weekend_fixture_on_tv()
	{
		$data = array ();
		
		$gw_info = $this -> db -> order_by('gw_id', 'desc') -> limit(1) -> get('tbl_gwtitle') -> row('gw_id');
		$data['match_fixture'] = $this -> welcome_model -> get_match_fixture($gw_info);
		
		$data['title'] = "খেলার সময়সূচী || ইউনিভার্সাল স্পোর্টস";
		$data['popular_post'] = $this -> welcome_model -> select_all_published_popular();
		$data['image'] = base_url() . 'live11.jpg';
		$data['url'] = base_url() . 'weekend_fixture_on_tv';
		$data['description'] = 'এই সপ্তাহের ম্যাচগুলো যে টিভি চ্যানেলে দেখাবে!';
		$data['keyword'] = "ইউনিভার্সাল স্পোর্টস, ফুটবল, ক্রিকেট, ম্যাচ বিশ্লেষণ, ফুটবল ফ্যানস বাংলাদেশ, এফএফবিডি, ইংলিশ প্রিমিয়ার লিগ, লা লিগা, চ্যাম্পিয়ন্স লিগ, টিভি চ্যানেলে ম্যাচ সম্প্রচার, খেলার খবর সরাসরি, খেলার খবর আজ,
        লা লিগা আজকের খেলা, আজকের খেলার খবর, খেলার সংবাদ, খেলার খবর ফুটবল, খেলার সময়সূচী, খেলার খবর";
		$data['maincontent'] = $this -> load -> view('frontend/weekend_fixture_on_tv', $data, true);
		$this -> load -> view('frontend/index_master', $data);
	}
	
	public function save_contact_sms()
	{
		$this -> load -> library('form_validation');
		
		$this -> form_validation -> set_rules('sender_name', 'Name', 'trim|required|min_length[3]');
		$this -> form_validation -> set_rules('sender_email', 'Email', 'trim|required');
		$this -> form_validation -> set_rules('sender_subject', 'Subject', 'trim|required');
		$this -> form_validation -> set_rules('sender_message', 'Message', 'trim|required');
		
		
		if ($this -> form_validation -> run() == FALSE) {
			$this -> contact();
		} else {
			$data = array ();
			$data['sender_name'] = $this -> input -> post('sender_name', true);
			$data['sender_email'] = $this -> input -> post('sender_email', true);
			$data['sender_subject'] = $this -> input -> post('sender_subject', true);
			$data['sender_message'] = $this -> input -> post('sender_message', true);
			date_default_timezone_set('Asia/Dhaka');
			$contact_time = date('Y-m-d H:i:s');
			$data['contact_date'] = $contact_time;
			
			$this -> db -> insert('tbl_contact', $data);
			
			$data['message'] = '<div class="label label-success">Your message received, we will get back to you soon at your email inbox!</div>';
			$this -> session -> set_userdata($data);
			redirect('contact');
		}
		
	}
	
	/*
	 * Function of Post News Details by id
	 * @Author Shakil
	 */
	
	public function news_details($post_id)
	{
		$data = array ();
		$data['post_info'] = $this -> welcome_model -> select_post_info_by_id($post_id);
		
		$category_id = $data['post_info'] -> post_category_id;
		$data['similar_post'] = $this -> welcome_model -> get_similar_post($category_id);
		
		if ($data['post_info'] -> post_tag_id == '')
		{
			$data['tag_info'] = $this -> welcome_model->select_all_tag_by_post_id($post_id);
		}
		else
		    {
			$data['tags_info'] = $data['post_info']->post_tag_id;
		}
		
		
		$value = $this -> session -> userdata('hit_count' . $post_id);
		
		if ($value == NULL) {
			$hit_count = $data['post_info'] -> hit_count + 1;
			$this -> welcome_model -> update_hit_count($hit_count, $post_id);
			
			$this -> session -> set_userdata('hit_count' . $post_id, 1);
		}
		
		
		$data['title'] = $data['post_info'] -> post_title . ' || ইউনিভার্সাল স্পোর্টস';
		
		$data['read_more_articles'] = $this -> welcome_model -> select_random_articles();
		
		$data['popular_post'] = $this -> welcome_model -> select_all_published_popular();
		$data['recent_post'] = $this -> welcome_model -> select_all_published_recent();
		$data['image'] = base_url() . '/image_upload/post_image/' . $data['post_info'] -> post_image;
		$data['url'] = base_url() . 'news-details/' . $post_id . '/' . $category_id . '/' . $data['post_info'] -> en_post_url;
		
		if ($data['post_info'] -> meta_description != '') {
			$data['description'] = $data['post_info'] -> meta_description;
		} else {
			$str = strip_tags($data['post_info'] -> post_description);
			$length = strlen($str);
			if ($length > 460) {
				$strCut = substr($str, 0, 450);
				$str = substr($strCut, 0, strrpos($strCut, ' ')) . '...';
			}
			
			$data['description'] = $str;
		}
		
		
		$data['keyword'] = "ইউনিভার্সাল স্পোর্টস, ফুটবল আর্টিক্যাল";
		$author_id = $data['post_info'] -> user_id;
		$fb_id = $this -> db -> get_where('user', array ('user_id' => $author_id)) -> row('user_fb');
		
		if ($fb_id != '') {
			$data['author_info'] = $fb_id;
		} else {
			$name = $this -> db -> get_where('user', array ('user_id' => $author_id)) -> row('user_name');
			$data['author_info'] = $name;
		}
		
		
		$data['maincontent'] = $this -> load -> view('frontend/news_details', $data, true);
		$this -> load -> view('frontend/index_master', $data);
	}
	
	
}

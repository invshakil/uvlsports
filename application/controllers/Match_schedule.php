<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of account_settings
 *
 * @author shaki
 */
class Match_schedule extends CI_Controller
{
    public function __construct() {
        parent::__construct();

        $admin_id = $this->session->userdata('user_id');
        if ($admin_id == NULL) {
            redirect('user_login', 'refresh');
        }

    }

    public function index() {

        $data = array();

        $data['title'] = 'Game Week Create | Admin Master';
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);

        $data['game_week_title'] = $this->db->get('tbl_gwtitle')->result();

        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/match_schedule/game_week_create', $data, true);

        $this->load->view('adminEntry/admin_master', $data);
    }

    public function save_game_week_title() {
        //set validation rules
        $this->load->library('form_validation');

        $this->form_validation->set_rules('game_week', 'Game Week Title', 'trim|required|min_length[3]|max_length[30]|is_unique[tbl_gwtitle.game_week]');
        $this->form_validation->set_rules('publication_status', 'Publication Status', 'trim|required');


        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $data = array();
            $data['game_week'] = $this->input->post('game_week', true);
            $data['publication_status'] = $this->input->post('publication_status', true);
            $this->db->insert('tbl_gwtitle',$data);
            $sdata = array();
            $sdata['message'] = 'Game Week Created Successfully!';
            $this->session->set_userdata($sdata);
            redirect('match_schedule');
        }
    }

    public function game_week_match_add()
    {
        $data = array();

        $data['title'] = 'Match Create | Admin Master';
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);
        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/match_schedule/game_week_match_add', $data, true);

        $this->load->view('adminEntry/admin_master', $data);
    }

    public function save_match_info()
    {
        //set validation rules
        $this->load->library('form_validation');

        $this->form_validation->set_rules('gw_id', 'Game Week Title', 'trim|required');
        $this->form_validation->set_rules('match_league', 'League', 'trim|required');
        $this->form_validation->set_rules('match_title', 'Match Name', 'trim|required');
        $this->form_validation->set_rules('match_time', 'Match Time', 'trim|required');
        $this->form_validation->set_rules('match_channel', 'Match channel', 'trim|required');
        $this->form_validation->set_rules('m_publication_status', 'Publication Status', 'trim|required');


        if ($this->form_validation->run() == FALSE) {
            
            $this->game_week_match_add();
        } else {
            $data = array();
            $data['gw_id'] = $this->input->post('gw_id', true);
            $data['match_league'] = $this->input->post('match_league', true);
            $data['match_title'] = $this->input->post('match_title', true);
            $data['match_time'] = strtotime($this->input->post('match_time', true));
            $data['match_channel'] = $this->input->post('match_channel', true);
            $data['m_publication_status'] = $this->input->post('m_publication_status', true);
            $this->db->insert('tbl_match_schedule',$data);
            $sdata = array();
            $sdata['message'] = 'Match Information Saved Successfully!';
            $this->session->set_userdata($sdata);
            redirect('match_schedule/game_week_match_add');
        }
    }

    public function manage_game_week_match()
    {
        $data = array();

        $data['game_week_matches'] = $this->super_admin_model->get_game_week_matches();

        $data['title'] = 'Manage Matches | Admin Master';
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);
        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/match_schedule/manage_match_schedule', $data, true);

        $this->load->view('adminEntry/admin_master', $data);

    }

    public function edit_match_schedule($id)
    {
        $data = array();

        $data['match_info'] = $info = $this->db->get_where('tbl_match_schedule', array('match_id' => $id))->row();

        $data['title'] = 'Game Week Create | Admin Master';
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);
        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/match_schedule/edit_match_schedule', $data, true);

        $this->load->view('adminEntry/admin_master', $data);
    }

    public function update_match_info()
    {

        $match_id  = $this->input->post('match_id', true);
        $data['gw_id'] = $this->input->post('gw_id', true);
        $data['match_league'] = $this->input->post('match_league', true);
        $data['match_title'] = $this->input->post('match_title', true);
        $data['match_time'] = strtotime($this->input->post('match_time', true));
        $data['match_channel'] = $this->input->post('match_channel', true);
        $data['m_publication_status'] = $this->input->post('m_publication_status', true);

//        echo '<pre>';print_r($match_id);exit;

        $r = $this->super_admin_model->post_update_match_info($data,$match_id);
        if ($r)
        {
            $sdata = array();
            $sdata['message'] = 'Match Information Updated Successfully!';
            $this->session->set_userdata($sdata);
            redirect('match_schedule/manage_game_week_match');
        }
        else{
            $sdata = array();
            $sdata['message'] = 'Failed!';
            $this->session->set_userdata($sdata);
            redirect('match_schedule/manage_game_week_match');
        }
    }

    public function status_to_pending($match_id)
    {
        $this->db->set('m_publication_status',0)->where('match_id',$match_id)->update('tbl_match_schedule');
        $sdata = array();
        $sdata['message'] = 'Match Publication status changed to pending!';
        $this->session->set_userdata($sdata);
        redirect('match_schedule/manage_game_week_match');
    }

    public function status_to_published($match_id)
    {
        $this->db->set('m_publication_status',1)->where('match_id',$match_id)->update('tbl_match_schedule');
        $sdata = array();
        $sdata['message'] = 'Match Publication status changed to published!';
        $this->session->set_userdata($sdata);
        redirect('match_schedule/manage_game_week_match');
    }

    public function delete_match($id)
    {
        $data = array();

        $result = $this->super_admin_model->delete_match($id);

        if ($result == 1){
            $data['message'] = 'Match Info Deleted Successfully!';
            $this->session->set_userdata($data);
            redirect('match_schedule/manage_game_week_match');
        } else{
            $data['message'] = 'Something went wrong!';
            $this->session->set_userdata($data);
            redirect('match_schedule/manage_game_week_match');
        }
    }



}
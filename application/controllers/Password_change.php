<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of password_change
 *
 * @author shaki
 */
class password_change extends CI_Controller{
    //put your code here
    
    public function __construct() {
        parent::__construct();

        $user_id = $this->session->userdata('user_id');
        if ($user_id == NULL) {
            redirect('user_login', 'refresh');
        }
        
    }
    
    public function index(){
        $data = array();

        $data['title'] = 'Password Change | User Panel';

        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);
        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/password_change', $data, true);
        $this->load->view('adminEntry/admin_master', $data);
    }

    public function save_password()
    {
        $id = $this->session->userdata('user_id');
        $current_password = md5($this->input->post('user_password'));

        $user_info = $this->db->get_where('user', array('user_id' => $id))->row();
        $password = $user_info->user_password;

        if ($current_password == $password)
        {
            $this->form_validation->set_rules('user_password', 'Current Password', 'trim|required');
            $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[6]|matches[confirm_password]');
            $this->form_validation->set_rules('confirm_password', 'Confirm password', 'trim|required');


            if ($this->form_validation->run() == FALSE) {
                $this->index();
            } else {
                $new_password = md5($this->input->post('new_password'));

                $this->db->set('user_password',$new_password)->where('user_id',$id)->update('user');
                $data['message'] = "Password changed successfully!";
                $this->session->set_userdata($data);
                redirect('password_change');

            }
        }
        else
        {
            $data['message'] = "Please enter your current password correctly!";
            $this->session->set_userdata($data);
            redirect('password_change');
        }


    }
}

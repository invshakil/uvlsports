<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: shaki
 * Date: 14-Aug-17
 * Time: 1:20 AM
 */
class Contact_mail extends CI_Controller
{
    public function __construct() {
        parent::__construct();

        $admin_id = $this->session->userdata('user_id');
        if ($admin_id == NULL) {
            redirect('secure_admin_login', 'refresh');
        }

        $access_level = $this->session->userdata('access_level');

        if ($access_level == 3) {
            redirect('user_dashboard', 'refresh');
        }
    }

    public function index()
    {
        $data['title'] = "Contact || Admin Panel";
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);

        $data['received_email'] = $this->db->order_by('contact_id','DESC')->get_where('tbl_contact',array('deletion_status' => 1))->result_array();

        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/contact/received_mail', $data, true);
        $this->load->view('adminEntry/admin_master', $data);
    }

    public function read_mail($id)
    {
        $data['title'] = "Contact || Admin Panel";
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);

        $this->db->set('read_status',1)->where('contact_id',$id)->update('tbl_contact');

        $data['row'] = $this->db->get_where('tbl_contact',array('deletion_status' => 1,'contact_id' => $id))->row();

        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/contact/read_mail', $data, true);
        $this->load->view('adminEntry/admin_master', $data);
    }

    public function reply_mail($id)
    {
        $data['title'] = "Contact || Admin Panel";
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);

        $data['row'] = $this->db->get_where('tbl_contact',array('deletion_status' => 1,'contact_id' => $id))->row();

        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/contact/compose_mail', $data, true);
        $this->load->view('adminEntry/admin_master', $data);
    }

    public function send_mail_to_the_user()
    {
        $data = array();
        $data['admin_id'] = $this->session->userdata('user_id');
        date_default_timezone_set('Asia/Dhaka');
        $contact_time = date('Y-m-d H:i:s');
        $data['reply_date'] = $contact_time;
        $data['receiver_email'] = 'uvlsports@gmail.com';

        $data['sender_email'] = $this->input->post('sender_email');
        $data['contact_id'] = $this->input->post('contact_id');
        $data['receiver_subject'] = $this->input->post('receiver_subject');
        $data['receiver_reply'] = $this->input->post('receiver_reply');

        $this->db->insert('tbl_contact_reply',$data);

        // EMAIL MODEL

        $from_email = $data['receiver_email']; //change this to yours
        $subject = $data['receiver_subject'];
        $message = $data['receiver_reply'];

        //configure email settings
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com'; //smtp host name
        $config['smtp_port'] = '465'; //smtp port number
        $config['smtp_user'] = $from_email;
        $config['smtp_pass'] = 'ffbdbcf$143'; //$from_email password
        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n"; //use double quotes
        $this->email->initialize($config);

        //send mail
        $this->email->from($from_email, 'Universal Sports');
        $this->email->to($data['sender_email']);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();

        $data['message'] = 'Email Sent Successfully!';
        $this->session->set_userdata($data);

        redirect('contact_mail');

    }

    public function sent_mail()
    {
        $data['title'] = "Contact Management || Admin Panel";
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);

        $data['sent_mail'] = $this->db->order_by('reply_id','DESC')->get('tbl_contact_reply')->result_array();

        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/contact/sent_mail', $data, true);
        $this->load->view('adminEntry/admin_master', $data);
    }

    public function delete_mail($id)
    {
        $data = array();

        $result = $this->db->delete('tbl_contact', array('contact_id' => $id));

        if ($result == 1){
            $data['message'] = 'This Email Deleted Successfully!';
            $this->session->set_userdata($data);
            redirect('contact_mail');
        } else{
            $data['message'] = 'Something went wrong!';
            $this->session->set_userdata($data);
            redirect('tags');
        }
    }

    public function read_sent_mail($id)
    {
        $data['title'] = "Contact || Admin Panel";
        $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
        $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);

        $data['row'] = $this->db->get('tbl_contact_reply')->row();

        $data['admin_maincontent'] = $this->load->view('adminEntry/pages/contact/read_sent_mail', $data, true);
        $this->load->view('adminEntry/admin_master', $data);
    }
}
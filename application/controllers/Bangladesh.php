<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of bangladesh
 *
 * @author shaki
 */
class Bangladesh extends CI_Controller{
    //put your code here
    public function index()
    { 
        $category_id = 8;
        $data = array();
        $data['popular_post'] = $this->welcome_model->select_all_published_popular_of_this_cat($category_id);
        $data['recent_post'] = $this->welcome_model->select_all_published_recent();
        //$data['video_post'] = $this->welcome_model->select_all_published_recent_video();

        /* Pagination Config */
        $config = array();
        $config["base_url"] = base_url() . "/bangladesh/index/";
        $config["total_rows"] = $this->welcome_leagues->record_count($category_id);
        $config["per_page"] = 10;
        $config["num_links"] = TRUE;

        /* BootStrap pagination Button Settings */
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config['full_tag_open'] = "<ul class='no-bullet'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        /* !-BootStrap pagination Button Settings */

        /* !- Pagination Config */
        $this->pagination->initialize($config);

        $str_links = $this->pagination->create_links();
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = explode('&nbsp;', $str_links);
        $data['all_post_from_league'] = $this->welcome_leagues->all_post_from_league($category_id, $config["per_page"], $page);

        $data['image'] = base_url().'/image_upload/post_image/'.$data['all_post_from_league'][0]->post_image;
        $data['url'] = base_url().'bangladesh';
        $data['description'] = 'বাংলাদেশ সম্পর্কিত সকল ধরণের ফুটবলের আপডেটের তালিকা।';
        $data['title'] = 'বাংলাদেশের ফুটবল সংবাদ || ইউনিভার্সাল স্পোর্টস';
        $data['keyword'] = "ইউনিভার্সাল স্পোর্টস, ফুটবল, ক্রিকেট, ম্যাচ বিশ্লেষণ, ফুটবল ফ্যানস বাংলাদেশ, এফএফবিডি, বাংলাদেশ, পাঠকদের মতামত, ট্রান্সফার রিউমার";
        $data['headline'] = 'বাংলাদেশ';
        $data['maincontent'] = $this->load->view('frontend/articles', $data, true);
        $this->load->view('frontend/index_master', $data);
    }
}

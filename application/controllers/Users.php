<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of users
 *
 * @author shakil
 */
session_start();

class Users extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session', 'form_validation', 'email'));
        $this->load->database();
        $this->load->model('user_model');
    }

    function index() {
        $this->register();
    }

    function register() {
        //set validation rules
        $this->form_validation->set_rules('user_name', 'First Name', 'trim|required|alpha|min_length[3]|max_length[30]|xss_clean');
        $this->form_validation->set_rules('user_email', 'Email ID', 'trim|required|valid_email|is_unique[user.email]');
        $this->form_validation->set_rules('user_password', 'Password', 'trim|required|matches[cpassword]|md5');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required');
        $this->form_validation->set_rules('user_avatar', 'User Image', 'trim|required');

        //validate form input
        if ($this->form_validation->run() == FALSE) {
            // fails
            $data = array();
            $data['title'] = 'Registration | Admin Panel';
            $data['header_content'] = $this->load->view('adminEntry/header_content', '', true);
            $data['footer_content'] = $this->load->view('adminEntry/footer_content', '', true);
            $this->load->view('adminEntry/user_registration', $data);
        } else {
            //insert the user registration details into database
            $data = array(
                'user_name' => $this->input->post('user_name'),
                'user_email' => $this->input->post('user_email'),
                'password' => $this->input->post('password'),
                'user_avatar' => $this->input->post('user_avatar')
            );

            // insert form data into database
            if ($this->user_model->insertUser($data)) {
                // send email
                if ($this->user_model->sendEmail($this->input->post('user_email'))) {
                    // successfully sent mail
                    $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">You are Successfully Registered! Please confirm the mail sent to your Email-ID!!!</div>');
                    redirect('Adminentry/index');
                } else {
                    // error
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                    redirect('Adminentry/user_registration');
                }
            } else {
                // error
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                redirect('Adminentry/user_registration');
            }
        }
    }

    function verify($hash = NULL) {
        if ($this->user_model->verifyEmailID($hash)) {
            $this->session->set_flashdata('verify_msg', '<div class="alert alert-success text-center">Your Email Address is successfully verified! Please login to access your account!</div>');
            redirect('Adminentry/index');
        } else {
            $this->session->set_flashdata('verify_msg', '<div class="alert alert-danger text-center">Sorry! There is error verifying your Email Address!</div>');
            redirect('Adminentry/user_registration');
        }
    }

}
?>
}

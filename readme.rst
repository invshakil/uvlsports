Change log: July Update 2017

- Reset password option has been introduced for user forgot password. 
- Website homepage now have infinite scroll load more posts like facebook/twitter
- Transfer Live option has been introduced which is developed by ajax
- User dashboard has much more functionality like changing profile picture, checking history of user previous post, check out his most popular post with click number etc.
- Content lock system has been introduced for admins. Whenever one admin entered into editing mode for a post, other admin can't edit that post and will know which admin is editing that post.
- RSS FEED has been created for facebook instant article feature which is working great till now. 
- Some of coding has been tweaked and bug fixed 

change log April 2017: 

- In Website home, After Login, registration/login menu disappears and new menu shows -> add post, view profile, Logged in as user, logout option
- Saving account Settings
- Password can be changed from user dashboard.
- User can check what he had posted till now and can edit, update his post and can submit that post for approval. 
- Admin List is being shown on admin dashboard.

change log beta version: 

- Fully Dynamic web site
- User Registration
- User Login
- Different Access for User, Admin and Super Admin.
- Add Post option for user which will be submitted for admin approval. 
- Admin Can add category, tag, create post, update post, update image, publish post which is in pending. 
- Different statistics view in dashboard for user and admin.
- User Profile
 
